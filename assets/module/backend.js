class App
{
	/* Ajax Request */
	static ajax_request(ajax_param,callback)
	{
		$.ajax(ajax_param).done(function(data){
			App.response_handler(data);
			if(callback !== undefined)
			{
				if(typeof callback == 'function')
				{
					callback(ajax_param,data);
				}
				else
				{
					eval(callback+'(ajax_param,data)');
				}
			}
		}).fail(function(response){
			swal({
				title: '<b>Request Failed',
				type: 'error',
				html:response.responseText,
				showCloseButton: true,
				focusConfirm: false,
				timer:null
			}).then(function(){},function(dismiss){})
		})
	}

	/* Jquery Time Ago */
	static timeago(date_param)
	{
		if(date_param !== null)
		{
			if(typeof date_param == 'object')
			{
				var date = new Date(date_param.date).toISOString()
				return jQuery.timeago(date)
			}
			else
			{
				var date = new Date(date_param).toISOString();
				return jQuery.timeago(date);
			}
		}
		else
		{
			return false;
		}
	}

	/* Response Handler Sweet Alert */
	static response_handler(data)
	{
		if(data.status == 'failed')
		{
			switch(data.message)
			{
				case 'validation_error':
					swal({
						type:'error',
						html:data.data.join('</p><p>')
					})
				break;

				case 'data_not_found':
					swal({
						type: 'error',
						title: 'Error!',
						text: 'data not found',
					})
				break;

				default :
					swal({
						type: 'error',
						title: 'Error!',
						text: data.message,
					})
				break;
			}
		}
	}

	/* Random String */
	static random_string(length)
	{
	    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');

	    if (! length) {
	        length = Math.floor(Math.random() * chars.length);
	    }

	    var str = '';
	    for (var i = 0; i < length; i++) {
	        str += chars[Math.floor(Math.random() * chars.length)];
	    }
	    return str;
	}

	/* Get URL Paremeters */
	static get_params(search_string)
	{
		var parse = function(params, pairs)
		{
		    var pair = pairs[0];
		    var parts = pair.split('=');
		    var key = decodeURIComponent(parts[0]);
		    var value = decodeURIComponent(parts.slice(1).join('='));
		    if (typeof params[key] === "undefined")
		    {
		    	params[key] = value;
		    }
		    else
		    {
		    	params[key] = [].concat(params[key], value);
		    }
		    return pairs.length == 1 ? params : parse(params, pairs.slice(1))
		}
		return search_string.length == 0 ? {} : parse({}, search_string.substr(1).split('&'))
	}

	/* print page */
	static print_page(page)
	{
		var printContents = document.getElementById('print_page').innerHTML;
		document.body.innerHTML = printContents;
		window.focus();
		window.print();
		window.location.reload()
	}
}


/* Datatable Custom */
class DataTable_Custom
{
	/* Destroy Data Table */
	static destroy_datatable(selector='.datatable_server_side')
	{
		if ($.fn.DataTable.isDataTable(selector))
		{
			$(selector).empty();
			$(selector).DataTable().clear().destroy();
		}
	}
		
	/* Custom Export File Name */
	static ExportFileName(that,e,dt,node,config,extend)
	{
		if(extend)
		{
			swal({
				title: 'Export File Name',
				input: 'text',
				inputPlaceholder:'enter file name',
				showCancelButton: true,
				confirmButtonText: 'Submit',
				showLoaderOnConfirm: true,
			}).then(function(result){
				if(result)
				{
					config.filename = result
					$.fn.DataTable.ext.buttons[extend].action.call(that, e, dt, node, config)
				}
			},function(dismiss){})
		}
	}

	static convertToRupiah(angka)
	{
		var rupiah = '';		
		var angkarev = angka.toString().split('').reverse().join('');
		for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
		return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
	}

	static convertToAngka(rupiah)
	{
		return parseInt(rupiah.replace(/[^0-9]/g, ''), 10);
	}

}

function convertToRupiah(angka)
{
	var rupiah = '';		
	var angkarev = angka.toString().split('').reverse().join('');
	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
}
/**
 * Usage example:
 * alert(convertToRupiah(10000000)); -> "Rp. 10.000.000"
 */
 
function convertToAngka(rupiah)
{
	return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);
}


// Ajax Pace
$(document).ajaxStart(function(){
	// Pace.restart();
	icheck_init('flat_green').on('ifChecked', function(event){datatable.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){datatable.row($(this).parents('tr')).deselect()});
})

// History Back
$(document).on('click', '.history_back', function(event) {
	event.preventDefault()
	window.history.back()
});

// Password Generator
$(document).on('click', '.passwd_generator', function(event) {
	event.preventDefault();
	var form_input 		= $(this).attr('input_form');
	var input 			= form_input.split('|');
	var random_string 	= Math.random().toString(36).slice(-8);
	if(input !== false)
	{
		$.each(input,function(index, el){
			$('input[name="'+el+'"]').val(random_string)
		});
	}
});

// Show Password
$(document).on('click', '.show_passwd', function(event) {
	event.preventDefault();
	var form_input 		= $(this).attr('input_form');
	var input 			= form_input.split('|');
	var state 			= ($(this).attr('state') == undefined)?'hidden':$(this).attr('state');
	console.log(state)
	if(state == 'hidden')
	{
		$(this).html('<i class="fa fa-eye-slash"></i> hide');
		$(this).attr('state','shown');
		if(input !== false)
		{
			$.each(input,function(index, el){
				$('input[name="'+el+'"]').attr('type', 'text');
			});
		}
	}
	else
	{
		$(this).html('<i class="fa fa-eye"></i> show');
		$(this).attr('state','hidden');
		if(input !== false)
		{
			$.each(input,function(index, el){
				$('input[name="'+el+'"]').attr('type', 'password');
			});
		}
	}
});

// button footer
var btn_footer = 
{
	check_all : ' <button class="btn btn-primary" title="check all" onclick="check_all()"><i class="fa fa-check"></i></button> ',
	history_back : ' <button class="btn btn-primary btn_history_back" title="back"><i class="fa fa-arrow-left"></i></button> ',
	init_module : ' <button class="btn btn-primary setting_btn_init_module">Init Module</button> ',
	reset_module : ' <button class="btn btn-danger setting_btn_reset_data">Reset Data</button>',
	bulk_action_disable : ' <button class="btn btn-warning bulk_action" action="disable" title="disable"><i class="fa fa-times-circle"></i></button> ',
	bulk_action_enable : ' <button class="btn btn-success bulk_action" action="enable" title="enable"><i class="fa fa-check-circle"></i></button> ',
	bulk_action_delete : ' <button class="btn btn-danger  bulk_action" action="delete" title="delete"><i class="fa fa-trash"></i></button> ',
	bulk_action_restore : ' <button class="btn bg-olive bulk_action" action="restore" title="restore"><i class="fa fa-undo"></i></button> ',
	bulk_action_force_delete : ' <button class="btn btn-danger bulk_action" action="force delete" title="force delete"><i class="fa fa-trash"></i></button >'
}

// Datatable Export Option
var dt_export_options = function()
{
	var return_config = 
	{
		exportOptions:
		{
			modifier:
			{
				selected:true,
			},
			columns: ':visible',
		}
    }
    return return_config
}

// Data Table Dom (Dom Position)
var datatable_dom 		= 
"<'row'<'col-sm-4 dt_length'l>>"+
"<'row'<'col-sm-8'B> <'col-sm-4'f>>"+
"<'row'<'col-sm-12'tr>>"+
"<'row'<'col-sm-6 col-md-6 col-lg-4'i><'col-sm-6 col-md-6 col-lg-8'>>"+
"<'row'<'col-sm-12 col-lg-7'<'pull-right'p>>>";

var datatable_buttons 	= 
[
	// Button Print
	$.extend(true,{}, dt_export_options,{
		extend: 'print',
		text:'print <i class="fa fa-print"></i>',
		exportOptions:
		{
			columns: ':visible'
		}
	}),

	// Button Copy
	$.extend(true,{}, dt_export_options,{
		extend: 'copy',
		text:'copy <i class="fa fa-copy"></i>',
		exportOptions:
		{
			columns: ':visible'
		}
	}),

	// Button PDF
	$.extend(true,{}, dt_export_options,{
		extend: 'pdfHtml5',
		text:'pdf <i class="fa fa-file-pdf-o"></i>',
		exportOptions:
		{
			columns: ':visible'
		},
		action:function(e, dt, node, config)
		{
			DataTable_Custom.ExportFileName(this,e, dt, node, config,'pdfHtml5');
		}
	}),

	// Button Excel
	$.extend(true,{}, dt_export_options,{
		extend: 'excelHtml5',
		text:'excel <i class="fa fa-file-excel-o"></i>',
		exportOptions:
		{
			columns: ':visible'
		},
		action:function(e, dt, node, config)
		{
			DataTable_Custom.ExportFileName(this,e, dt, node, config,'excelHtml5');
		}
	}),

	// Button CSV
	$.extend(true,{}, dt_export_options,{
		extend: 'csv',
		text:'csv',
		exportOptions:
		{
			columns: ':visible'
		},
		action:function(e, dt, node, config)
		{
			DataTable_Custom.ExportFileName(this,e, dt, node, config,'csv');
		}
	}),

	// Button Column Visible
	{
		extend:'colvis',
		text:'column <i class="fa fa-columns"></i>'
	}
]

// Data Table Default Init
var datatable = $('.datatable').DataTable({
	lengthMenu: [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
	responsive:true,
	dom: datatable_dom,
	buttons:datatable_buttons
});

// Data Table Server Side
var datatable_server_side = function(ajax_param,datatable_param,drawCallback){
	ajax_param.dataFilter = function(data)
	{
		var json = jQuery.parseJSON(data);
		json.recordsTotal		= json.record_total;
		json.recordsFiltered	= json.record_filtered;
		return JSON.stringify(json);
    }

    // ajax_param.headers = {'CIMS-TOKEN':token};
	$('.datatable_server_side').DataTable({
		lengthMenu: [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
		processing: true,
		fixedHeader: true,
		serverSide: ajax_param.ajax,
		responsive: true,
		searchable:false,
		ajax: ajax_param,
		columns: datatable_param.columns,
		dom: datatable_dom,
		columnDefs:datatable_param.columnDefs,
		buttons:datatable_buttons,
		initComplete: function()
		{
			var api = this.api();
			api.columns().every(function()
			{
				var that = this;
				$('.footer_search', this.footer()).on('keyup change', function(){
					if (that.search() !== this.value)
					{
						that.search(this.value).draw()
					}
				})
			})
		},
		drawCallback:function(dcparam)
		{
			var api = this.api();
			if(drawCallback !== undefined)
			{
				if(typeof drawCallback == 'function')
				{
					drawCallback(ajax_param,api);
				}
				else
				{
					eval(drawCallback+'(ajax_param,api)');
				}
			}
		}
	})
}

// App Init Onload
$(document).ready(function(){
	icheck_init()

	// Datatable Server Side Footer Search
	$('.datatable_server_side tfoot th').each(function(data){
		var length = $('.datatable_server_side tfoot th').length;
		if(data !== 0 && data !== 1 && data !== length-1)
		{
			var title = $(this).text();
			$(this).html('<input type="text" placeholder="'+title+'" class="form-control footer_search input-xs" style="height:24px;">');
		}	
	});
});

// Datatable Select Data Length
$('select[name="DataTables_Table_0_length"]').on('change',function(event){
	event.preventDefault();
	icheck_init('flat_green').on('ifChecked', function(event){datatable.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){datatable.row($(this).parents('tr')).deselect()});
});

// Datatable Paginate
$(document).on('click', '.paginate_button', function(event){
	event.preventDefault();
	icheck_init('flat_green').on('ifChecked', function(event){datatable.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){datatable.row($(this).parents('tr')).deselect()});
});

// Datatable Search
$('input[type="search"]').on('keyup',function(event){
	event.preventDefault();
	icheck_init('flat_green').on('ifChecked', function(event){datatable.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){datatable.row($(this).parents('tr')).deselect()});
});

// Datatable Thead
$('table thead').on('click',function(event){
	event.preventDefault();
	icheck_init('flat_green').on('ifChecked', function(event){datatable.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){datatable.row($(this).parents('tr')).deselect()});
});

// Datatable Change Visible Column
datatable.on( 'column-visibility.dt',function(e,settings,column,state){
	icheck_init('flat_green').on('ifChecked', function(event){datatable.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){datatable.row($(this).parents('tr')).deselect()});
});
// Bull Option Check
var bulk_option = document.getElementsByClassName('bulk_option');

// iCheck Check All
function check_all()
{
	for(i = bulk_option.length; i--;)
	{
		if(bulk_option[i].checked == true)
		{
			bulk_option[i].checked = false;
			$('.bulk_option').iCheck('update')[i].checked = false;
			datatable.row($(bulk_option[i]).parents('tr')).deselect();
		}
		else
		{
			bulk_option[i].checked = true;			
			$('.bulk_option').iCheck('update')[i].checked = true;
			datatable.row($(bulk_option[i]).parents('tr')).select();
		}
    }
}

// iCheck init
function icheck_init(callback)
{
	var flat_green = 
	$('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass   : 'iradio_flat-green',
	});

	var flat_red = 
	$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
		checkboxClass: 'icheckbox_flat-red',
		radioClass   : 'iradio_flat-red',
	});

	var flat_blue = 
	$('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
		checkboxClass: 'icheckbox_flat-blue',
		radioClass   : 'iradio_flat-blue',
	});

	if(callback !== undefined)
	{
		return eval(callback);
	}
}