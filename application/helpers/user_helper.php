<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @method is administrator
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/
function is_admin()
{
	$ci =& get_instance();
	$user_session = $ci->session->userdata('user_id');

	if(!empty($user_session))
	{
		$ci->load->model([
			'bos/user_model' => 'user'
		]);

		$find = $ci->user->find($user_session);
		return (!empty($find))?($find->level == 'admin'):FALSE;
	}
}

/**
 * @method is logged in
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/
function is_logged_in()
{
	$ci =& get_instance();
	$user_session = $ci->session->userdata('user_id');

	if(!empty($user_session))
	{
		$ci->load->model([
			'bos/user_model' => 'user'
		]);

		$find = $ci->user->find($user_session);
		return (!empty($find))?redirect(base_url(),'refresh'):FALSE;
	}
}

/**
 * @method is me
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/
function is_me($user_id=null)
{
	$ci =& get_instance();
	$user_session = $ci->session->userdata('user_id');
	return ($user_id == $user_session);
}

/**
 * @method is have access
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/
function is_have_access()
{
	$ci =& get_instance();
}


/* End of file user_helper.php */
/* Location: ./application/helpers/user_helper.php */