<div class="row">
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3 id="sekolah_count">0</h3>
				<p>Sekolah</p>
			</div>
			<div class="icon"><i class="fa fa-university"></i></div>
			<a href="<?php echo(base_url('app/bos/sekolah')) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3 id="program_count">0</h3>
				<p>Program</p>
			</div>
			<div class="icon"><i class="ion ion-bag"></i></div>
			<a href="<?php echo(base_url('app/bos/program')) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3 id="kegiatan_count">0</h3>
				<p>Kegiatan</p>
			</div>
			<div class="icon"><i class="ion ion-bag"></i></div>
			<a href="<?php echo(base_url('app/bos/kegiatan')) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3 id="rka_count">0</h3>
				<p>RKA</p>
			</div>
			<div class="icon"><i class="ion ion-bag"></i></div>
			<a href="<?php echo(base_url('app/bos/rka')) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	// Set Active Parent Menu
	$.each($('.treeview'),function(index, el){
		if($(el).attr('module') == 'bos')
		{
			$(el).addClass('active')
		}
	});

	// Set Active Sub Menu
	$.each($('ul.treeview-menu'),function(index, el){
		if($(el).attr('parent_module') == 'bos')
		{
			$.each($(el).children('li'),function(index, el){
				if($(el).attr('sub_module') == 'dashboard')
				{
					$(el).addClass('active')
				}
			});
		}
	});

	$.ajax({
		url: '<?php echo(base_url("bos")) ?>',
		dataType: 'json',
		success:function(data)
		{
			$('#sekolah_count').text(data.data.sekolah.count)
			$('#program_count').text(data.data.program.count)
			$('#kegiatan_count').text(data.data.kegiatan.count)
			$('#rka_count').text(data.data.rka.count)
		},
		error:function(error)
		{
			console.log(error)
		}
	})
});
</script>