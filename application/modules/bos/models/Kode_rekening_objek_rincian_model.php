<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category MODEL
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Kode_rekening_objek_rincian_model extends Model
{
	use SoftDeletes;
	public $timestamps	= true;
	
	protected $table = TablePrefix.'kode_rekening_objek_rincian';
	protected $guarded	= [];
	protected $hidden 	= [];
	protected $fillable = [];
	protected $connection = ENVIRONMENT;
}
/* End of file Kode_rekening_objek_rincian_model.php */
/* Location: ./application/modules/bos/models/Kode_rekening_objek_rincian_model.php */