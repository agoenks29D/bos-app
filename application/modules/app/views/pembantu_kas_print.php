<div class="box" id="print_page">
	<div class="box-header with-border">
		 <h3 class="box-title">
		 	Pembantu Kas <?php echo ucfirst(month_indonesia($pembantu_kas->bulan));?>
		 	<?php //echo nice_date($pembantu_kas->created_at,'d/M/Y'); ?>
		 </h3>
	</div>
	<div class="box-body">
		<div class="col-lg-6">
			<table class="table table-striped table-condensed" cellspacing="0" width="50%">
				<tr>
					<td>Nama Sekolah</td>
					<td>: <?php echo $pembantu_kas->sekolah()->nama; ?></td>
				</tr>
				<tr>
					<td>Desa/Kecamatan</td>
					<td>: <?php echo $pembantu_kas->sekolah()->kecamatan; ?></td>
				</tr>
				<tr>
					<td>Kabupaten/Kota</td>
					<td>: <?php echo $pembantu_kas->sekolah()->kabupaten; ?></td>
				</tr>
				<tr>
					<td>Provinsi</td>
					<td>: <?php echo $pembantu_kas->sekolah()->provinsi; ?></td>
				</tr>
			</table>
		</div>
		<table class="table table-striped table-responsive table-hover table-bordered datatable_server_side" cellspacing="0" width="100%">
			<thead>
				<th>No</th>
				<th>Kode Rekening</th>
				<th>Nomor Bukti</th>
				<th>Uraian</th>
				<th>Pemasukan</th>
				<th>Pengeluaran</th>
				<th>Saldo</th>
			</thead>
			<tbody>
				<?php 
				$saldo = (!empty($latest_pembantu_kas))?$latest_pembantu_kas->saldo:0;
				foreach ($pembantu_kas->uraian() as $key => $uraian)
				{
					echo '<tr>';
					echo '<td>'.($key+1).'</td>';
					echo '<td>'.$uraian->kode_rekening.'</td>';
					echo '<td>'.$uraian->nomor_bukti.'</td>';
					echo '<td>'.$uraian->uraian.'</td>';
					echo '<td>'.$uraian->pemasukan.'</td>';
					echo '<td>'.$uraian->pengeluaran.'</td>';
					echo '<td>'.((!empty($uraian->pemasukan))?$saldo = $saldo+$uraian->pemasukan:(!empty($uraian->pengeluaran))?$saldo = $saldo-$uraian->pengeluaran:FALSE).'</td>';
					echo '</tr>';
				}
				?>
			</tbody>
		</table>
		<div class="col-lg-6" style="margin-top:6%;float: left;">
			Menyetujui,<br>
			Kepala sekolah <?php echo $pembantu_kas->sekolah()->nama; ?>
			<br><br><br><br>
			<?php
			if(!empty($footer_role['kepala_sekolah']))
			{
				$user = $this->user->find($footer_role['kepala_sekolah']['user_id']);
				echo (!empty($user))?$user->full_name.'<br>'.$user->nip:false;
			}
			?>
		</div>
		<div class="col-lg-6" style="margin-top:6%;float: right;">
			Kabanjahe <br> Bendahara Dana BOS
			<br><br><br><br>
			<?php
			if(!empty($footer_role['bendahara']))
			{
				$user = $this->user->find($footer_role['bendahara']['user_id']);
				echo (!empty($user))?$user->full_name.'<br>'.$user->nip:false;
			}
			?>
		</div>
	</div>
</div>

<div class="box">
	<div class="box-body">
		<a onclick="App.print_page('<?php echo(base_url("app/bos/pembantu_kas/".$pembantu_kas->id."/print")) ?>')" class="btn btn-success btn-sm pull-left">
			<i class="fa fa-print"></i> Print
		</a>
	</div>
</div>