<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category CORE
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/

/* load rest controller library */
require(APPPATH.'libraries/rest/REST_Controller.php');

/**
 * core rest api
*/
class Rest_api extends REST_Controller
{
	/**
	 * constructor class
	*/
	public function __construct()
	{
		parent::__construct();
		$this->module_privilege();
	}

	public function module_privilege()
	{
		// $response = ['status' => 'success','message' => $this->router->method];
		// $this->response($response,REST_Controller::HTTP_OK);
	}
}

/* End of file Rest_api.php */
/* Location: ./application/core/Rest_api.php */
