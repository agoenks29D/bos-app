<!-- list RKA -->
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs pull-right">
				<li class="pull-left header">RKA <span class="subtitle_header"></span></li>
				<?php
				if(is_admin())
				{
					echo '<li><a href="#tab_list_data" class="tab_list_trashed" data-toggle="tab"><i class="fa fa-trash"></i> Trash</a></li>';
				}
				?>
				<li class="active"><a href="#tab_list_data" class="tab_list_data" data-toggle="tab"><i class="fa fa-list"></i> List</a></li>
				<li><a href="#tab_new_data" title="tambah RKA" data-toggle="modal" data-target="#modal_add"><i class="fa fa-plus"></i> New</a></li>
			</ul>
			<div class="tab-content no-padding">
				<div class="tab-pane active" id="tab_list_data">
					<div class="box-body">
						<?php echo $table; ?>
					</div>
				</div>
				<div class="box-footer"></div>
			</div>
		</div>
	</div>
</div>

<!-- modal add rka  -->
<div class="modal fade" id="modal_add">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="TambahRKA">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tambah RKA</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Urusan Pemerintahan</label>
						<input type="text" name="urusan_pemerintahan" placeholder="Urusan Pemerintahan" class="form-control">
					</div>
					<div class="form-group">
						<label>Organisasi</label>
						<input type="text" name="organisasi" placeholder="Organisasi" class="form-control">
					</div>
					<div class="form-group">
						<label>Jumlah Dana</label>
						<input type="text" name="jumlah_dana" placeholder="Jumlah Dana" class="form-control">
					</div>
					<div class="form-group">
						<label>Program</label>
						<select class="form-control select_program" name="program" style="width: 100%;"></select>
					</div>
					<div class="form-group">
						<label>Kegiatan</label>
						<select class="form-control select_kegiatan" name="kegiatan" style="width: 100%;"></select>
					</div>
					<div class="form-group">
						<label>Sasaran Kegiatan</label>
						<input type="text" name="sasaran_kegiatan" placeholder="Sasaran Kegiatan" class="form-control" id="SasaranKegiatan">
					</div>
					<div class="form-group">
						<label>Lokasi Kegiatan</label>
						<select class="form-control select_sekolah" name="sekolah_id" style="width: 100%;"></select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- modal edit rka -->
<div class="modal fade" id="modal_edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="EditRKA">
				<input type="hidden" id="EditRKAId">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Edit RKA</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Urusan Pemerintahan</label>
						<input type="text" name="urusan_pemerintahan" placeholder="Urusan Pemerintahan" class="form-control" id="EditRkaUrusanPemerintahan">
					</div>
					<div class="form-group">
						<label>Organisasi</label>
						<input type="text" name="organisasi" placeholder="Organisasi" class="form-control" id="EditRkaOrganisasi">
					</div>
					<div class="form-group">
						<label>Jumlah Dana</label>
						<input type="text" name="jumlah_dana" placeholder="Jumlah Dana" class="form-control" id="EditRkaJumlahDana">
					</div>
					<div class="form-group">
						<label>Program</label>
						<select class="form-control select_program" name="program" style="width: 100%;" id="EditRkaProgram"></select>
					</div>
					<div class="form-group">
						<label>Kegiatan</label>
						<select class="form-control select_kegiatan" name="kegiatan" style="width: 100%;" id="EditRkaKegiatan"></select>
					</div>
					<div class="form-group">
						<label>Sasaran Kegiatan</label>
						<input type="text" name="sasaran_kegiatan" placeholder="Sasaran Kegiatan" class="form-control" id="EditRkaSasaranKegiatan">
					</div>
					<div class="form-group">
						<label>Lokasi Kegiatan</label>
						<select class="form-control select_sekolah" name="sekolah_id" style="width: 100%;" id="EditRkaLokasiKegiatan"></select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
var sekolah_ids = JSON.parse(localStorage.getItem('sekolah_ids'));
// Run On Loaded
$(document).ready(function(){
	// Draw Table
	Rka.draw_table({ajax:true,sekolah_ids:sekolah_ids})

	// Button Footer
	$('.box-footer').html(btn_footer.check_all+btn_footer.bulk_action_delete);

	// List Data
	$('.tab_list_data').click(function(event) {
		Rka.draw_table({ajax:true,sekolah_ids:sekolah_ids});
		$('.subtitle_header').text('list');
		$('.box-footer').html(btn_footer.check_all+btn_footer.bulk_action_delete);
	});

	// List Data In Trash
	$('.tab_list_trashed').click(function(event) {
		Rka.draw_table({only_trash:true});
		$('.subtitle_header').text('in trash');
		$('.box-footer').html(btn_footer.check_all+' '+btn_footer.bulk_action_restore+' '+btn_footer.bulk_action_force_delete);
	});

	// Set Active Parent Menu
	$.each($('.treeview'),function(index, el){
		if($(el).attr('module') == 'tata_usaha')
		{
			$(el).addClass('active')
		}
	});

	// Set Active Sub Menu
	$.each($('ul.treeview-menu'),function(index, el){
		if($(el).attr('parent_module') == 'tata_usaha')
		{
			$.each($(el).children('li'),function(index, el){
				if($(el).attr('sub_module') == 'rka')
				{
					$(el).addClass('active')
				}
			});
		}
	});
});

$('.select_sekolah').select2({
	placeholder:'Pilih Lokasi Kegiatan',
	allowClear: false,
	ajax:
	{
		url: '<?php echo base_url("bos/sekolah");?>',
		dataType: 'json',
		delay: 250,
		type:'GET',
		data: function (params,data)
		{
			var request_data =
			{
				ajax:true,
				sekolah_ids:sekolah_ids,
				search:
				{
					value:params.term
				},
				length:10,
				start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
				order:
				{
					column:'id',
					type:'asc'
				}
			}
			return request_data;
		},
		processResults: function (data, params,x)
		{
			params.page = params.page || 1;
			var result_data = 
			{
				results: data.data,
				pagination:
				{
					more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
				}
			}
			return result_data
		}
	},
	escapeMarkup: function (markup)
	{
		return markup;
	},
	templateResult: function(data)
	{
		if(data.loading)
		{
			return data.text;
		}
		return '<div>'+data.nama+'</div>';
	},
	templateSelection: function (data, container)
	{
		return data.nama || data.text;
	}
});

$('.select_program').select2({
	placeholder:'Pilih Program',
	allowClear: false,
	ajax:
	{
		url: '<?php echo base_url("bos/program");?>',
		dataType: 'json',
		delay: 250,
		type:'GET',
		data: function (params,data)
		{
			var request_data =
			{
				ajax:true,
				search:
				{
					value:params.term
				},
				length:10,
				start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
				order:
				{
					column:'id',
					type:'asc'
				}
			}
			return request_data;
		},
		processResults: function (data, params,x)
		{
			params.page = params.page || 1;
			var result_data = 
			{
				results: data.data,
				pagination:
				{
					more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
				}
			}
			return result_data
		}
	},
	escapeMarkup: function (markup)
	{
		return markup;
	},
	templateResult: function(data)
	{
		if(data.loading)
		{
			return data.text;
		}
		return '<div>'+data.nama+'</div>';
	},
	templateSelection: function (data, container)
	{
		$('#SasaranKegiatan').val(data.kelompok_sasaran);
		return data.nama || data.text;
	}
});

// Select Kegiatan On Selected Program
$('.select_program').on('select2:select', function (e) { 
	var program_id = $(this).val();
	$('.select_kegiatan').select2({
		placeholder:'Pilih Kegiatan',
		allowClear: false,
		ajax:
		{
			url: '<?php echo base_url("bos/kegiatan");?>',
			dataType: 'json',
			delay: 250,
			type:'GET',
			data: function (params,data)
			{
				var request_data =
				{
					ajax:true,
					program_id:program_id,
					search:
					{
						value:params.term
					},
					length:10,
					start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
					order:
					{
						column:'id',
						type:'asc'
					}
				}
				return request_data;
			},
			processResults: function (data, params,x)
			{
				params.page = params.page || 1;
				var result_data = 
				{
					results: data.data,
					pagination:
					{
						more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
					}
				}
				return result_data
			}
		},
		escapeMarkup: function (markup)
		{
			return markup;
		},
		templateResult: function(data)
		{
			if(data.loading)
			{
				return data.text;
			}
			return '<div>'+data.nama+'</div>';
		},
		templateSelection: function(data)
		{
			return data.nama || data.text
		}
	});
});

// bulk action
$(document).on('click', '.bulk_action', function(event){
	event.preventDefault();
	var checked = [];
	var action 	= $(this).attr('action');
	var title 	= action;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});
    $.each(bulk_option,function(index, el){
    	if(el.checked == true)
    	{
    		checked.push(el.value);
    	}
    });

	swal({
		title: 	title+' rka',
		text: 	"Are you sure,want to "+action+" checked rka?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#d33',
		cancelButtonColor: '#3085d6',
		confirmButtonText: "Yes, "+action+" it!"
	}).then(function(){
		if(checked !== false)
		{
			if($.isEmptyObject(checked))
			{
				swal("Oops...", "No checked found", "error");
			}
			else
			{
				Rka.bulk_action({id:checked,action:action},function(option,data){
					switch(option.data.action)
					{
						case 'restore':
							Rka.draw_table({only_trash:true});
						break;

						case 'force delete':
							Rka.draw_table({ajax:true,only_trash:true});
						break;

						default :
							Rka.draw_table({});
						break;
					}
				})
			}
		}
	},(dismiss)=>{});
});

/* Rka Class */
class Rka
{
	static bulk_action(option,callback)
	{
		App.ajax_request({url: '<?php echo base_url("bos/bulk_action_rka") ?>',type: 'POST',dataType: 'json',data:option},callback)
	}

	static draw_table(option)
	{
		DataTable_Custom.destroy_datatable();
		datatable_server_side(
		{
			url:'<?php echo base_url("bos/rka")?>',
			type:'GET',
			data:option
		},
		{
			columns:
			[
				{
					data:'id',render:function (data, type, full, meta)
					{
						return '<input type="checkbox" class="bulk_option flat-green" name="bulk_check[]" value="'+data+'"> ';
					}
				},
				{
					data:'id',render:function (data, type, full, meta)
					{
						return (meta.row+1);
					}
				},
				{data:'sekolah.nama'},
				{data:'program'},
				{data:'kegiatan'},
				{data:'jumlah_dana'},
				{
					data:'status',render:function(data, type, full, meta)
					{
						switch(data)
						{
							case 0:
								return '<button class="btn btn-xs btn-warning btn_status_data" status="draft" data_id="'+full.id+'">draft</button>';
							break;

							case 1:
								return '<button class="btn btn-xs btn-success btn_status_data" status="publish" data_id="'+full.id+'">publish</button>';
							break;
						}
					}
				},
				{
					data:'id',render:function (data, type, full, meta)
					{
						var btn_delete = (!$.isEmptyObject(option) && typeof option.only_trash !== 'undefined')?'force delete':'delete';
						var btn_detail = (!$.isEmptyObject(option) && typeof option.only_trash !== 'undefined')?'detail_trashed':'detail';
						var btn_setup = (typeof option.only_trash == 'undefined')?
						'<a href="<?php echo base_url("app/bos/rka/")?>'+data+'" class="btn_option btn btn-xs btn-default"><i class="fa fa-file-o"></i></a>':'';

						var btn_edit = (typeof option.only_trash == 'undefined')?
						'<button class="btn_option btn btn-xs btn-default" title="edit" data_id="'+data+'" option="edit"><i class="fa fa-edit"></i></button>':'';
						
						var html = 
						btn_setup+
						btn_edit+
						'<button class="btn_option btn btn-xs btn-danger" title="delete" data_id="'+data+'" option="'+btn_delete+'"><i class="fa fa-trash"></i></button> '+
						'<button class="btn_option btn btn-xs btn-info" title="detail" data_id="'+data+'" option="'+btn_detail+'"><i class="fa fa-search"></i></button>';
						return html;
					}
				}
			],
			columnDefs:
			[
				{
					defaultContent:null,
					searchable: false,
					orderable: false,
					targets: 0
				},
				{
					defaultContent:null,
					searchable: false,
					orderable: false,
					targets: 1
				}
			]
		},
		function(ajax_param,api){
			datatable = api;
			$(this).on('column-visibility.dt', function(e,settings,column,state){
			icheck_init('flat_green').on('ifChecked', function(event){api.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){api.row($(this).parents('tr')).deselect()})})
			icheck_init('flat_green').on('ifChecked', function(event){api.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){api.row($(this).parents('tr')).deselect()})
		})
	}
}

// Button Option
$(document).on( 'click', 'button.btn_option',function(){
	var data_id = $(this).attr('data_id');
	var option = $(this).attr('option');
	var title 	= option;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});

	switch(option)
	{
		// button option edit
		case 'edit':
			$('#EditRKAId').val(data_id);
			App.ajax_request({
				url: '<?php echo base_url("bos/rka/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
			},function(ajax_param,data){
				var program_id = data.data.program_id;
				if(data.status == 'success')
				{
					var allow_edit = data.data.allow_edit;
					if(allow_edit == 0)
					{
						if("<?php echo (is_admin())?'admin':'user';?>" !== "admin")
						{
							swal({ type: 'error',title: 'Oops...',text: 'This data not editable'})
						}
						else
						{
							allow_edit = true;
						}
					}
					else
					{
						allow_edit = true;
					}

					if(allow_edit)
					{
						App.ajax_request({
							url: '<?php echo base_url("bos/program/");?>'+program_id,
							type: 'GET',
							dataType: 'json'
						},function(ajax_param,data){
							$('.select_program').select2('trigger', 'select',{data:{id:program_id,text:data.data.nama }});
						})
						$('#EditRkaUrusanPemerintahan').val(data.data.urusan_pemerintahan)
						$('#EditRkaOrganisasi').val(data.data.organisasi)
						$('#EditRkaJumlahDana').val(data.data.jumlah_dana)
						$('#EditRkaSasaranKegiatan').val(data.data.sasaran_kegiatan)
						// Set Select2 program
						App.ajax_request({
						url: '<?php echo base_url("bos/program/");?>'+data.data.program_id,
							type: 'GET',
							dataType: 'json'
						},function(ajax_param,data){
							if(data.status == 'success')
							{
								$('#EditRkaProgram').select2('trigger', 'select',{data:{id:data.data.id,text:data.data.nama }});
							}
						})

						// Set Select2 kegiatan
						App.ajax_request({
						url: '<?php echo base_url("bos/kegiatan/");?>'+data.data.kegiatan_id,
							type: 'GET',
							dataType: 'json'
						},function(ajax_param,data){
							if(data.status == 'success')
							{
								$('#EditRkaKegiatan').select2('trigger', 'select',{data:{id:data.data.id,text:data.data.nama }});
							}
						})

						// Set Select2 Lokasi Kegiatan
						App.ajax_request({
						url: '<?php echo base_url("bos/sekolah/");?>'+data.data.sekolah_id,
							type: 'GET',
							dataType: 'json'
						},function(ajax_param,data){
							if(data.status == 'success')
							{
								$('#EditRkaLokasiKegiatan').select2('trigger', 'select',{data:{id:data.data.id,text:data.data.nama }});
							}
						})

						$('#modal_edit').modal('show');
					}
				}
			})
		break;

		// button option detail
		case 'detail':

			App.ajax_request({
				url: '<?php echo base_url("bos/rka/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
				data:{only_trash:false},
			},function(ajax_param,data){

				var status = (data.data.status == 0)?'<button class="btn btn-warning">draft</button>':'<button class="btn btn-success">publish</button>';

				App.ajax_request({
					url: '<?php echo base_url("bos/sekolah/");?>'+data.data.sekolah_id,
					type: 'GET',
					dataType: 'json',
					data:{only_trash:false},
					},function(ajax_param,sekolah){
					swal({
						title: 'Detail RKA',
						type: 'info',
						html:
							'<table class="table table-striped table-responsive table-hover">'+
								'<tr>'+
									'<td>Sekolah </td><td>'+sekolah.data.nama+'</td>'+
								'</tr>'+
								'<tr>'+
									'<td>Program </td><td>'+data.data.program+'</td>'+
								'</tr>'+
								'<tr>'+
									'<td>Kegiatan </td><td>'+data.data.kegiatan+'</td>'+
								'</tr>'+
								'<tr>'+
									'<td>Jumlah Dana </td><td>'+data.data.jumlah_dana+'</td>'+
								'</tr>'+
								'<tr>'+
									'<td>Status </td><td>'+status+'</td>'+
								'</tr>'+
							'</table>',
						showCloseButton: true
					})
				})
			})
		break;

		// button option detail trashed
		case 'detail_trashed':
			App.ajax_request({
				url: '<?php echo base_url("bos/rka/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
				data:{only_trash:true},
			},function(ajax_param,data){
				swal({
					title: 'Detail Kegiatan '+data.data.nama,
					type: 'info',
					html:
						'<table class="table table-striped table-responsive table-hover">'+
							'<tr>'+
								'<td>Nama Kegiatan </td><td>'+data.data.nama+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Tanggal Pembuatan </td><td>'+data.data.created_at+'</td>'+
							'</tr>'+
						'</table>',
					showCloseButton: true
				})
			})
		break;

		// button option delete
		case 'delete':
			swal({
				title: 	title+' rka',
				text: 	"Are you sure,want to "+option+" rka?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, "+option+" it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/delete_rka/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(ajax_param,data){
					Rka.draw_table({ajax:true,sekolah_ids:sekolah_ids});
				})
			},(dismiss)=>{});
		break;

		// button option force delete
		case 'force delete':
			swal({
				title: 	title+' rka',
				text: 	"Are you sure,want to "+option+" rka?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, "+option+" it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/force_delete_rka/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(ajax_param,data){
					Rka.draw_table({ajax:true,only_trash:true});
				})
			},(dismiss)=>{});
		break;

		default:
		break;
	}
})

// Tambah Kegiatan
$("#TambahRKA").on('submit',(function(e){
	e.preventDefault();
	App.ajax_request({
		url: '<?php echo base_url("bos/add_rka");?>',
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this)
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			$('#TambahRKA')[0].reset()
			swal({
				position: 'top-end',
				type: 'success',
				title:'RKA berhasil ditambahkan',
				showConfirmButton: false,
				timer: 2000
			}).then(()=>{},(dismiss)=>{})
			Rka.draw_table({ajax:true,sekolah_ids:sekolah_ids})
		}
	})
}))

// Set Status Data
$(document).on('click','.btn_status_data',function(){
	if("<?php echo (is_admin())?'admin':'user';?>" == "admin")
	{
		var status = $(this).attr('status');
		var data_id = $(this).attr('data_id');
		switch(status)
		{
			case 'publish':
				App.ajax_request({
					url: '<?php echo base_url("bos/set_status_rka/");?>'+data_id+'/draft',
					type: 'GET',
					dataType: 'json'
				},function(ajax_param,data){
					Rka.draw_table({ajax:true,sekolah_ids:sekolah_ids})
				})
			break;

			case 'draft':
				App.ajax_request({
					url: '<?php echo base_url("bos/set_status_rka/");?>'+data_id+'/publish',
					type: 'GET',
					dataType: 'json'
				},function(ajax_param,data){
					Rka.draw_table({ajax:true,sekolah_ids:sekolah_ids})
				})
			break;

			default:
				swal({ type: 'error',title: 'Oops...',text: 'Status undefined'})
			break;
		}
	}
})

// Perbaharui Rka
$("#EditRKA").on('submit',(function(e){
	e.preventDefault();
	var data_id = $('#EditRKAId').val();
	App.ajax_request({
		url: '<?php echo base_url("bos/update_rka/");?>'+data_id,
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this)
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			$('#EditRKA')[0].reset()
			swal({
				position: 'top-end',
				type: 'success',
				title: 'RKA berhasil perbaharui',
				showConfirmButton: false,
				timer: 2000
			}).then(()=>{},(dismiss)=>{})
			Rka.draw_table({ajax:true,sekolah_ids:sekolah_ids})
		}
	})
	
}))
</script>