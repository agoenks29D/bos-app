<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category API
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
class File extends Rest_api
{
	/**
	 * constructor class
	*/
	public function __construct()
	{
		parent::__construct();
		$this->form_validation->set_data($this->post());
		$this->load->model([
			'file_model' => 'file'
		]);
	}

	/**
	 * get image file
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function image_get($unique_id=null,$resize_string=null)
	{
		$image = $this->file->where('id',$unique_id)->orWhere('slug',$unique_id)->first();
		if(!empty($image))
		{
			if(file_exists($image->path))
			{
				header('content-type:'.$image->mime);
				$resize = explode('x',$resize_string);
				$pathinfo = pathinfo($image->path);
				$image_path = realpath($image->path);

				$height = (isset($resize[0]))?$resize[0]:128;
				$width = (isset($resize[1]))?$resize[1]:128;
				if(!empty($resize_string))
				{
					if(in_array($pathinfo['extension'], ['jpg','jpeg']))
					{
						exit(imagejpeg($this->resize_image(imagecreatefromjpeg($image_path),$height,$width)));
					}
					elseif($pathinfo['extension'] == 'png')
					{
						exit(imagepng($this->resize_image(imagecreatefrompng($image_path),$height,$width)));
					}
				}
				else
				{
					exit(file_get_contents($image_path));
				}
			}
		}
		else
		{
			$this->response(['status' => 'failed','message' => 'data_not_found'],REST_Controller::HTTP_OK);
		}
	}

	/**
	 * @method resize image
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function resize_image($img, $box_width=128, $box_height=128)
	{
		$box_width = filter_var($box_width,FILTER_VALIDATE_INT);
		$box_height = filter_var($box_height,FILTER_VALIDATE_INT);
		$new = imagecreatetruecolor(($box_width)?$box_width:128, ($box_height)?$box_height:128);
		$new = imagecreatetruecolor($box_width,$box_height);
		if($new === false)
		{
			return null;
		}
		$fill = imagecolorallocate($new, 255, 255, 205);
		imagefill($new, 0, 0, $fill);
		
		$hratio = $box_height / imagesy($img);
		$wratio = $box_width / imagesx($img);
		$ratio = min($hratio, $wratio);

		if($ratio > 1.0)$ratio = 1.0;

		$sy = floor(imagesy($img) * $ratio);
		$sx = floor(imagesx($img) * $ratio);

		$m_y = floor(($box_height - $sy) / 2);
		$m_x = floor(($box_width - $sx) / 2);
		if(!imagecopyresampled($new, $img,$m_x, $m_y,0, 0,$sx, $sy,imagesx($img), imagesy($img)))
		{
		
			imagedestroy($new);
			return null;
		}
		return $new;
	}

	/**
	 * @method validate modules folder
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function validate_modules_folder($path)
	{
		if(!empty($path))
		{
			(!is_dir($path))?mkdir($path,0777,TRUE):FALSE;
	    	return realpath($path);
		}
	}

	/**
	 * @method site data folder
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function site_data_folder()
	{
		if(file_exists(Modules::run('bos/setting/get_app_config')))
		{
			$app_config = json_decode(file_get_contents(Modules::run('bos/setting/get_app_config')));
			return $this->validate_modules_folder($app_config->site_folder);
		}
	}

	/**
	 * @method upload file
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function upload($fields_name=null,$config_param=null)
	{
		// Default Upload Config
	    $default['upload_path'] = $this->validate_modules_folder($this->site_data_folder().'/uploads/');
	    $default['allowed_types'] = 'jpg|jpeg|png|gif';
	    $default['max_size'] = 124000;
	    $default['max_width'] = 124000;
	    $default['max_height'] = 124000;
	    $default['min_width'] = 0;
	    $default['min_height'] = 0;
	    $default['max_filename'] = 0;
	    $default['max_filename_increment'] = 1000;
	    $default['encrypt_name'] = FALSE;
	    $default['file_name'] = NULL;
	    $default['file_ext_tolower'] = FALSE;
	    $default['remove_spaces'] = TRUE;
	    $default['detect_mime'] = TRUE;
	    $default['mod_mime_fix'] = TRUE;

	    // Load Codeigniter Upload Library
	    $this->load->library('upload');

	    // Initialize Default Config
	    $this->upload->initialize($default);

	    if(!is_array($fields_name))
	    {
			$config['upload_path'] = return_if_exists($config_param,'upload_path',$default['upload_path']);
			$config['allowed_types'] = return_if_exists($config_param,'allowed_types',$default['allowed_types']);
			$config['max_size'] = return_if_exists($config_param,'max_size',$default['max_size']);
			$config['max_width'] = return_if_exists($config_param,'max_width',$default['max_width']);
			$config['max_height'] = return_if_exists($config_param,'max_height',$default['max_height']);
			$config['min_width'] = return_if_exists($config_param,'min_width',$default['min_width']);
			$config['min_height'] = return_if_exists($config_param,'min_height',$default['min_height']);
			$config['max_filename'] = return_if_exists($config_param,'max_filename',$default['max_filename']);
			$config['max_filename_increment'] = return_if_exists($config_param,'max_filename_increment',$default['max_filename_increment']);
			$config['encrypt_name'] = return_if_exists($config_param,'encrypt_name',$default['encrypt_name']);
			$config['remove_spaces'] = return_if_exists($config_param,'remove_spaces',$default['remove_spaces']);
			$config['detect_mime'] = return_if_exists($config_param,'detect_mime',$default['detect_mime']);
			$config['mod_mime_fix'] = return_if_exists($config_param,'mod_mime_fix',$default['mod_mime_fix']);
			$config['file_ext_tolower'] = return_if_exists($config_param,'file_ext_tolower',$default['file_ext_tolower']);
			$config['file_name'] = return_if_exists($config_param,'file_name',$default['file_name']);
				
			// validate modules folder
			$this->validate_modules_folder($config['upload_path']);

			// Initialize Config
			$this->upload->initialize($config);

			// Upload File
			$upload[$fields_name] = $this->do_upload($fields_name,$this)[$fields_name];
			return $upload;
	    }
	}

	/**
	 * @method do upload single file or multiple file
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function do_upload($field_name,$ci)
	{
		// Upload File
		if(isset($_FILES[$field_name]))
		{
			if(is_array($_FILES[$field_name]['name']))
			{
				$file_count = count($_FILES[$field_name]['name']);
				for($i = 0; $i < $file_count; $i++)
				{
					$_FILES['file']['name'] = $_FILES[$field_name]['name'][$i];
					$_FILES['file']['type'] = $_FILES[$field_name]['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES[$field_name]['tmp_name'][$i];
					$_FILES['file']['error'] = $_FILES[$field_name]['error'][$i];
					$_FILES['file']['size'] = $_FILES[$field_name]['size'][$i];
					$data[$field_name][$i] = ($ci->upload->do_upload('file'))?$ci->upload->data():$ci->upload->display_errors();
				}
			}
			else
			{
				$data[$field_name] = ($ci->upload->do_upload($field_name))?$ci->upload->data():$ci->upload->display_errors();
			}
		}
		return (isset($data))?$data:false;
	}
}
/* End of file File.php */
/* Location: ./application/modules/bos/controllers/File.php */