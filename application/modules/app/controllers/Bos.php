<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category CONTROLLER
 * @author author_name <author@domain.com>
 * @version 0.1
*/
class Bos extends CI_Controller
{
	/**
	 * constructor class
	*/
	public function __construct()
	{
		parent::__construct();
		$this->template->set_base('base_template');
		if(empty($this->session->userdata('user_id')))
		{
			redirect(base_url('app/sign_in'));
		}
		$this->load->model(['bos/user_model' => 'user']);
	}

	/* bos dashboard */
	public function index()
	{
		$this->template->load(null,'bos_dashboard');
	}

	/* sekolah */
	public function sekolah($param0=null,$param1=null)
	{
		if(!empty($param0))
		{
			if($param0 == 'edit')
			{
				if(!empty($param1))
				{
					$this->template->load(null,'sekolah_edit');
				}
			}
			else
			{
				show_404();
			}
		}
		else
		{
			$data['table'] = $this->template->datatable(['#','No','NPSN','Nama','Jenis','Jumlah Siswa','Status','Option'],true);
			$this->template->load(null,'sekolah',$data);
		}
	}

	/* program */
	public function program()
	{
		$data['table'] = $this->template->datatable(['#','No','Nama','Kelompok Sasaran','Option'],true);
		$this->template->load(null,'program',$data);
	}

	/* kegiatan */
	public function kegiatan($param0=null,$param1=null)
	{
		$data['table'] = $this->template->datatable(['#','No','Nama','Option'],false);
		$this->template->load(null,'kegiatan',$data);
	}

	/* kode rekening */
	public function kode_rekening($param0=null)
	{
		$data['table'] = $this->template->datatable(['#','No','Nama','Akun','Kelompok','Jenis','Option'],true);
		$this->template->load(null,'kode_rekening',$data);
	}

	/* kode rekening objek */
	public function kode_rekening_objek($kode_rekening_id=null)
	{
		$data['table'] = $this->template->datatable(['#','No','Nama','Option'],true);
		$this->template->load(null,'kode_rekening_objek',$data);
	}

	/* kode rekening objek rincian */
	public function kode_rekening_objek_rincian($kode_rekening_objek_id=null)
	{
		$data['table'] = $this->template->datatable(['#','No','Nama','Option'],true);
		$this->template->load(null,'kode_rekening_objek_rincian',$data);	
	}

	/* sumber dana */
	public function sumber_dana($param0=null,$param1=null)
	{
		$this->template->load(null,'sumber_dana');
	}

	/* rka */
	public function rka($param0=null,$param1=null)
	{
		$this->load->model([
			'bos/rka_model' => 'rka',
			'bos/rka_uraian_model' => 'rka_uraian',
			'bos/rka_rincian_model' => 'rka_rincian',
			'bos/rka_uraian_rincian_model' => 'rka_uraian_rincian',
			'bos/kode_rekening_model' => 'kode_rekening',
			'bos/kode_rekening_objek_model' => 'kode_rekening_objek',
			'bos/kode_rekening_objek_rincian_model' => 'kode_rekening_objek_rincian'
		]);

		$jumlah_total = array();

		if(!empty($param0))
		{
			$rka = $this->rka->find($param0);
			if(!empty($rka))
			{
				foreach ($rka->uraian() as $uraian_key => $uraian)
				{
					foreach ($uraian->rincian() as $key => $rincian)
					{
						array_push($jumlah_total, $rincian->uraian_rincian()->sum('jumlah'));
					}
				}

				$data['rka_jumlah'] = array_sum($jumlah_total);
				$data['rka'] = $rka;
				$data['footer_role'] = 
				[
					'kepala_sekolah' => footer_role('rka',$param0,'kepala_sekolah'),
					'komite' => footer_role('rka',$param0,'komite'),
					'bendahara' => footer_role('rka',$param0,'bendahara')
				];
				if(!empty($param1))
				{
					switch ($param1)
					{
						case 'print':
							$this->template->load(null,'rka_print',$data);
						break;
						
						default:
							show_404();
						break;
					}
				}
				else
				{
					if($rka->status == 0)
					{
						$this->template->load(null,'rka_setup',$data);
					}
					else
					{
						$this->template->load(null,'rka_print',$data);
					}
				}
			}
			else
			{
				show_404();
			}
		}
		else
		{
			$data['table'] = $this->template->datatable(['#','No','Sekolah','Program','Kegiatan','Jumlah Dana','Status','Option'],true);
			$this->template->load(null,'rka',$data);
		}
	}

	/* pembantu kas */
	public function pembantu_kas($param0=null,$sekolah_id=null)
	{
		$this->load->model([
			'bos/pembantu_kas_model' => 'pembantu_kas',
			'bos/pembantu_kas_data_model' => 'pembantu_kas_data',
			'bos/sekolah_model' => 'sekolah'
		]);

		if(!empty($param0))
		{
			$pembantu_kas = $this->pembantu_kas->find($param0);
			if(!empty($pembantu_kas))
			{
				$latest_pembantu_kas = $this->pembantu_kas->where('sekolah_id',$sekolah_id)->orderBy('id','desc')->get();
				$data['pembantu_kas'] = $pembantu_kas;
				$data['latest_pembantu_kas'] = (isset($latest_pembantu_kas[1]))?$latest_pembantu_kas[1]:0;
				$data['table'] = $this->template->datatable(['#','No','Kode Rekening','Nomor Bukti','Uraian','Pemasukan','Pengeluaran','Option'],false);
				$data['footer_role'] = 
				[
					'kepala_sekolah' => footer_role('pembantu_kas',$param0,'kepala_sekolah'),
					'bendahara' => footer_role('pembantu_kas',$param0,'bendahara')
				];
				switch ($pembantu_kas->status)
				{
					case 0:
						$this->template->load(null,'pembantu_kas_setup',$data);
					break;
					
					default:
						$this->template->load(null,'pembantu_kas_print',$data);
					break;
				}
			}
			else
			{
				show_404();
			}
		}
		else
		{
			$data['table'] = $this->template->datatable(['#','No','Sekolah','Bulan','Tahun','Saldo','Status','Option'],false);
			$this->template->load(null,'pembantu_kas',$data);
		}
	}

	/* pembantu bank */
	public function pembantu_bank($param0=null,$sekolah_id=null)
	{
		$this->load->model([
			'bos/pembantu_bank_model' => 'pembantu_bank',
			'bos/pembantu_bank_data_model' => 'pembantu_bank_data',
			'bos/sekolah_model' => 'sekolah'
		]);

		if(!empty($param0))
		{
			$pembantu_bank = $this->pembantu_bank->find($param0);
			if(!empty($pembantu_bank))
			{
				$data['pembantu_bank'] = $pembantu_bank;
				$data['latest_pembantu_bank'] = $this->pembantu_bank->where('sekolah_id',$sekolah_id)->orderBy('id','desc')->first();
				$data['table'] = $this->template->datatable(['#','No','Kode Rekening','Nomor Bukti','Uraian','Pemasukan','Pengeluaran','Option'],false);
				$data['footer_role'] = 
				[
					'kepala_sekolah' => footer_role('pembantu_bank',$param0,'kepala_sekolah'),
					'bendahara' => footer_role('pembantu_bank',$param0,'bendahara')
				];
				switch ($pembantu_bank->status)
				{
					case 0:
						$this->template->load(null,'pembantu_bank_setup',$data);
					break;
					
					default:
						$this->template->load(null,'pembantu_bank_print',$data);
					break;
				}
			}
			else
			{
				show_404();
			}
		}
		else
		{
			$data['table'] = $this->template->datatable(['#','No','Sekolah','Bulan','Tahun','Saldo','Status','Option'],false);
			$this->template->load(null,'pembantu_bank',$data);
		}
	}

	/* bku */
	public function bku($param0=null,$sekolah_id=null)
	{
		$this->load->model([
			'bos/bku_model' => 'bku',
			'bos/bku_data_model' => 'bku_data',
			'bos/sekolah_model' => 'sekolah'
		]);

		if(!empty($param0))
		{
			$bku = $this->bku->find($param0);
			if(!empty($bku))
			{
				$data['bku'] = $bku;
				$data['latest_bku'] = $this->bku->where('sekolah_id',$sekolah_id)->orderBy('id','desc')->first();
				$data['table'] = $this->template->datatable(['#','No','Kode Rekening','Nomor Bukti','Uraian','Pemasukan','Pengeluaran','Option'],false);
				$data['footer_role'] = 
				[
					'kepala_sekolah' => footer_role('bku',$param0,'kepala_sekolah'),
					'bendahara' => footer_role('bku',$param0,'bendahara')
				];
				switch ($bku->status)
				{
					case 0:
						$this->template->load(null,'bku_setup',$data);
					break;
					
					default:
						$this->template->load(null,'bku_print',$data);
					break;
				}
			}
			else
			{
				show_404();
			}
		}
		else
		{
			$data['table'] = $this->template->datatable(['#','No','Sekolah','Bulan','Tahun','Saldo','Status','Option'],false);
			$this->template->load(null,'bku',$data);
		}
	}

	/* penutupan kas */
	public function penutupan_kas()
	{
		$data['table'] = $this->template->datatable(['#','No','Sekolah','Tanggal','Penerimaan','Pengeluaran','Saldo Buku','Saldo Kas','Status','Option'],false);
		$this->template->load(null,'penutupan_kas',$data);	
	}

	/* SP2B */
	public function sp2b()
	{
		$this->template->load(null,'sp2b');
	}

	/* SP2B SD */
	public function sp2b_sd()
	{
		$this->template->load(null,'sp2b_setup');
	}

	/* SP2B SMP */
	public function sp2b_smp()
	{
		$this->template->load(null,'sp2b_setup');	
	}
}

/* End of file Bos.php */
/* Location: ./application/modules/app/controllers/Bos.php */