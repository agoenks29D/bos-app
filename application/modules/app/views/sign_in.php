<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>BOS | Log in</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/bootstrap/dist/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/font-awesome/css/fontawesome.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/Ionicons/css/ionicons.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/dist/css/AdminLTE.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/plugins/iCheck/square/blue.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/plugins/sweetalert2/dist/sweetalert2.min.css');?>">
</head>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="<?php echo base_url();?>"><b>BOS</b> Login</a>
		</div>
		<div class="login-box-body">
			<p class="login-box-msg">Sign in to start your session</p>
			<form id="sign_in" method="post">
				<div class="form-group has-feedback">
					<input type="text" class="form-control" placeholder="Email or Username" name="identity">
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" placeholder="Password" autocomplete="off" name="password">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-8">
						<div class="checkbox icheck">
							<label><input type="checkbox"> Remember Me</label>
						</div>
					</div>
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat submit">Sign In</button>
					</div>
				</div>
			</form>
			<a href="#">I forgot my password</a><br>
			<a href="register.html" class="text-center">Register a new membership</a>
		</div>
	</div>
	<script src="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/jquery/dist/jquery.min.js');?>"></script>
	<script src="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
	<script src="<?php echo base_url('assets/AdminLTE-2.4.8/plugins/iCheck/icheck.min.js');?>"></script>
	<script src="<?php echo base_url('assets/vendor/plugins/sweetalert2/dist/sweetalert2.all.min.js');?>"></script>
	<script>
		$(function(){
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%'
			});
		});
		$('#sign_in').on('submit',(event)  => {
			event.preventDefault();
			$('.submit').addClass('disabled');
			$.ajax({
				url: '<?php echo base_url("bos/user/sign_in");?>',
				type: 'POST',
				dataType: 'json',
				data: {
					identity:$('input[name="identity"]').val(),
					password:$('input[name="password"]').val()
				},
				cache:false,
				success:(data) => {
					$('.submit').removeClass('disabled');
					if(data.status == 'success')
					{
						if(typeof(Storage) !== undefined)
						{
							localStorage.setItem('user',JSON.stringify(data.data))
						}
						swal({
							type:'success',
							showConfirmButton:false,
							text:'Hello '+data.data.full_name,
							title:'authentication success'
						})
						setTimeout(() => window.location.href="<?php echo base_url(_function('router','module'));?>", 1000);
					}
					else
					{
						switch(data.message)
						{
							case 'authentication_failed':
								swal({type: 'error',title: 'Oops...',text: 'authentication failed!'})
							break;

							case 'data_not_found':
								swal({type: 'error',title: 'Oops...',text: 'email / username not found'})
							break;
						}
					}
				},
				error:(error) => {
					$('.submit').removeClass('disabled');
					console.log(error)
				}
			})
		})
	</script>
</body>
</html>
