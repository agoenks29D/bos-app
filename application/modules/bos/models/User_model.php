<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category MODEL
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class User_model extends Model
{
	use SoftDeletes;
	public $timestamps	= true;
	
	protected $table = TablePrefix.'user';
	protected $guarded	= [];
	protected $hidden 	= ['password'];
	protected $fillable = [];
	protected $connection = ENVIRONMENT;

	/* user group */
	public function group()
	{
		return $this->hasOne('User_group_model','id','user_group_id')->first();
	}

	/* user role */
	public function role()
	{
		return $this->hasMany('User_role_model','user_id')->get();
	}
}
/* End of file User_model.php */
/* Location: ./application/modules/bos/models/user/User_model.php */