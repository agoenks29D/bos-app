<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category MODEL
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rka_uraian_model extends Model
{
	use SoftDeletes;
	public $timestamps	= true;
	
	protected $table = TablePrefix.'rka_uraian';
	protected $guarded	= [];
	protected $hidden 	= [];
	protected $fillable = [];
	protected $connection = ENVIRONMENT;

	/* uraian -> rincian */
	public function rincian()
	{
		return $this->hasMany('Rka_rincian_model','rka_uraian_id')->get();
	}

	/* uraian -> kode rekening */
	public function kode_rekening()
	{
		return $this->belongsTo('Kode_rekening_model','kode_rekening_id')->first();
	}

	/* uraian -> kode rekening objek */
	public function kode_rekening_objek()
	{
		return $this->belongsTo('Kode_rekening_objek_model','kode_rekening_objek_id')->first();
	}
}
/* End of file Rka_uraian_model.php */
/* Location: ./application/modules/bos/models/Rka_uraian_model.php */