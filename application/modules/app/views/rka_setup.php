<style type="text/css">
.add_rincian
{
	cursor: pointer;
}
a.add_uraian_rincian
{
	cursor: pointer;
}
</style>
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">RKA</h3> 
	</div>
	<div class="box-body">
		<table class="table table-striped table-bordered table-hover datatable_server_side" cellspacing="0" width="100%">
			<thead>
				<tr>
					<td colspan="8"><center><b>RINCIAN ANGGARAN BELANJA MENURUT PROGRAM DAN PER KEGIATAN SATUAN KERJA PERANGKAT DAERAH</b></center></td>
				</tr>
				<tr>
					<td colspan="2">
						<b>Urusan Pemerintahan</b>
						<br>
						<b>Organisasi</b>
						<br>
						<b>Program</b>
						<br>
						<b>Kegiatan</b>
					</td>
					<td colspan="6">
						<b>: </b><?php echo $rka->urusan_pemerintahan; ?>
						<br>
						<b>: </b><?php echo $rka->organisasi; ?>
						<br>
						<b>: </b><?php echo $rka->program; ?>
						<br>
						<b>: </b><?php echo $rka->kegiatan; ?>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<b>Lokasi Kegiatan</b>
					</td>
					<td colspan="6">
						<b>: </b><?php echo $rka->lokasi_kegiatan; ?>
					</td>
				</tr>
				<tr>
					<td rowspan="2">
						<nobr><b>NO</b></nobr>
					</td>
					<td rowspan="2">
						<nobr><b>KODE REKENING</b></nobr>
					</td>
					<td rowspan="2">
						<nobr><b>URAIAN</b></nobr>
					</td>
					<td colspan="3">
						<nobr><b>RINCIAN PERHITUNGAN</b></nobr>
					</td>
					<td rowspan="2">
						<nobr><b>JUMLAH (Rp)</b></nobr>
					</td>
					<td rowspan="2">
						<nobr><b>OPTION</b></nobr>
					</td>
				</tr>
				<tr>
					<td>
						<nobr><b>Volume</b></nobr>
					</td>
					<td>
						<nobr><b>Satuan</b></nobr>
					</td>
					<td>
						<nobr><b>Harga Satuan</b></nobr>
					</td>
				</tr>
			</thead>
			<tbody>
				<?php
				$get_uraian = $rka->uraian();
				if($get_uraian->isNotEmpty())
				{
					foreach ($get_uraian as $nomor_urut => $uraian)
					{
						if(!empty($uraian->kode_rekening()))
						{
							// Get Rincian
							$get_rincian = $uraian->rincian();
							?>
							<tr>
								<td><?php echo $nomor_urut+1;?></td>
								<td>
									<?php 
										echo 
										$uraian->kode_rekening()->akun.'.'
										.$uraian->kode_rekening()->kelompok.'.'
										.$uraian->kode_rekening()->jenis;
									?>
								</td>
								<?php 
								if($get_rincian->isNotEmpty())
								{
									echo '<td>';
										echo '<b class="add_rincian" uraian_id="'.$uraian->id.'" kode_rekening_objek_id="'.$uraian->kode_rekening_objek()->id.'">'.$uraian->kode_rekening()->nama.'</b>';
									echo '</td>';

									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td>';
									// echo '<button type="button" class="btn btn-xs btn-default btn_option" data_id="'.$uraian->id.'" option="edit_uraian"><i class="fa fa-edit"></i></button>';
									echo '<button type="button" class="btn btn-xs btn-danger btn_option" data_id="'.$uraian->id.'" option="delete_uraian"><i class="fa fa-trash"></i></button>';
									echo '</td>';

									echo '<tr>';
										echo '<td></td>';
										echo '<td>'.$uraian->kode_rekening()->akun.'.'
										.$uraian->kode_rekening()->kelompok.'.'
										.$uraian->kode_rekening()->jenis.'.'
										.$uraian->kode_rekening_objek()->id.
										'</td>';
										echo '<td>'.$uraian->kode_rekening_objek()->nama.'</td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
									echo '</tr>';
									foreach ($get_rincian as $rincian)
									{
										$get_uraian_rincian = $rincian->uraian_rincian();
										if($get_uraian_rincian->isNotEmpty())
										{
											echo '<tr>';
											echo '<td></td>';
											echo '<td>'.$uraian->kode_rekening()->akun.'.'
											.$uraian->kode_rekening()->kelompok.'.'
											.$uraian->kode_rekening()->jenis.'.'
											.$uraian->kode_rekening_objek()->id.'.'
											.$rincian->kode_rekening_objek_rincian_id.
											'</td>';
											echo '<td>';
											echo '<a class="add_uraian_rincian" rincian_id="'.$rincian->id.'">'.$rincian->kode_rekening_objek_rincian()->nama.'</a>';
											echo '</td>';
											echo '<td></td>';
											echo '<td></td>';
											echo '<td></td>';
											echo '<td></td>';
											echo '<td>';
												// echo '<button type="button" class="btn btn-xs btn-default btn_option" option="edit_rincian" data_id="'.$rincian->id.'"><i class="fa fa-edit"></i></button>';
												echo '<button type="button" class="btn btn-xs btn-danger btn_option" option="delete_rincian" data_id="'.$rincian->id.'"><i class="fa fa-trash"></i></button>';
												echo '</td>';
											echo '</tr>';
											foreach ($get_uraian_rincian as $uraian_rincian)
											{
												echo '<tr>';
												echo '<td></td>';
												echo '<td></td>';
												echo '<td>'.$uraian_rincian->rincian.'</td>';
												echo '<td>'.$uraian_rincian->volume.'</td>';
												echo '<td>'.$uraian_rincian->satuan.'</td>';
												echo '<td>'.$uraian_rincian->harga.'</td>';
												echo '<td>'.$uraian_rincian->jumlah.'</td>';
												echo '<td>';
												echo '<button type="button" class="btn btn-xs btn-default btn_option" option="edit_uraian_rincian" data_id="'.$uraian_rincian->id.'"><i class="fa fa-edit"></i></button>';
												echo '<button type="button" class="btn btn-xs btn-danger btn_option" option="delete_uraian_rincian" data_id="'.$uraian_rincian->id.'"><i class="fa fa-trash"></i></button>';
												echo '</td>';
												echo '</tr>';
											}
										}
										else
										{
											echo '<tr>';
											echo '<td></td>';
											echo '<td>'.$uraian->kode_rekening()->akun.'.'
											.$uraian->kode_rekening()->kelompok.'.'
											.$uraian->kode_rekening()->jenis.'.'
											.$uraian->kode_rekening_objek()->id.'.'
											.$rincian->kode_rekening_objek_rincian_id.
											'</td>';
											echo '<td><a class="add_uraian_rincian" rincian_id="'.$rincian->id.'">'.$rincian->kode_rekening_objek_rincian()->nama.'</a></td>';
											echo '<td></td>';
											echo '<td></td>';
											echo '<td></td>';
											echo '<td></td>';
											echo '<td></td>';
											echo '</tr>';
										}
									}
								}
								else
								{
									echo '<td>';
										echo '<b class="add_rincian" uraian_id="'.$uraian->id.'" kode_rekening_objek_id="'.$uraian->kode_rekening_objek()->id.'">'.$uraian->kode_rekening()->nama.'</b>';
									echo '</td>';

									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
									// echo '<td><button type="button" class="btn btn-xs btn-default btn_option" data_id="'.$uraian->id.'" option="edit_uraian"><i class="fa fa-edit"></i></button><button type="button" class="btn btn-xs btn-danger btn_option" data_id="'.$uraian->id.'" option="delete_uraian"><i class="fa fa-trash"></i></button></td>';
								}
								?>
							</tr>
							<?php
						}
					}
				}
				?>
				<tr>
					<td colspan="5"></td>
					<td><b>Jumlah Total :</b></td>
					<td colspan="2">Rp.<?php echo (isset($rka_jumlah) && !empty($rka_jumlah))?$rka_jumlah:0; ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="box-footer">
		<button type="button" class="btn btn-success btn-sm pull-right" id="PublishRka"><i class="fa fa-save"></i> Simpan</button>
		<button type="button" class="btn btn-primary btn-sm pull-left" data-toggle="modal" data-target="#modal_add"><i class="fa fa-plus"></i> Uraian</button>
	</div>
</div>

<iframe name="print_frame" width="0" height="0" frameborder="0" src="about:blank"></iframe>

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Footer Setup</h3>
	</div>
	<div class="box-body">
		<div class="col-lg-6">
			<table class="table table-striped">
				<tr>
					<td>Kepala Sekolah</td>
					<td id="KepalaSekolahName">
						<?php
						if(!empty($footer_role['kepala_sekolah']))
						{
							$user = $this->user->find($footer_role['kepala_sekolah']['user_id']);
							echo (!empty($user))?$user->full_name.' '.$user->nip:false;
						}
						?>
					</td>
					<td><button class="btn btn-default btn_choose_role" role_data="kepala_sekolah">Pilih</button></td>
				</tr>
				<tr>
					<td>Komite</td>
					<td id="KomiteName">
						<?php
						if(!empty($footer_role['komite']))
						{
							$user = $this->user->find($footer_role['komite']['user_id']);
							echo (!empty($user))?$user->full_name.' '.$user->nip:false;
						}
						?>
					</td>
					<td><button class="btn btn-default btn_choose_role" role_data="komite">Pilih</button></td>
				</tr>
				<tr>
					<td>Bendahara</td>
					<td id="BendaharaName">
						<?php
						if(!empty($footer_role['bendahara']))
						{
							$user = $this->user->find($footer_role['bendahara']['user_id']);
							echo (!empty($user))?$user->full_name.' '.$user->nip:false;
						}
						?>
					</td>
					<td><button class="btn btn-default btn_choose_role" role_data="bendahara">Pilih</button></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).on('click','.btn_choose_role',function(event){
	event.preventDefault();
	$('.select_role').val('').trigger('change')
	$('.select_user').val('').trigger('change')
	$('.role_name').val($(this).attr('role_data'));
	$('#modal_choose_role').modal('show')
})

$(document).ready(function() {
	$('.select_role').select2({
		placeholder:'Pilih Role',
		allowClear: true,
		ajax:
		{
			url: '<?php echo base_url("bos/user/role");?>',
			dataType: 'json',
			delay: 250,
			type:'GET',
			data: function (params,data)
			{
				var request_data =
				{
					ajax:true,
					search:
					{
						value:params.term
					},
					length:10,
					start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
					order:
					{
						column:'id',
						type:'asc'
					}
				}
				return request_data;
			},
			processResults: function (data, params,x)
			{
				params.page = params.page || 1;
				var result_data = 
				{
					results: data.data,
					pagination:
					{
						more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
					}
				}
				return result_data
			}
		},
		escapeMarkup: function (markup)
		{
			return markup;
		},
		templateResult: function(data)
		{
			if(data.loading)
			{
				return data.text;
			}
			return '<div>'+data.name+'</div>';
		},
		templateSelection: function(data)
		{
			return data.name || data.text
		}
	});

	// Select User Role
	$('.select_role').on('select2:select', function(e){
		var role_id = $(this).val();
		App.ajax_request({
		url:'<?php echo base_url("bos/user/role/") ?>'+role_id
		},function(ajax_param,data){
			if(data.status == 'success')
			{
				$(".select_user").val('').trigger('change')
				var role_data = data.data;
				$('.select_user').select2({
					placeholder:'Pilih User',
					allowClear: true,
					ajax:
					{
						url: '<?php echo base_url("bos/user/user_role");?>',
						dataType: 'json',
						delay: 250,
						type:'GET',
						data: function (params,data)
						{
							if(role_data.per_sekolah == 1)
							{
								var request_data =
								{
									ajax:true,
									search:
									{
										value:params.term
									},
									length:10,
									start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
									order:
									{
										column:'id',
										type:'asc'
									},
									role_id:role_data.id,
									sekolah_id:<?php echo $rka->sekolah_id;?>
								}
							}
							else
							{
								var request_data =
								{
									ajax:true,
									search:
									{
										value:params.term
									},
									length:10,
									start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
									order:
									{
										column:'id',
										type:'asc'
									},
									role_id:role_data.id
								}	
							}
							
							return request_data;
						},
						processResults: function (data, params)
						{
							params.page = params.page || 1;
							var result_data = 
							{
								results: $.map(data.data, function(obj){
									var set_data = {
										id: obj.user.id,
										text: obj.user.full_name,
										nip:obj.user.nip
									}
									return set_data
								}),
								pagination:
								{
									more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
								}
							}
							return result_data
						}
					},
					escapeMarkup: function (markup)
					{
						return markup;
					},
					templateResult: function(data)
					{
						if(data.loading)
						{
							return data.text;
						}
						return '<div>'+data.text+' - '+data.nip+'</div>'
					},
					templateSelection: function(data)
					{
						return data.text
					}
				});
			}
		});
	});

	// Submit Role
	$("#ChooseRole").on('submit',(function(e){
		e.preventDefault();
		var location_path = window.location.pathname.split('/');
		$('input.data_id').val(location_path[5]);
		App.ajax_request({
			url: '<?php echo base_url("bos/footer_role");?>',
			type: 'POST',
			dataType: 'json',
			contentType:false,
			cache: false,
			processData:false,
			data:new FormData(this)
		},function(ajax_param,data){
			$('.select_role').val('').trigger('change')
			$('.select_user').val('').trigger('change')
			if(data.status == 'success')
			{
				$('#ChooseRole')[0].reset()
				swal({
					position: 'top-end',
					type: 'success',
					title: 'role berhasil ditambahkan',
					showConfirmButton: false,
					timer: 2000,
					onClose:window.location.reload()
				}).then(()=>{},(dismiss)=>{})
			}
		})
	}))
});

$(document).on('click','#PublishRka',function(event){
	event.preventDefault();
	var location_path = window.location.pathname.split('/');
	App.ajax_request({
		url:'<?php echo base_url("bos/set_status_rka/") ?>'+location_path[5]+'/publish'
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			swal({
				position: 'top-end',
				type: 'success',
				title: 'RKA Berhasil Di Simpan',
				showConfirmButton: false,
				timer: 2000,
				onClose:window.location.reload()
			}).then(()=>{},(dismiss)=>{})
		}
	});
})
$(document).on('click','.btn_option',function(event){
	event.preventDefault();
	var location_path = window.location.pathname.split('/');
	var data_id = $(this).attr('data_id');
	var option = $(this).attr('option');
	var title 	= option;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});
	switch(option)
	{
		case 'edit_uraian':
			App.ajax_request({
				url:'<?php echo base_url("bos/rka_uraian/") ?>'+data_id
			},function(ajax_param,data){
				if(data.status == 'success')
				{
					$('#modal_edit_uraian').modal('show');
					// Set Select2 Kode Rekening
					App.ajax_request({
						url:'<?php echo base_url("bos/kode_rekening/") ?>'+data.data.kode_rekening_id
					},function(ajax_param,data){
						if(data.status == 'success')
						{
							$('#EditUraianSelectRekening').select2('trigger', 'select',{data:{id:data.data.id,text:data.data.nama }});
						}
					});
					// Set Select2 Kode Rekening Objek
					App.ajax_request({
						url:'<?php echo base_url("bos/kode_rekening_objek/") ?>'+data.data.kode_rekening_objek_id
					},function(ajax_param,data){
						if(data.status == 'success')
						{
							$('#EditUraianSelectUraianObjek').select2('trigger', 'select',{data:{id:data.data.id,text:data.data.nama }});
						}
					});
				}
			});
		break;

		case 'delete_uraian':
			swal({
				title: 	'delete uraian',
				text: 	"Are you sure,want to delete uraian?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, delete it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/force_delete_rka_uraian/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(ajax_param,data){
					window.location.reload()
				})
			},(dismiss)=>{});
		break;

		case 'edit_rincian':
		break;

		case 'delete_rincian':
			swal({
				title: 	'delete rincian',
				text: 	"Are you sure,want to delete rincian?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, delete it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/force_delete_rka_rincian/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(ajax_param,data){
					window.location.reload()
				})
			},(dismiss)=>{});
		break;

		case 'delete_uraian_rincian':
			swal({
				title: 	'delete uraian rincian',
				text: 	"Are you sure,want to delete uraian rincian?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, delete it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/force_delete_rka_uraian_rincian/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(ajax_param,data){
					window.location.reload()
				})
			},(dismiss)=>{});
		break;

		case 'edit_uraian_rincian':
			$('#modal_update_uraian_rincian').modal('show');
			App.ajax_request({
				url: '<?php echo base_url("bos/rka_uraian_rincian/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
				data:
				{
					rka_id:location_path[5]
				}
			},function(ajax_param,data){
				if(data.status == 'success')
				{
					$('#EditUraianRincianId').val(data.data.id);
					$('#EditUraianRincianNama').val(data.data.rincian)
					$('#EditUraianRincianVolume').val(data.data.volume)
					$('#EditUraianRincianSatuan').val(data.data.satuan)
					$('#EditUraianRincianHarga').val(data.data.harga)
				}
			})
		break;

		default:
		break;
	}
})

$(document).ready(function(){
	// Add Rincian
	$('.add_rincian').on('click',function(){
		$('#modal_add_rincian').modal('show')
		$('#add_rka_rincian_uraian_id').val($(this).attr('uraian_id'));
		var kode_rekening_objek_id = $(this).attr('kode_rekening_objek_id');
		$('.select_kode_rekening_objek_rincian').select2({
			placeholder:'Pilih Rincian',
			allowClear: false,
			ajax:
			{
				url: '<?php echo base_url("bos/kode_rekening_objek_rincian");?>',
				dataType: 'json',
				delay: 250,
				type:'GET',
				data: function (params,data)
				{
					var request_data =
					{
						ajax:true,
						search:
						{
							value:params.term
						},
						length:10,
						start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
						order:
						{
							column:'id',
							type:'asc'
						},
						kode_rekening_objek_id:kode_rekening_objek_id
					}
					return request_data;
				},
				processResults: function (data, params,x)
				{
					params.page = params.page || 1;
					var result_data = 
					{
						results: data.data,
						pagination:
						{
							more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
						}
					}
					return result_data
				}
			},
			escapeMarkup: function (markup)
			{
				return markup;
			},
			templateResult: function(data)
			{
				if(data.loading)
				{
					return data.text;
				}
				return '<div>'+data.nama+'</div>';
			},
			templateSelection: function(data)
			{
				return data.nama || data.text
			}
		});
	})

	// Add Uraian Rincian
	$('.add_uraian_rincian').on('click',function(){
		var location_path = window.location.pathname.split('/');
		$('#modal_add_uraian_rincian').modal('show')
		$('#rka_rincian_id').val($(this).attr('rincian_id'));
		$('#rka_id').val(location_path[5]);
	})
	var location_path = window.location.pathname.split('/');

	// Set Active Parent Menu
	$.each($('.treeview'),function(index, el){
		if($(el).attr('module') == 'tata_usaha')
		{
			$(el).addClass('active')
		}
	});

	// Set Active Sub Menu
	$.each($('ul.treeview-menu'),function(index, el){
		if($(el).attr('parent_module') == 'tata_usaha')
		{
			$.each($(el).children('li'),function(index, el){
				if($(el).attr('sub_module') == 'rka')
				{
					$(el).addClass('active')
				}
			});
		}
	});

	// Tambah Uraian
	$("#TambahUraian").on('submit',(function(e){
		e.preventDefault();
		$('input.rka_id').val(location_path[5]);
		App.ajax_request({
			url: '<?php echo base_url("bos/add_rka_uraian");?>',
			type: 'POST',
			dataType: 'json',
			contentType:false,
			cache: false,
			processData:false,
			data:new FormData(this)
		},function(ajax_param,data){
			if(data.status == 'success')
			{
				$('#TambahUraian')[0].reset()
				swal({
					position: 'top-end',
					type: 'success',
					title: 'uraian berhasil ditambahkan',
					showConfirmButton: false,
					timer: 2000,
					onClose:window.location.reload()
				}).then(()=>{},(dismiss)=>{})
			}
		})
	}))

	// Tambah Rincian
	$("#TambahRincian").on('submit',(function(e){
		e.preventDefault();
		$('input.rka_id').val(location_path[5]);
		App.ajax_request({
			url: '<?php echo base_url("bos/add_rka_rincian");?>',
			type: 'POST',
			dataType: 'json',
			contentType:false,
			cache: false,
			processData:false,
			data:new FormData(this)
		},function(ajax_param,data){
			console.log(data)
			if(data.status == 'success')
			{
				$('#TambahRincian')[0].reset()
				swal({
					position: 'top-end',
					type: 'success',
					title: 'rincian berhasil ditambahkan',
					showConfirmButton: false,
					timer: 2000,
					onClose:window.location.reload()
				}).then(()=>{},(dismiss)=>{})
			}
		})
	}))

	// Tambah Uraian Rincian
	$("#TambahUraianRincian").on('submit',(function(e){
		e.preventDefault();
		$('input.rka_id').val(location_path[5]);
		App.ajax_request({
			url: '<?php echo base_url("bos/add_rka_uraian_rincian");?>',
			type: 'POST',
			dataType: 'json',
			contentType:false,
			cache: false,
			processData:false,
			data:new FormData(this)
		},function(ajax_param,data){
			if(data.status == 'success')
			{
				$('#TambahUraian')[0].reset()
				swal({
					position: 'top-end',
					type: 'success',
					title: 'uraian berhasil ditambahkan',
					showConfirmButton: false,
					timer: 2000,
					onClose:window.location.reload()
				}).then(()=>{},(dismiss)=>{})
			}
		})
	}))

	// perbaharui Uraian Rincian
	$("#UpdateUraianRincian").on('submit',(function(e){
		e.preventDefault();
		var data_id = $('#EditUraianRincianId').val();
		App.ajax_request({
			url: '<?php echo base_url("bos/update_rka_uraian_rincian/");?>'+data_id,
			type: 'POST',
			dataType: 'json',
			contentType:false,
			cache: false,
			processData:false,
			data:new FormData(this)
		},function(ajax_param,data){
			if(data.status == 'success')
			{
				$('#TambahUraian')[0].reset()
				swal({
					position: 'top-end',
					type: 'success',
					title: 'uraian rincian berhasil diperbaharui',
					showConfirmButton: false,
					timer: 2000,
					onClose:window.location.reload()
				}).then(()=>{},(dismiss)=>{})
			}
		})
	}))

	// Select Code Rekening
	$('.select_kode_rekening').select2({
		placeholder:'Pilih Kode Rekening',
		allowClear: false,
		ajax:
		{
			url: '<?php echo base_url("bos/kode_rekening");?>',
			dataType: 'json',
			delay: 250,
			type:'GET',
			data: function (params,data)
			{
				var request_data =
				{
					ajax:true,
					search:
					{
						value:params.term
					},
					length:10,
					start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
					order:
					{
						column:'id',
						type:'asc'
					}
				}
				return request_data;
			},
			processResults: function (data, params,x)
			{
				params.page = params.page || 1;
				var result_data = 
				{
					results: data.data,
					pagination:
					{
						more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
					}
				}
				return result_data
			}
		},
		escapeMarkup: function (markup)
		{
			return markup;
		},
		templateResult: function(data)
		{
			if(data.loading)
			{
				return data.text;
			}
			return '<div>'+data.nama+'</div>';
		},
		templateSelection: function(data)
		{
			return data.nama || data.text
		}
	});

	// Select Kode Rekening On Selected
	$('.select_kode_rekening').on('select2:select',function(e){
		if ($('.select_kode_rekening_objek').hasClass('select2-hidden-accessible'))
		{
			$('.select_kode_rekening_objek').prop('selectedIndex',0);
			$('.select_kode_rekening_objek').select2('destroy');
		}
		var kode_rekening_id = $(this).val();
		$('.select_kode_rekening_objek').select2({
			placeholder:'Pilih Uraian Objek',
			allowClear: false,
			ajax:
			{
				url: '<?php echo base_url("bos/kode_rekening_objek");?>',
				dataType: 'json',
				delay: 250,
				type:'GET',
				data: function (params,data)
				{
					var request_data =
					{
						ajax:true,
						kode_rekening_id:kode_rekening_id,
						search:
						{
							value:params.term
						},
						length:10,
						start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
						order:
						{
							column:'id',
							type:'asc'
						}
					}
					return request_data;
				},
				processResults: function (data, params,x)
				{
					params.page = params.page || 1;
					var result_data = 
					{
						results: data.data,
						pagination:
						{
							more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
						}
					}
					return result_data
				}
			},
			escapeMarkup: function (markup)
			{
				return markup;
			},
			templateResult: function(data)
			{
				if(data.loading)
				{
					return data.text;
				}
				return '<div>'+data.nama+'</div>';
			},
			templateSelection: function(data)
			{
				return data.nama || data.text
			}
		});
	});
});

</script>

<!-- Tambah Uraian -->
<div class="modal fade" id="modal_add">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="TambahUraian">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="ModalTitleUraian">Tambah Uraian</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Pilih Kode Rekening</label>
						<select class="form-control select_kode_rekening" name="kode_rekening" style="width: 100%;"></select>
					</div>
					<div class="form-group">
						<label>Pilih Uraian Objek</label>
						<select class="form-control select_kode_rekening_objek" name="kode_rekening_objek" style="width: 100%;"></select>
					</div>
					<input type="hidden" name="rka_id" class="rka_id">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_choose_role">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="ChooseRole">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Setup Footer</h4>
				</div>
				<input type="hidden" name="data_id" class="data_id">
				<input type="hidden" name="module" value="rka">
				<div class="modal-body">
					<div class="form-group">
						<label>Pilih Jabatan</label>
						<select class="form-control select_role" style="width: 100%;"></select>
					</div>
					<div class="form-group">
						<label>Pilih User</label>
						<select class="form-control select_user" name="user_id" style="width: 100%;"></select>
					</div>
					<input type="hidden" name="role" class="role_name">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>


<!-- Edit Uraian -->
<div class="modal fade" id="modal_edit_uraian">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="EditUraian">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="ModalTitleUraian">Edit Uraian</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Pilih Kode Rekening</label>
						<select class="form-control select_kode_rekening" name="kode_rekening" id="EditUraianSelectRekening" style="width: 100%;"></select>
					</div>
					<div class="form-group">
						<label>Pilih Uraian Objek</label>
						<select class="form-control select_kode_rekening_objek" name="kode_rekening_objek" id="EditUraianSelectUraianObjek" style="width: 100%;"></select>
					</div>
					<input type="hidden" name="rka_id" class="rka_id">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Tambah Rincian -->
<div class="modal fade" id="modal_add_rincian">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="TambahRincian">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tambah Rincian</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Pilih Rincian</label>
						<select class="form-control select_kode_rekening_objek_rincian" name="kode_rekening_objek_rincian_id" style="width: 100%;"></select>
					</div>
					<input type="hidden" name="rka_uraian_id" id="add_rka_rincian_uraian_id">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Tambah Uraian Rincian -->
<div class="modal fade" id="modal_add_uraian_rincian">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="TambahUraianRincian">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tambah Uraian Rincian</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Nama Rincian</label>
						<input type="text" class="form-control" name="rincian" placeholder="Nama Rincian">
					</div>
					<div class="form-group">
						<label>Volume</label>
						<input type="text" class="form-control" name="volume" placeholder="Volume">
					</div>
					<div class="form-group">
						<label>Satuan</label>
						<input type="text" class="form-control" name="satuan" placeholder="Satuan">
					</div>
					<div class="form-group">
						<label>Harga</label>
						<input type="text" class="form-control" name="harga" placeholder="Harga">
					</div>
					<input type="hidden" name="rka_rincian_id" id="rka_rincian_id">
					<input type="hidden" name="rka_id" id="rka_id">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Perbaharui Uraian Rincian -->
<div class="modal fade" id="modal_update_uraian_rincian">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="UpdateUraianRincian">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Perbaharui Uraian Rincian</h4>
				</div>
				<input type="hidden" id="EditUraianRincianId">
				<div class="modal-body">
					<div class="form-group">
						<label>Nama Rincian</label>
						<input type="text" class="form-control" name="rincian" placeholder="Nama Rincian" id="EditUraianRincianNama">
					</div>
					<div class="form-group">
						<label>Volume</label>
						<input type="text" class="form-control" name="volume" placeholder="Volume" id="EditUraianRincianVolume">
					</div>
					<div class="form-group">
						<label>Satuan</label>
						<input type="text" class="form-control" name="satuan" placeholder="Satuan" id="EditUraianRincianSatuan">
					</div>
					<div class="form-group">
						<label>Harga</label>
						<input type="text" class="form-control" name="harga" placeholder="Harga" id="EditUraianRincianHarga">
					</div>
					<input type="hidden" name="rka_rincian_id" id="rka_rincian_id">
					<input type="hidden" name="rka_id" id="rka_id">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan Perubahan</button>
				</div>
			</form>
		</div>
	</div>
</div>