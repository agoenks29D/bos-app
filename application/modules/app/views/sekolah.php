<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs pull-right">
				<li class="pull-left header">Sekolah <span class="subtitle_header"></span></li>
				<li><a href="#tab_list_data" class="tab_list_trashed" data-toggle="tab"><i class="fa fa-trash"></i> Trash</a></li>
				<li class="active"><a href="#tab_list_data" class="tab_list_data" data-toggle="tab"><i class="fa fa-list"></i> List</a></li>
				<li><a href="#tab_new_data"	 class="tab_new_data" data-toggle="tab"><i class="fa fa-plus"></i> New</a></li>
			</ul>
			<div class="tab-content no-padding">
				<div class="tab-pane" id="tab_new_data">
					<div class="box-body">
						<form id="TambahSekolah" enctype="multipart/form-data">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Nama Sekolah</label>
									<input type="text" name="nama" class="form-control input-sm" placeholder="Nama Sekolah">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>NPSN</label>
									<input type="text" name="npsn" class="form-control input-sm" placeholder="NPSN">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
								<div class="form-group">
									<label class="form-check-label">Negeri&nbsp;&nbsp;</label><input class="flat-green" type="radio" name="tipe" value="negeri">
									<label class="form-check-label">Swasta&nbsp;&nbsp;</label><input class="flat-green" type="radio" name="tipe" value="swasta">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
								<div class="form-group">
									<label>Jumlah Siswa</label>
									<input type="text" name="jumlah_siswa" class="form-control input-sm" placeholder="Jumlah Siswa">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Logo</label>
									<br>
									<input type="file" name="logo" class="form-control input-sm">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
								<div class="form-group">
									<label>Kecamatan</label>
									<input type="text" name="kecamatan" class="form-control input-sm" placeholder="Kecamatan">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
								<div class="form-group">
									<label>Kabupaten</label>
									<input type="text" name="kabupaten" class="form-control input-sm" placeholder="Kabupaten">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
								<div class="form-group">
									<label>Provinsi</label>
									<input type="text" name="provinsi" class="form-control input-sm" placeholder="Provinsi">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Alamat</label>
									<textarea class="form-control" name="alamat" placeholder="Alamat"></textarea>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group">
									<label>Status</label>
									<p>
										<label><input type="radio" name="status" class="flat-red" value="1" checked> Active</label>
										<label><input type="radio" name="status" class="flat-red" value="0"> Non-active</label>
									</p>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
								<div class="form-group">
									<button class="btn btn-primary btn-block"><i class="fa fa-save"></i> Save</button>
								</div>
							</div>
						</form>
					</div>
				</div>

				<div class="tab-pane active" id="tab_list_data">
					<div class="box-body">
						<?php echo $table; ?>
					</div>
				</div>
				
				<div class="box-footer"></div>
			</div>
		</div>
	</div>
</div>


<!-- modal add program  -->
<div class="modal fade" id="modal_edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="EditSekolah" enctype="multipart/form-data">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Edit Sekolah</h4>
				</div>
				<div class="modal-body">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="form-group">
							<label>Nama Sekolah</label>
							<input type="text" name="nama" class="form-control input-sm" id="EditNamaSekolah">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="form-group">
							<label>NPSN</label>
							<input type="text" name="npsn" class="form-control input-sm" id="EditNPSNSekolah">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
						<div class="form-group">
							<label class="form-check-label">Negeri&nbsp;&nbsp;</label><input class="flat-green EditJenisSekolah" type="radio" name="tipe" value="negeri">
							<label class="form-check-label">Swasta&nbsp;&nbsp;</label><input class="flat-green EditJenisSekolah" type="radio" name="tipe" value="swasta">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
						<div class="form-group">
							<label>Jumlah Siswa</label>
							<input type="text" name="jumlah_siswa" class="form-control input-sm" id="EditJumlahSiswaSekolah">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="form-group">
							<label>Logo</label>
							<br>
							<input type="file" name="logo" class="form-control input-sm">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="form-group">
							<label>Alamat</label>
							<textarea class="form-control" name="alamat" id="EditAlamatSekolah"></textarea>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label>Status</label>
							<p>
								<label><input type="radio" name="status" class="flat-red EditStatusSekolah" value="1" checked> Active</label>
								<label><input type="radio" name="status" class="flat-red EditStatusSekolah" value="0"> Non-active</label>
							</p>
						</div>
					</div>
				</div>
				<input type="hidden" id="EditSekolahId">
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
var sekolah_ids = JSON.parse(localStorage.getItem('sekolah_ids'));
// Run On Loaded
$(document).ready(function(){
	// Draw Table
	Sekolah.draw_table({ajax:true,sekolah_ids:sekolah_ids})

	// Button Footer
	$('.box-footer').html(btn_footer.check_all+btn_footer.bulk_action_delete);

	$('.tab_new_data').click(function(event) {
		$('.subtitle_header').text('new');
		$('.box-footer').empty();
	});

	// List Data
	$('.tab_list_data').click(function(event) {
		Sekolah.draw_table({ajax:true,sekolah_ids:sekolah_ids});
		$('.subtitle_header').text('list');
		$('.box-footer').html(btn_footer.check_all+btn_footer.bulk_action_delete);
	});

	// List Data In Trash
	$('.tab_list_trashed').click(function(event) {
		Sekolah.draw_table({only_trash:true});
		$('.subtitle_header').text('in trash');
		$('.box-footer').html(btn_footer.check_all+' '+btn_footer.bulk_action_restore+' '+btn_footer.bulk_action_force_delete);
	});

	// Set Active Parent Menu
	$.each($('.treeview'),function(index, el){
		if($(el).attr('module') == 'bos')
		{
			$(el).addClass('active')
		}
	});

	// Set Active Sub Menu
	$.each($('ul.treeview-menu'),function(index, el){
		if($(el).attr('parent_module') == 'bos')
		{
			$.each($(el).children('li'),function(index, el){
				if($(el).attr('sub_module') == 'sekolah')
				{
					$(el).addClass('active')
				}
			});
		}
	});
});

// bulk action
$(document).on('click', '.bulk_action', function(event){
	event.preventDefault();
	var checked = [];
	var action 	= $(this).attr('action');
	var title 	= action;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});
    $.each(bulk_option,function(index, el){
    	if(el.checked == true)
    	{
    		checked.push(el.value);
    	}
    });

	swal({
		title: 	title+' sekolah',
		text: 	"Are you sure,want to "+action+" checked sekolah?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#d33',
		cancelButtonColor: '#3085d6',
		confirmButtonText: "Yes, "+action+" it!"
	}).then(function(){
		if(checked !== false)
		{
			if($.isEmptyObject(checked))
			{
				swal("Oops...", "No checked found", "error");
			}
			else
			{
				Sekolah.bulk_action({id:checked,action:action},function(option,data){
					switch(option.data.action)
					{
						case 'restore':
							Sekolah.draw_table({only_trash:true,sekolah_ids:sekolah_ids});
						break;

						case 'force delete':
							Sekolah.draw_table({ajax:true,only_trash:true,sekolah_ids:sekolah_ids});
						break;

						default :
							Sekolah.draw_table({});
						break;
					}
				})
			}
		}
	},(dismiss)=>{});
});

/* Sekolah Class */
class Sekolah
{
	static bulk_action(option,callback)
	{
		App.ajax_request({url: '<?php echo base_url("bos/bulk_action_sekolah") ?>',type: 'POST',dataType: 'json',data:option},callback)
	}

	static draw_table(option)
	{
		DataTable_Custom.destroy_datatable();
		datatable_server_side(
		{
			url:'<?php echo base_url("bos/sekolah")?>',
			type:'GET',
			data:option
		},
		{
			columns:
			[
				{
					data:'id',render:function (data, type, full, meta)
					{
						return '<input type="checkbox" class="bulk_option flat-green" name="bulk_check[]" value="'+data+'"> ';
					}
				},
				{
					data:'id',render:function (data, type, full, meta)
					{
						return (meta.row+1);
					}
				},
				{data:'npsn'},
				{data:'nama'},
				{data:'tipe'},
				{data:'jumlah_siswa'},
				{
					data:'status',render:function(data,type,full,meta)
					{
						return (data == 1)?'<button class="btn btn-success btn-xs">aktif</button>':'<button class="btn btn-danger btn-xs">non-aktif</button>';
					}
				},
				{
					data:'id',render:function (data, type, full, meta)
					{
						var btn_delete = (!$.isEmptyObject(option) && typeof option.only_trash !== 'undefined')?'force delete':'delete';
						var btn_detail = (!$.isEmptyObject(option) && typeof option.only_trash !== 'undefined')?'detail_trashed':'detail';
						var btn_edit = (typeof option.only_trash == 'undefined')?
						'<button class="btn_option btn btn-xs btn-default" title="edit" data_id="'+data+'" option="edit"><i class="fa fa-edit"></i></button>':'';
						
						var html = 
						btn_edit+
						'<button class="btn_option btn btn-xs btn-danger" title="delete" data_id="'+data+'" option="'+btn_delete+'"><i class="fa fa-trash"></i></button> '+
						'<button class="btn_option btn btn-xs btn-info" title="detail" data_id="'+data+'" option="'+btn_detail+'"><i class="fa fa-search"></i></button>';
						return html;
					}
				}
			],
			columnDefs:
			[
				{
					defaultContent:null,
					searchable: false,
					orderable: false,
					targets: 0
				},
				{
					defaultContent:null,
					searchable: false,
					orderable: false,
					targets: 1
				}
			]
		},
		function(ajax_param,api){
			datatable = api;
			$(this).on('column-visibility.dt', function(e,settings,column,state){
			icheck_init('flat_green').on('ifChecked', function(event){api.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){api.row($(this).parents('tr')).deselect()})})
			icheck_init('flat_green').on('ifChecked', function(event){api.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){api.row($(this).parents('tr')).deselect()})
		})
	}
}

// Button Option
$(document).on( 'click', 'button.btn_option',function(){
	var data_id = $(this).attr('data_id');
	var option = $(this).attr('option');
	var title 	= option;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});

	switch(option)
	{
		// button option edit
		case 'edit':
			App.ajax_request({
				url: '<?php echo base_url("bos/sekolah/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
			},function(ajax_param,data){
				if(data.status == 'success')
				{
					$('.EditJenisSekolah[value="'+data.data.tipe+'"]').iCheck('check');
					$('.EditStatusSekolah[value="'+data.data.status+'"]').iCheck('check');
					$('#EditNamaSekolah').val(data.data.nama);
					$('#EditNPSNSekolah').val(data.data.npsn);
					$('#EditJumlahSiswaSekolah').val(data.data.jumlah_siswa);
					$('#EditAlamatSekolah').val(data.data.alamat);
					$('#EditSekolahId').val(data.data.id);
					$('#modal_edit').modal('show');
				}
			})
		break;

		// button option detail
		case 'detail':
			App.ajax_request({
				url: '<?php echo base_url("bos/sekolah/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
				data:{only_trash:false},
			},function(ajax_param,data){
				swal({
					imageUrl: '<?php echo base_url('bos/file/image/') ?>'+data.data.logo,
					title: 'Detail Sekolah',
					type: (data.data.logo == null)?'info':false,
					html:
						'<table class="table table-striped table-responsive table-hover">'+
							'<tr>'+
								'<td>Nama Sekolah </td><td>'+data.data.nama+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>NPSN </td><td>'+data.data.npsn+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Jenis  </td><td>'+data.data.tipe+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Jumlah Siswa  </td><td>'+data.data.jumlah_siswa+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Alamat  </td><td>'+data.data.alamat+'</td>'+
							'</tr>'+
						'</table>',
					showCloseButton: true
				})
			})
		break;

		// button option detail trashed
		case 'detail_trashed':
			App.ajax_request({
				url: '<?php echo base_url("bos/sekolah/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
				data:{only_trash:true},
			},function(ajax_param,data){
				swal({
					title: 'Detail Sekolah',
					type: (data.data.logo == null)?'info':false,
					html:
						'<table class="table table-striped table-responsive table-hover">'+
							'<tr>'+
								'<td>Nama Sekolah </td><td>'+data.data.nama+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>NPSN </td><td>'+data.data.npsn+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Jenis  </td><td>'+data.data.tipe+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Jumlah Siswa  </td><td>'+data.data.jumlah_siswa+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Alamat  </td><td>'+data.data.alamat+'</td>'+
							'</tr>'+
						'</table>',
					showCloseButton: true
				})
			})
		break;

		// button option delete
		case 'delete':
			swal({
				title: 	title+' sekolah',
				text: 	"Are you sure,want to "+option+" sekolah?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, "+option+" it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/delete_sekolah/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(ajax_param,data){
					Sekolah.draw_table({ajax:true,sekolah_ids:sekolah_ids});
				})
			},(dismiss)=>{});
		break;

		// button option force delete
		case 'force delete':
			swal({
				title: 	title+' sekolah',
				text: 	"Are you sure,want to "+option+" sekolah?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, "+option+" it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/force_delete_sekolah/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(){
					Sekolah.draw_table({ajax:true,only_trash:true,sekolah_ids:sekolah_ids});
				})
			},(dismiss)=>{});
		break;

		default:
		break;
	}
})

// Tambah Sekolah
$("#TambahSekolah").on('submit',(function(e){
	e.preventDefault();
	App.ajax_request({
		url: '<?php echo base_url("bos/add_sekolah");?>',
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this)
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			$('#TambahSekolah')[0].reset()
			swal({
				position: 'top-end',
				type: 'success',
				title: 'sekolah '+data.data.nama+' berhasil ditambahkan',
				showConfirmButton: false,
				timer: 2000
			}).then(()=>{},(dismiss)=>{})
			Sekolah.draw_table({ajax:true,sekolah_ids:sekolah_ids})
		}
	})
}))

// Perbaharui Sekolah
$("#EditSekolah").on('submit',(function(e){
	e.preventDefault();
	var data_id = $('#EditSekolahId').val();
	App.ajax_request({
		url: '<?php echo base_url("bos/update_sekolah/");?>'+data_id,
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this)
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			$('#EditSekolah')[0].reset()
			swal({
				position: 'top-end',
				type: 'success',
				title: 'sekolah '+data.data.nama+' berhasil perbaharui',
				showConfirmButton: false,
				timer: 2000
			}).then(()=>{},(dismiss)=>{})
			Sekolah.draw_table({ajax:true,sekolah_ids:sekolah_ids})
		}
	})
	
}))
</script>