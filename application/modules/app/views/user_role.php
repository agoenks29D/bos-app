<!-- list user role -->
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs pull-right">
				<li class="pull-left header">User Role <span class="subtitle_header"></span></li>
				<li class="active"><a href="#tab_list_data" class="tab_list_data" data-toggle="tab"><i class="fa fa-list"></i> List</a></li>
				<li><a href="#tab_new_data" title="tambah user role" data-toggle="modal" data-target="#modal_add"><i class="fa fa-plus"></i> New</a></li>
			</ul>
			<div class="tab-content no-padding">
				<div class="tab-pane active" id="tab_list_data">
					<div class="box-body">
						<?php echo $table; ?>
					</div>
				</div>
				<div class="box-footer"></div>
			</div>
		</div>
	</div>
</div>

<!-- modal add user role  -->
<div class="modal fade" id="modal_add">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="TambahUserRole">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tambah User Role</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Pilih User</label>
						<select class="form-control select_user" name="user_id" style="width: 100%;"></select>
					</div>
					<div class="form-group">
						<label>Pilih Role</label>
						<select class="form-control select_role" name="role_id" style="width: 100%;"></select>
					</div>
					<div class="form-group">
						<label>Pilih Sekolah</label>
						<select class="form-control select_sekolah" name="sekolah_id" style="width: 100%;"></select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
// Run On Loaded
$(document).ready(function(){
	// Draw Table
	UserRole.draw_table({ajax:false})

	// Button Footer
	$('.box-footer').html(btn_footer.check_all+btn_footer.bulk_action_delete);

	// List Data
	$('.tab_list_data').click(function(event) {
		UserRole.draw_table({ajax:false});
		$('.subtitle_header').text('list');
		$('.box-footer').html(btn_footer.check_all+btn_footer.bulk_action_delete);
	});

	// List Data In Trash
	$('.tab_list_trashed').click(function(event) {
		UserRole.draw_table({only_trash:true});
		$('.subtitle_header').text('in trash');
		$('.box-footer').html(btn_footer.check_all+' '+btn_footer.bulk_action_restore+' '+btn_footer.bulk_action_force_delete);
	});

	// Set Active Parent Menu
	$.each($('.treeview'),function(index, el){
		if($(el).attr('module') == 'app')
		{
			$(el).addClass('active')
		}
	});

	// Set Active Sub Menu
	$.each($('ul.treeview-menu'),function(index, el){
		if($(el).attr('parent_module') == 'app')
		{
			$.each($(el).children('li'),function(index, el){
				if($(el).attr('sub_module') == 'user_role')
				{
					$(el).addClass('active')
				}
			});
		}
	});
});

// Select User
$('.select_user').select2({
	placeholder:'pilih user',
	allowClear: false,
	ajax:
	{
		url: '<?php echo base_url("bos/user");?>',
		dataType: 'json',
		delay: 250,
		type:'GET',
		data: function (params,data)
		{
			var request_data =
			{
				ajax:true,
				search:
				{
					value:params.term
				},
				length:10,
				start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
				order:
				{
					column:'id',
					type:'asc'
				}
			}
			return request_data;
		},
		processResults: function (data, params,x)
		{
			params.page = params.page || 1;
			var result_data = 
			{
				results: data.data,
				pagination:
				{
					more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
				}
			}
			return result_data
		}
	},
	escapeMarkup: function (markup)
	{
		return markup;
	},
	templateResult: function(data)
	{
		if(data.loading)
		{
			return data.text;
		}
		return '<div>'+data.full_name+'</div>';
	},
	templateSelection: function(data)
	{
		return data.full_name || data.text
	}
});

$('.select_role').select2({
	placeholder:'pilih role',
	allowClear: false,
	ajax:
	{
		url: '<?php echo base_url("bos/user/role");?>',
		dataType: 'json',
		delay: 250,
		type:'GET',
		data: function (params,data)
		{
			var request_data =
			{
				ajax:true,
				search:
				{
					value:params.term
				},
				length:10,
				start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
				order:
				{
					column:'id',
					type:'asc'
				}
			}
			return request_data;
		},
		processResults: function (data, params,x)
		{
			params.page = params.page || 1;
			var result_data = 
			{
				results: data.data,
				pagination:
				{
					more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
				}
			}
			return result_data
		}
	},
	escapeMarkup: function (markup)
	{
		return markup;
	},
	templateResult: function(data)
	{
		if(data.loading)
		{
			return data.text;
		}
		return '<div>'+data.name+'</div>';
	},
	templateSelection: function(data)
	{
		return data.name || data.text
	}
});

$('.select_sekolah').select2({
	placeholder:'pilih sekolah',
	allowClear: false,
	ajax:
	{
		url: '<?php echo base_url("bos/sekolah");?>',
		dataType: 'json',
		delay: 250,
		type:'GET',
		data: function (params,data)
		{
			var request_data =
			{
				ajax:true,
				search:
				{
					value:params.term
				},
				length:10,
				start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
				order:
				{
					column:'id',
					type:'asc'
				}
			}
			return request_data;
		},
		processResults: function (data, params,x)
		{
			params.page = params.page || 1;
			var result_data = 
			{
				results: data.data,
				pagination:
				{
					more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
				}
			}
			return result_data
		}
	},
	escapeMarkup: function (markup)
	{
		return markup;
	},
	templateResult: function(data)
	{
		if(data.loading)
		{
			return data.text;
		}
		return '<div>'+data.nama+'</div>';
	},
	templateSelection: function (data, container)
	{
		return data.nama || data.text;
	}
});



$('.select_role').on('select2:select', function (e) { 
	var role_id = $(this).val();
	App.ajax_request({
		url: '<?php echo base_url("bos/user/role/");?>'+role_id,
		type: 'GET',
		dataType: 'json',
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			if(data.data.per_sekolah == 0)
			{
				$(".select_sekolah").prop("disabled", true);
			}
			else
			{
				$(".select_sekolah").prop("disabled", false);
			}
		}
	})
});

// bulk action
$(document).on('click', '.bulk_action', function(event){
	event.preventDefault();
	var checked = [];
	var action 	= $(this).attr('action');
	var title 	= action;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});
    $.each(bulk_option,function(index, el){
    	if(el.checked == true)
    	{
    		checked.push(el.value);
    	}
    });

	swal({
		title: 	title+' user role',
		text: 	"Are you sure,want to "+action+" checked user role?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#d33',
		cancelButtonColor: '#3085d6',
		confirmButtonText: "Yes, "+action+" it!"
	}).then(function(){
		if(checked !== false)
		{
			if($.isEmptyObject(checked))
			{
				swal("Oops...", "No checked found", "error");
			}
			else
			{
				UserRole.bulk_action({id:checked,action:action},function(option,data){
					switch(option.data.action)
					{
						case 'restore':
							UserRole.draw_table({only_trash:true});
						break;

						case 'force delete':
							UserRole.draw_table({ajax:false,only_trash:true});
						break;

						default :
							UserRole.draw_table({});
						break;
					}
				})
			}
		}
	},(dismiss)=>{});
});

/* UserRole Class */
class UserRole
{
	static bulk_action(option,callback)
	{
		App.ajax_request({url: '<?php echo base_url("bos/bulk_action_user_role") ?>',type: 'POST',dataType: 'json',data:option},callback)
	}

	static draw_table(option)
	{
		DataTable_Custom.destroy_datatable();
		datatable_server_side(
		{
			url:'<?php echo base_url("bos/user/user_role")?>',
			type:'GET',
			data:option
		},
		{
			columns:
			[
				{
					data:'id',render:function (data, type, full, meta)
					{
						return '<input type="checkbox" class="bulk_option flat-green" name="bulk_check[]" value="'+data+'"> ';
					}
				},
				{
					data:'id',render:function (data, type, full, meta)
					{
						return (meta.row+1);
					}
				},
				{data:'user.full_name'},
				{data:'role.name'},
				{
					data:'id',render:function (data, type, full, meta)
					{
						if(full.sekolah)
						{
							return full.sekolah.nama;
						}
						// return (typeof full.sekolah !== 'undefined')?full.sekolah.nama:'';
						return '-';
					}
				},
				{
					data:'id',render:function (data, type, full, meta)
					{
						var btn_delete = (!$.isEmptyObject(option) && typeof option.only_trash !== 'undefined')?'force delete':'delete';
						var btn_edit = (typeof option.only_trash == 'undefined')?
						'<button class="btn_option btn btn-xs btn-default" title="edit" data_id="'+data+'" option="edit"><i class="fa fa-edit"></i></button>':'';
						
						var html = 
						btn_edit+
						'<button class="btn_option btn btn-xs btn-danger" title="delete" data_id="'+data+'" option="force delete"><i class="fa fa-trash"></i></button> ';
						return html;
					}
				}
			],
			columnDefs:
			[
				{
					defaultContent:null,
					searchable: false,
					orderable: false,
					targets: 0
				},
				{
					defaultContent:null,
					searchable: false,
					orderable: false,
					targets: 1
				}
			]
		},
		function(ajax_param,api){
			datatable = api;
			$(this).on('column-visibility.dt', function(e,settings,column,state){
			icheck_init('flat_green').on('ifChecked', function(event){api.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){api.row($(this).parents('tr')).deselect()})})
			icheck_init('flat_green').on('ifChecked', function(event){api.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){api.row($(this).parents('tr')).deselect()})
		})
	}
}

// Button Option
$(document).on( 'click', 'button.btn_option',function(){
	var data_id = $(this).attr('data_id');
	var option = $(this).attr('option');
	var title 	= option;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});
	switch(option)
	{
		// button option edit
		case 'edit':
			App.ajax_request({
				url: '<?php echo base_url("bos/user_role/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
			},function(ajax_param,data){
				var program_id = data.data.program_id;
				if(data.status == 'success')
				{
					App.ajax_request({
						url: '<?php echo base_url("bos/program/");?>'+program_id,
						type: 'GET',
						dataType: 'json'
					},function(ajax_param,data){
						$('.select_program').select2('trigger', 'select',{data:{id:program_id,text:data.data.nama }});
					})
				}
			})
		break;

		// button option force delete
		case 'force delete':
			App.ajax_request({
				url: '<?php echo base_url("bos/force_delete_user_role/");?>'+data_id,
				type: 'GET',
				dataType: 'json'	
			},function(){
				UserRole.draw_table({ajax:false,only_trash:true});
			})
		break;

		default:
		break;
	}
})

// Tambah User Role
$("#TambahUserRole").on('submit',(function(e){
	e.preventDefault();
	App.ajax_request({
		url: '<?php echo base_url("bos/user/add_user_role");?>',
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this)
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			$('#TambahUserRole')[0].reset()
			swal({
				position: 'top-end',
				type: 'success',
				title: 'user role berhasil ditambahkan',
				showConfirmButton: false,
				timer: 2000
			}).then(()=>{},(dismiss)=>{})
			UserRole.draw_table({ajax:false})
		}
	})
}))
</script>