<!-- list bku -->
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs pull-right">
				<li class="pull-left header">BKU Setup <span class="subtitle_header"></span></li>
				<li class="active"><a href="#tab_list_data" class="tab_list_data" data-toggle="tab"><i class="fa fa-list"></i> List</a></li>
				<li><a href="#tab_new_data" title="tambah bku" data-toggle="modal" data-target="#modal_add"><i class="fa fa-plus"></i> New</a></li>
			</ul>
			<div class="tab-content no-padding">
				<div class="tab-pane active" id="tab_list_data">
					<div class="box-body">
						<table class="table table-striped table-responsive table-hover datatable_server_side" cellspacing="0" width="100%">
							<thead>
								<th>#</th>
								<th>No</th>
								<th>Kode Rekening</th>
								<th>Nomor Bukti</th>
								<th>Uraian</th>
								<th>Pemasukan</th>
								<th>Pengeluaran</th>
								<th>Saldo</th>
								<th>Option</th>
							</thead>
							<tbody>
								<?php 
								$saldo = (!empty($latest_bku))?$latest_bku->saldo:0;
								foreach ($bku->uraian() as $key => $uraian)
								{
									if($key == 0)
									{
										echo '<tr>';
										echo '<td></td>';
										echo '<td>'.($key+1).'</td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td>Saldo Bulan Lalu</td>';
										echo '<td>'.$saldo.'</td>';
										echo '<td></td>';
										echo '<td>'.$saldo.'</td>';
										echo '<td>';
										echo '<button class="btn_option btn btn-xs btn-danger" title="delete" data_id="'.$uraian->id.'" option="force delete"><i class="fa fa-trash"></i></button>';
										echo '</td>';
										echo '</tr>';

									}
									echo '<tr>';
									echo '<td><input type="checkbox" class="bulk_option flat-green" name="bulk_check[]" value="'.$uraian->id.'"></td>';
									echo '<td>'.($key+2).'</td>';
									echo '<td>'.$uraian->kode_rekening.'</td>';
									echo '<td>'.$uraian->nomor_bukti.'</td>';
									echo '<td>'.$uraian->uraian.'</td>';
									echo '<td>'.$uraian->pemasukan.'</td>';
									echo '<td>'.$uraian->pengeluaran.'</td>';
									echo '<td>'.((!empty($uraian->pemasukan))?$saldo = $saldo+$uraian->pemasukan:(!empty($uraian->pengeluaran))?$saldo = $saldo-$uraian->pengeluaran:FALSE).'</td>';
									echo '<td>';
									echo '<button class="btn_option btn btn-xs btn-danger" title="delete" data_id="'.$uraian->id.'" option="force delete"><i class="fa fa-trash"></i></button>';
									echo '</td>';
									echo '</tr>';
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="box-footer"></div>
			</div>
		</div>
	</div>
</div>

<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Footer Setup</h3>
	</div>
	<div class="box-body">
		<div class="col-lg-6">
			<table class="table table-striped">
				<tr>
					<td>Kepala Sekolah</td>
					<td id="KepalaSekolahName">
						<?php
						if(!empty($footer_role['kepala_sekolah']))
						{
							$user = $this->user->find($footer_role['kepala_sekolah']['user_id']);
							echo (!empty($user))?$user->full_name.' '.$user->nip:false;
						}
						?>
					</td>
					<td><button class="btn btn-default btn_choose_role" role_data="kepala_sekolah">Pilih</button></td>
				</tr>
				<tr>
					<td>Bendahara</td>
					<td id="BendaharaName">
						<?php
						if(!empty($footer_role['bendahara']))
						{
							$user = $this->user->find($footer_role['bendahara']['user_id']);
							echo (!empty($user))?$user->full_name.' '.$user->nip:false;
						}
						?>
					</td>
					<td><button class="btn btn-default btn_choose_role" role_data="bendahara">Pilih</button></td>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_choose_role">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="ChooseRole">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Setup Footer</h4>
				</div>
				<input type="hidden" name="data_id" class="data_id">
				<input type="hidden" name="module" value="bku">
				<div class="modal-body">
					<div class="form-group">
						<label>Pilih Jabatan</label>
						<select class="form-control select_role" style="width: 100%;"></select>
					</div>
					<div class="form-group">
						<label>Pilih User</label>
						<select class="form-control select_user" name="user_id" style="width: 100%;"></select>
					</div>
					<input type="hidden" name="role" class="role_name">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>


<!-- modal add bku  -->
<div class="modal fade" id="modal_add">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="TambahBKUData">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tambah Uraian</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" name="bku_id" value="<?php echo $bku->id;?>">
					<div class="form-group">
						<label>Jenis</label>
						<select name="jenis" class="form-control">
							<option value="pemasukan">Pemasukan</option>
							<option value="pengeluaran">Pengeluaran</option>
						</select>
					</div>
					<div class="form-group">
						<label>Jumlah</label>
						<input type="text" name="jumlah" placeholder="Jumlah" class="form-control">
					</div>
					<div class="form-group">
						<label>Kode Rekening</label>
						<input type="text" name="kode_rekening" placeholder="Kode Rekening" class="form-control">
					</div>
					<div class="form-group">
						<label>Nomor Bukti</label>
						<input type="text" name="nomor_bukti" placeholder="Nomor Bukti" class="form-control">
					</div>
					<div class="form-group">
						<label>Uraian</label>
						<textarea name="uraian" class="form-control" placeholder="Uraian"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- modal edit bku -->
<div class="modal fade" id="modal_edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="EditBKU">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Edit BKU</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Bulan</label>
						<select name="bulan" class="form-control">
						<?php 
						foreach (month_indonesia() as $key => $month)
						{
							echo '<option value="'.$key.'">'.ucfirst($month).'</option>';	
						}
						?>
						</select>
					</div>
					<div class="form-group">
						<label>Tahun</label>
						<input type="text" name="tahun" placeholder="Tahun" class="form-control">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">

$(document).on('click','.btn_choose_role',function(event){
	event.preventDefault();
	$('.select_role').val('').trigger('change')
	$('.select_user').val('').trigger('change')
	$('.role_name').val($(this).attr('role_data'));
	$('#modal_choose_role').modal('show')
})

$(document).ready(function() {
	$('.select_role').select2({
		placeholder:'Pilih Role',
		allowClear: true,
		ajax:
		{
			url: '<?php echo base_url("bos/user/role");?>',
			dataType: 'json',
			delay: 250,
			type:'GET',
			data: function (params,data)
			{
				var request_data =
				{
					ajax:true,
					search:
					{
						value:params.term
					},
					length:10,
					start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
					order:
					{
						column:'id',
						type:'asc'
					}
				}
				return request_data;
			},
			processResults: function (data, params,x)
			{
				params.page = params.page || 1;
				var result_data = 
				{
					results: data.data,
					pagination:
					{
						more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
					}
				}
				return result_data
			}
		},
		escapeMarkup: function (markup)
		{
			return markup;
		},
		templateResult: function(data)
		{
			if(data.loading)
			{
				return data.text;
			}
			return '<div>'+data.name+'</div>';
		},
		templateSelection: function(data)
		{
			return data.name || data.text
		}
	});

	// Select User Role
	$('.select_role').on('select2:select', function(e){
		var role_id = $(this).val();
		App.ajax_request({
		url:'<?php echo base_url("bos/user/role/") ?>'+role_id
		},function(ajax_param,data){
			if(data.status == 'success')
			{
				$(".select_user").val('').trigger('change')
				var role_data = data.data;
				$('.select_user').select2({
					placeholder:'Pilih User',
					allowClear: true,
					ajax:
					{
						url: '<?php echo base_url("bos/user/user_role");?>',
						dataType: 'json',
						delay: 250,
						type:'GET',
						data: function (params,data)
						{
							if(role_data.per_sekolah == 1)
							{
								var request_data =
								{
									ajax:true,
									search:
									{
										value:params.term
									},
									length:10,
									start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
									order:
									{
										column:'id',
										type:'asc'
									},
									role_id:role_data.id,
									sekolah_id:<?php echo $bku->sekolah_id;?>
								}
							}
							else
							{
								var request_data =
								{
									ajax:true,
									search:
									{
										value:params.term
									},
									length:10,
									start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
									order:
									{
										column:'id',
										type:'asc'
									},
									role_id:role_data.id
								}	
							}
							
							return request_data;
						},
						processResults: function (data, params)
						{
							params.page = params.page || 1;
							var result_data = 
							{
								results: $.map(data.data, function(obj){
									var set_data = {
										id: obj.user.id,
										text: obj.user.full_name,
										nip:obj.user.nip
									}
									return set_data
								}),
								pagination:
								{
									more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
								}
							}
							return result_data
						}
					},
					escapeMarkup: function (markup)
					{
						return markup;
					},
					templateResult: function(data)
					{
						if(data.loading)
						{
							return data.text;
						}
						return '<div>'+data.text+' - '+data.nip+'</div>'
					},
					templateSelection: function(data)
					{
						return data.text
					}
				});
			}
		});
	});

	// Submit Role
	$("#ChooseRole").on('submit',(function(e){
		e.preventDefault();
		var location_path = window.location.pathname.split('/');
		$('input.data_id').val(location_path[5]);
		App.ajax_request({
			url: '<?php echo base_url("bos/footer_role");?>',
			type: 'POST',
			dataType: 'json',
			contentType:false,
			cache: false,
			processData:false,
			data:new FormData(this)
		},function(ajax_param,data){
			$('.select_role').val('').trigger('change')
			$('.select_user').val('').trigger('change')
			if(data.status == 'success')
			{
				$('#ChooseRole')[0].reset()
				swal({
					position: 'top-end',
					type: 'success',
					title: 'role berhasil ditambahkan',
					showConfirmButton: false,
					timer: 2000,
					onClose:window.location.reload()
				}).then(()=>{},(dismiss)=>{})
			}
		})
	}))
});
// Run On Loaded
$(document).ready(function(){
	// Button Footer
	$('.box-footer').html(btn_footer.check_all+btn_footer.bulk_action_delete+'<button class="btn btn-success pull-right" id="PublishBku"><i class="fa fa-save"></i> Simpan</button>');
	$('.datatable_server_side').DataTable({
		responsive:true
	});

	// Set Active Parent Menu
	$.each($('.treeview'),function(index, el){
		if($(el).attr('module') == 'tata_usaha')
		{
			$(el).addClass('active')
		}
	});

	// Set Active Sub Menu
	$.each($('ul.treeview-menu'),function(index, el){
		if($(el).attr('parent_module') == 'tata_usaha')
		{
			$.each($(el).children('li'),function(index, el){
				if($(el).attr('sub_module') == 'bku')
				{
					$(el).addClass('active')
				}
			});
		}
	});
});

// Publish BKU
$(document).on('click','#PublishBku',function(event){
	event.preventDefault();
	var location_path = window.location.pathname.split('/');
	App.ajax_request({
		url:'<?php echo base_url("bos/set_status_bku/") ?>'+location_path[5]+'/publish',
		data:{
			saldo:<?php echo (!empty($saldo))?$saldo:0; ?>
		}
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			swal({
				position: 'top-end',
				type: 'success',
				title: 'BKU Berhasil Di Simpan',
				showConfirmButton: false,
				timer: 2000,
				onClose:window.location.reload()
			}).then(()=>{},(dismiss)=>{})
		}
	});
})

// bulk action
$(document).on('click', '.bulk_action', function(event){
	event.preventDefault();
	var checked = [];
	var action 	= $(this).attr('action');
	var title 	= action;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});
    $.each(bulk_option,function(index, el){
    	if(el.checked == true)
    	{
    		checked.push(el.value);
    	}
    });

	swal({
		title: 	title+' bku uraian',
		text: 	"Are you sure,want to "+action+" checked bku uraian?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#d33',
		cancelButtonColor: '#3085d6',
		confirmButtonText: "Yes, "+action+" it!"
	}).then(function(){
		if(checked !== false)
		{
			if($.isEmptyObject(checked))
			{
				swal("Oops...", "No checked found", "error");
			}
			else
			{
				Bku.bulk_action({id:checked,action:action},function(option,data){
					switch(option.data.action)
					{
						case 'restore':
							window.location.reload();
						break;

						case 'force delete':
							window.location.reload();
						break;

						default :
							window.location.reload();
						break;
					}
				})
			}
		}
	},(dismiss)=>{});
});

/* Bku Class */
class Bku
{
	static bulk_action(option,callback)
	{
		App.ajax_request({url: '<?php echo base_url("bos/bulk_action_bku_data") ?>',type: 'POST',dataType: 'json',data:option},callback)
	}
}

// Button Option
$(document).on( 'click', 'button.btn_option',function(){
	var data_id = $(this).attr('data_id');
	var option = $(this).attr('option');
	var title 	= option;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});

	switch(option)
	{
		// button option edit
		case 'edit':
			App.ajax_request({
				url: '<?php echo base_url("bos/bku_data");?>'+data_id,
				type: 'GET',
				dataType: 'json',
			},function(ajax_param,data){
				if(data.status == 'success')
				{
					$('#EditBulanPembantuBank').val(data.data.bulan);
					$('#EditTahunPembantuBank').val(data.data.tahun);
					$('#EditBKUId').val(data.data.id);
					$('#modal_edit').modal('show');
				}
			})
		break;

		// button option force delete
		case 'force delete':
			swal({
				title: 	title+' bku data',
				text: 	"Are you sure,want to "+option+" bku data?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, "+option+" it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/force_delete_bku_data/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(){
					window.location.reload();
				})
			},(dismiss)=>{});

		break;

		default:
		break;
	}
})

// Tambah BKU
$("#TambahBKUData").on('submit',(function(e){
	e.preventDefault();
	App.ajax_request({
		url: '<?php echo base_url("bos/add_bku_data");?>',
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this)
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			$('#TambahBKUData')[0].reset()
			swal({
				position: 'top-end',
				type: 'success',
				title: 'bku berhasil ditambahkan',
				showConfirmButton: false,
				timer: 2000
			}).then(()=>{},(dismiss)=>{})
			window.location.reload();
		}
	})
}))

// Perbaharui BKU
$("#EditBKU").on('submit',(function(e){
	e.preventDefault();
	var data_id = $('#EditBKUId').val();
	App.ajax_request({
		url: '<?php echo base_url("bos/update_bku_data/");?>'+data_id,
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this)
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			$('#EditBKU')[0].reset()
			swal({
				position: 'top-end',
				type: 'success',
				title: 'bku berhasil perbaharui',
				showConfirmButton: false,
				timer: 2000
			}).then(()=>{},(dismiss)=>{})
			Bku.draw_table({ajax:false})
		}
	})
	
}))
</script>