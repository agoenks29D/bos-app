<!-- list penutupan kas -->
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs pull-right">
				<li class="pull-left header">Penutupan Kas <span class="subtitle_header"></span></li>
				<?php
				if(is_admin())
				{
					echo '<li><a href="#tab_list_data" class="tab_list_trashed" data-toggle="tab"><i class="fa fa-trash"></i> Trash</a></li>';
				}
				?>
				<li class="active"><a href="#tab_list_data" class="tab_list_data" data-toggle="tab"><i class="fa fa-list"></i> List</a></li>
				<li><a href="#tab_new_data" title="tambah penutupan kas" data-toggle="modal" data-target="#modal_add"><i class="fa fa-plus"></i> New</a></li>
			</ul>
			<div class="tab-content no-padding">
				<div class="tab-pane active" id="tab_list_data">
					<div class="box-body">
						<?php echo $table; ?>
					</div>
				</div>
				<div class="box-footer"></div>
			</div>
		</div>
	</div>
</div>

<!-- modal add penutupan kas  -->
<div class="modal fade" id="modal_add">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="TambahPenutupanKas">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Register Penutupan Kas</h4>
				</div>
				<div class="modal-body">
					<div class="col-lg-6">
						<div class="form-group">
							<label>Sekolah</label>
							<select class="form-control select_sekolah" name="sekolah_id" style="width: 100%;" id="TambahPenutupanKasSelectSekolah"></select>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Tanggal Penutupan</label>
							<input type="text" name="tanggal" placeholder="Tanggal Penutupan" class="form-control datepicker" disabled>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Nama Penutup Kas (Pemegang Kas)</label>
							<input type="text" name="nama_penutup_kas" placeholder="Nama Penutup Kas" class="form-control" id="TambahPenutupanKasNamaPenutup" disabled>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Penutupan Kas Lalu</label>
							<input type="text" class="form-control" id="TambahPenutupanKasPenutupanKasLalu" disabled>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Total Penerimaan</label>
							<input type="text" class="form-control TambahPenutupanKasTotalPenerimaan" placeholder="Total Penerimaan" disabled>
							<input type="hidden" name="pemasukan" class="TambahPenutupanKasTotalPenerimaan">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Total Pengeluaran</label>
							<input type="text" class="form-control TambahPenutupanKasTotalPengeluaran" placeholder="Total Pengeluaran" disabled>
							<input type="hidden" name="pengeluaran" class="TambahPenutupanKasTotalPengeluaran">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Saldo Buku</label>
							<input type="text" placeholder="Saldo Buku" class="form-control TambahPenutupanKasSaldoBuku" disabled>
							<input type="hidden" class="TambahPenutupanKasSaldoBuku">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Saldo Kas</label>
							<input type="text" name="saldo_kas" placeholder="Saldo Kas" class="form-control TambahPenutupanKasSaldoKas" disabled>
							<input type="hidden" name="saldo_buku" class="TambahPenutupanKasSaldoKas">
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label>Saldo Bank</label>
							<input type="text" placeholder="Saldo Bank" class="form-control TambahPenutupanKasSaldoBank" disabled>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label>Deskripsi Perbedaan</label>
							<textarea class="form-control" name="deskripsi_perbedaan_kas_bank" id="TambahPenutupanKasDeskripsi" disabled placeholder="Deskripsi Perbedaan Kas Dan Bank"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- modal edit penutupan kas -->
<div class="modal fade" id="modal_edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="EditProgram">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Edit Program</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="EditProgramId">
					<div class="form-group">
						<label>Nama Program</label>
						<input type="text" name="nama" placeholder="Nama Program" class="form-control" id="EditNamaProgram">
					</div>
					<div class="form-group">
						<label>Kelompok Sasaran</label>
						<input type="text" name="kelompok_sasaran" placeholder="Kelompok Sasaran" class="form-control" id="EditKelompokSasaranProgram">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan Perubahan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
var sekolah_ids = JSON.parse(localStorage.getItem('sekolah_ids'));
// Run On Loaded
$(document).ready(function(){
	$('#TambahPenutupanKasSelectSekolah').on('select2:select', function(e){
		var sekolah_data = $('#TambahPenutupanKasSelectSekolah').select2('data');
		var sekolah_id = $(this).val();
		$('#TambahPenutupanKas')[0].reset();
		// $("#TambahPenutupanKasSelectSekolah").empty().trigger('change')
		// $('#TambahPenutupanKasSelectSekolah').select2('trigger', 'select',{data:{id:sekolah_data[0].id,text:sekolah_data[0].nama}});

		$('.datepicker').removeAttr('disabled');
		$('#TambahPenutupanKasNamaPenutup').removeAttr('disabled');
		$('.datepicker').datepicker({
			autoclose: true,
			format:'mm/dd/yyyy'
	    }).on('change', function(e){
	    	var date = $(this).val();
	    	var pembantu_bank;
	        App.ajax_request({url: '<?php echo base_url("bos/bku")?>',type: 'GET',dataType: 'json',data:{ajax:true,date:date,sekolah_id:sekolah_id}},function(ajax_request,data){
	        	$('#TambahPenutupanKas')[0].reset();
	        	if(data.data.length>0)
	        	{
	        		var new_date = new Date(date);
	        		var currMonth = new_date.getMonth() + 1;
	        		var currYear = new_date.getFullYear();
	        		$('.datepicker').val(date);
	        		var bku_id = data.data[0].id;
		        	App.ajax_request({url: '<?php echo base_url("bos/bku_data/")?>',type: 'GET',dataType:'json',ajax:true,bku_id:bku_id},function(ajax_request,data){
		        		var pengeluaran = 0;
		        		var pemasukan = 0;
		        		if(data.data.length>0)
		        		{
		        			$.each(data.data,function(index, el){
		        				pengeluaran += el.pengeluaran;
		        				pemasukan += el.pemasukan;
		        			});

		        			App.ajax_request({
			        			url: '<?php echo base_url("bos/pembantu_bank/")?>',
			        			type: 'GET',
			        			dataType: 'json',
			        			data:
			        			{
			        				ajax:true,
			        				sekolah_id:sekolah_id,
			        				month:currMonth,
			        				year:currYear
			        			}
			        		},function(ajax_request,data){
				        			if(data.data.length>0)
				        			{
				        				pembantu_bank = data.data[0];
					        			$('.TambahPenutupanKasSaldoBank').val(pembantu_bank.saldo);
					        			if(pembantu_bank.saldo !== (pemasukan - pengeluaran))
					        			{
					        				$('#TambahPenutupanKasDeskripsi').removeAttr('disabled');
					        			}
				        			}
				        	})
		        		}

		        		App.ajax_request({url: '<?php echo base_url("bos/latest_penutupan_kas/")?>'+sekolah_id,type: 'GET',dataType: 'json'},function(ajax_request,data){
		        			if(data.data !== null)
		        			{
		        				$('#TambahPenutupanKasPenutupanKasLalu').val(data.data.tanggal)
		        			}
		        		})

		        		$('.TambahPenutupanKasTotalPenerimaan').val(pemasukan);
		        		$('.TambahPenutupanKasTotalPengeluaran').val(pengeluaran);
		        		$('.TambahPenutupanKasSaldoBuku').val(pemasukan - pengeluaran);
		        		$('.TambahPenutupanKasSaldoKas').val(pemasukan - pengeluaran);
		        	})
	        	}
	        })
	    });
	});
	
	// Draw Table
	PenutupanKas.draw_table({ajax:true,sekolah_ids:sekolah_ids})

	// Button Footer
	$('.box-footer').html(btn_footer.check_all+btn_footer.bulk_action_delete);

	// List Data
	$('.tab_list_data').click(function(event) {
		PenutupanKas.draw_table({ajax:true,sekolah_ids:sekolah_ids});
		$('.subtitle_header').text('list');
		$('.box-footer').html(btn_footer.check_all+btn_footer.bulk_action_delete);
	});

	// List Data In Trash
	$('.tab_list_trashed').click(function(event) {
		PenutupanKas.draw_table({only_trash:true,sekolah_ids:sekolah_ids});
		$('.subtitle_header').text('in trash');
		$('.box-footer').html(btn_footer.check_all+' '+btn_footer.bulk_action_restore+' '+btn_footer.bulk_action_force_delete);
	});

	// Set Active Parent Menu
	$.each($('.treeview'),function(index, el){
		if($(el).attr('module') == 'tata_usaha')
		{
			$(el).addClass('active')
		}
	});

	// Set Active Sub Menu
	$.each($('ul.treeview-menu'),function(index, el){
		if($(el).attr('parent_module') == 'tata_usaha')
		{
			$.each($(el).children('li'),function(index, el){
				if($(el).attr('sub_module') == 'penutupan_kas')
				{
					$(el).addClass('active')
				}
			});
		}
	});

	$('.select_sekolah').select2({
		placeholder:'Pilih Sekolah',
		allowClear: false,
		ajax:
		{
			url: '<?php echo base_url("bos/sekolah");?>',
			dataType: 'json',
			delay: 250,
			type:'GET',
			data: function (params,data)
			{
				var request_data =
				{
					ajax:true,
					sekolah_ids:sekolah_ids,
					search:
					{
						value:params.term
					},
					length:10,
					start:(typeof params.page !== 'undefined')?(params.page-1)*10:0,
					order:
					{
						column:'id',
						type:'asc'
					}
				}
				return request_data;
			},
			processResults: function (data, params,x)
			{
				params.page = params.page || 1;
				var result_data = 
				{
					results: data.data,
					pagination:
					{
						more: (data.data.length == 10)?(params.page * 10 < data.record_total):false
					}
				}
				return result_data
			}
		},
		escapeMarkup: function (markup)
		{
			return markup;
		},
		templateResult: function(data)
		{
			if(data.loading)
			{
				return data.text;
			}
			return '<div>'+data.nama+'</div>';
		},
		templateSelection: function (data, container)
		{
			return data.nama || data.text;
		}
	});
});

// bulk action
$(document).on('click', '.bulk_action', function(event){
	event.preventDefault();
	var checked = [];
	var action 	= $(this).attr('action');
	var title 	= action;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});
    $.each(bulk_option,function(index, el){
    	if(el.checked == true)
    	{
    		checked.push(el.value);
    	}
    });

	swal({
		title: 	title+' penutupan kas',
		text: 	"Are you sure,want to "+action+" checked penutupan kas?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#d33',
		cancelButtonColor: '#3085d6',
		confirmButtonText: "Yes, "+action+" it!"
	}).then(function(){
		if(checked !== false)
		{
			if($.isEmptyObject(checked))
			{
				swal("Oops...", "No checked found", "error");
			}
			else
			{
				PenutupanKas.bulk_action({id:checked,action:action},function(option,data){
					switch(option.data.action)
					{
						case 'restore':
							PenutupanKas.draw_table({only_trash:true,sekolah_ids:sekolah_ids});
						break;

						case 'force delete':
							PenutupanKas.draw_table({ajax:true,only_trash:true,sekolah_ids:sekolah_ids});
						break;

						default :
							PenutupanKas.draw_table({sekolah_ids:sekolah_ids});
						break;
					}
				})
			}
		}
	},(dismiss)=>{});
});

/* PenutupanKas Class */
class PenutupanKas
{
	static bulk_action(option,callback)
	{
		App.ajax_request({url: '<?php echo base_url("bos/bulk_action_penutupan_kas") ?>',type: 'POST',dataType: 'json',data:option},callback)
	}

	static draw_table(option)
	{
		DataTable_Custom.destroy_datatable();
		datatable_server_side(
		{
			url:'<?php echo base_url("bos/penutupan_kas")?>',
			type:'GET',
			data:option
		},
		{
			columns:
			[
				{
					data:'id',render:function (data, type, full, meta)
					{
						return '<input type="checkbox" class="bulk_option flat-green" name="bulk_check[]" value="'+data+'"> ';
					}
				},
				{
					data:'id',render:function (data, type, full, meta)
					{
						return (meta.row+1);
					}
				},
				{data:'sekolah.nama'},
				{data:'tanggal'},
				{data:'pemasukan'},
				{data:'pengeluaran'},
				{data:'saldo_buku'},
				{data:'saldo_kas'},
				{
					data:'status',
					render:function (data, type, full, meta)
					{
						switch(data)
						{
							case 0:
								return '<button class="btn btn-xs btn-warning btn_status_data" status="draft" data_id="'+full.id+'">draft</button>';
							break;

							case 1:
								return '<button class="btn btn-xs btn-success btn_status_data" status="publish" data_id="'+full.id+'">publish</button>';
							break;
						}
					}
				},
				{
					data:'id',render:function (data, type, full, meta)
					{
						var btn_delete = (!$.isEmptyObject(option) && typeof option.only_trash !== 'undefined')?'force delete':'delete';
						var btn_detail = (!$.isEmptyObject(option) && typeof option.only_trash !== 'undefined')?'detail_trashed':'detail';
						var btn_edit = (typeof option.only_trash == 'undefined')?
						'<button class="btn_option btn btn-xs btn-default" title="edit" data_id="'+data+'" option="edit"><i class="fa fa-edit"></i></button>':'';
						
						var html = 
						// btn_edit+
						'<button class="btn_option btn btn-xs btn-danger" title="delete" data_id="'+data+'" option="'+btn_delete+'"><i class="fa fa-trash"></i></button> '+
						'<button class="btn_option btn btn-xs btn-info" title="detail" data_id="'+data+'" option="'+btn_detail+'"><i class="fa fa-search"></i></button>';
						return html;
					}
				}
			],
			columnDefs:
			[
				{
					defaultContent:null,
					searchable: false,
					orderable: false,
					targets: 0
				},
				{
					defaultContent:null,
					searchable: false,
					orderable: false,
					targets: 1
				}
			]
		},
		function(ajax_param,api){
			datatable = api;
			$(this).on('column-visibility.dt', function(e,settings,column,state){
			icheck_init('flat_green').on('ifChecked', function(event){api.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){api.row($(this).parents('tr')).deselect()})})
			icheck_init('flat_green').on('ifChecked', function(event){api.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){api.row($(this).parents('tr')).deselect()})
		})
	}
}

// Button Option
$(document).on( 'click', 'button.btn_option',function(){
	var data_id = $(this).attr('data_id');
	var option = $(this).attr('option');
	var title 	= option;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});
	switch(option)
	{
		// button option edit
		case 'edit':
			App.ajax_request({
				url: '<?php echo base_url("bos/penutupan_kas/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
			},function(ajax_param,data){
				if(data.status == 'success')
				{
					$('#EditNamaProgram').val(data.data.nama);
					$('#EditKelompokSasaranProgram').val(data.data.kelompok_sasaran);
					$('#EditProgramId').val(data.data.id);
					$('#modal_edit').modal('show');
				}
			})
		break;

		// button option detail
		case 'detail':
			App.ajax_request({
				url: '<?php echo base_url("bos/penutupan_kas/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
				data:{only_trash:false},
			},function(ajax_param,data){
				swal({
					title: 'Detail Program '+data.data.nama,
					type: 'info',
					html:
						'<table class="table table-striped table-responsive table-hover">'+
							'<tr>'+
								'<td>Nama Program </td><td>'+data.data.nama+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Tanggal Pembuatan </td><td>'+data.data.created_at+'</td>'+
							'</tr>'+
						'</table>',
					showCloseButton: true
				})
			})
		break;

		// button option detail trashed
		case 'detail_trashed':
			App.ajax_request({
				url: '<?php echo base_url("bos/penutupan_kas/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
				data:{only_trash:true},
			},function(ajax_param,data){
				swal({
					title: 'Detail Program '+data.data.nama,
					type: 'info',
					html:
						'<table class="table table-striped table-responsive table-hover">'+
							'<tr>'+
								'<td>Nama Program </td><td>'+data.data.nama+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Tanggal Pembuatan </td><td>'+data.data.created_at+'</td>'+
							'</tr>'+
						'</table>',
					showCloseButton: true
				})
			})
		break;

		// button option delete
		case 'delete':
			swal({
				title: 	title+' penutupan kas',
				text: 	"Are you sure,want to "+option+" penutupan kas?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, "+option+" it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/delete_penutupan_kas/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(ajax_param,data){
					PenutupanKas.draw_table({ajax:true,sekolah_ids:sekolah_ids});
				})
			},(dismiss)=>{});
		break;

		// button option force delete
		case 'force delete':
			swal({
				title: 	title+' penutupan kas',
				text: 	"Are you sure,want to "+option+" penutupan kas?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, "+option+" it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/force_delete_penutupan_kas/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(){
					PenutupanKas.draw_table({ajax:true,only_trash:true,sekolah_ids:sekolah_ids});
				})
			},(dismiss)=>{});
		break;

		default:
		break;
	}
})

// Tambah Penutupan Kas
$("#TambahPenutupanKas").on('submit',(function(e){
	e.preventDefault();
	App.ajax_request({
		url: '<?php echo base_url("bos/add_penutupan_kas");?>',
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this)
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			$('#TambahPenutupanKas')[0].reset()
			swal({
				position: 'top-end',
				type: 'success',
				title: 'penutupan kas '+data.data.tanggal+' berhasil ditambahkan',
				showConfirmButton: false,
				timer: 2000
			}).then(()=>{},(dismiss)=>{})
			PenutupanKas.draw_table({ajax:true,sekolah_ids:sekolah_ids})
		}
	})
}))

// Perbaharui Penutupan Kas
$("#EditProgram").on('submit',(function(e){
	e.preventDefault();
	var data_id = $('#EditProgramId').val();
	App.ajax_request({
		url: '<?php echo base_url("bos/update_penutupan_kas/");?>'+data_id,
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this)
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			$('#EditProgram')[0].reset()
			swal({
				position: 'top-end',
				type: 'success',
				title: 'penutupan kas '+data.data.nama+' berhasil perbaharui',
				showConfirmButton: false,
				timer: 2000
			}).then(()=>{},(dismiss)=>{})
			PenutupanKas.draw_table({ajax:true,sekolah_ids:sekolah_ids})
		}
	})
}))

$(document).on('click','.btn_status_data',function(){
	if("<?php echo (is_admin())?'admin':'user';?>" == "admin")
	{
		var status = $(this).attr('status');
		var data_id = $(this).attr('data_id');
		switch(status)
		{
			case 'publish':
				App.ajax_request({
					url: '<?php echo base_url("bos/set_status_penutupan_kas/");?>'+data_id+'/draft',
					type: 'GET',
					dataType: 'json'
				},function(ajax_param,data){
					PenutupanKas.draw_table({ajax:true,sekolah_ids:sekolah_ids})
				})
			break;

			case 'draft':
				App.ajax_request({
					url: '<?php echo base_url("bos/set_status_penutupan_kas/");?>'+data_id+'/publish',
					type: 'GET',
					dataType: 'json'
				},function(ajax_param,data){
					PenutupanKas.draw_table({ajax:true,sekolah_ids:sekolah_ids})
				})
			break;

			default:
				swal({ type: 'error',title: 'Oops...',text: 'Status undefined'})
			break;
		}
	}
})
</script>