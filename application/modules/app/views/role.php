<div class="col-md-12 col-sm-12 col-xs-12">
	<button class="btn btn-primary btn_add_role"><i class="fa fa-plus"></i>Add Role</button>
</div>
<br><br>
<?php 
foreach ($role as $role_value)
{
	?>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span>
			<div class="info-box-content">
				<span class="info-box-text"><?php echo $role_value->name; ?></span>
				<span class="info-box-number"><?php echo $role_value->user()->count(); ?><small> user</small></span>
				<span class="info-box-number">
					<button class="btn btn-primary btn-xs btn_option" option="edit" data_id="<?php echo($role_value->id) ?>"><i class="fa fa-edit"></i></button>
					<button class="btn btn-danger btn-xs btn_option" option="delete" data_id="<?php echo($role_value->id) ?>"><i class="fa fa-trash"></i></button>
				</span>
			</div>
		</div>
	</div>
	<?php
}
?>
<div class="modal fade" id="modal_add">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="TambahRole">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="ModalTitleUraian">Tambah Role</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Name</label>
						<input type="text" name="name" class="form-control" placeholder="Name">
					</div>
					<div class="form-group">
						<label>Access Module</label>
						<select class="form-control access_module" name="access_module[]" style="width: 100%;" multiple="multiple"></select>
					</div>
					<div class="form-group">
						<label>Per Sekolah</label>
						<select name="per_sekolah" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).on('click','.btn_add_role',function(){
	$('#modal_add').modal('show');
})
$("#TambahRole").on('submit',(function(e){
		e.preventDefault();
		App.ajax_request({
			url: '<?php echo base_url("bos/user/add_role");?>',
			type: 'POST',
			dataType: 'json',
			contentType:false,
			cache: false,
			processData:false,
			data:new FormData(this)
		},function(ajax_param,data){
			if(data.status == 'success')
			{
				swal({
					position: 'top-end',
					type: 'success',
					title: 'role berhasil ditambahkan',
					showConfirmButton: false,
					timer: 2000,
					onClose:window.location.reload()
				}).then(()=>{},(dismiss)=>{})
			}
	})
}))
$(document).ready(function(){
	// Set Active Parent Menu
	$.each($('.treeview'),function(index, el){
		if($(el).attr('module') == 'app')
		{
			$(el).addClass('active')
		}
	});

	// Set Active Sub Menu
	$.each($('ul.treeview-menu'),function(index, el){
		if($(el).attr('parent_module') == 'app')
		{
			$.each($(el).children('li'),function(index, el){
				if($(el).attr('sub_module') == 'role')
				{
					$(el).addClass('active')
				}
			});
		}
	});

	App.ajax_request({
		url: '<?php echo base_url("bos/module");?>',
		dataType: 'json',
		delay: 250,
		type:'GET'
	},function(ajax_request,data){
		$('.access_module').select2({data:data.data,
			placeholder: "Module",
			allowClear: true
		})		
	})
});
$(document).on('click','.btn_option',function(event){
	event.preventDefault();
	var option = $(this).attr('option');
	var data_id = $(this).attr('data_id');
	console.log(data_id)
	switch(option)
	{
		case 'edit':
		App.ajax_request({
			url: '<?php echo base_url("bos/module");?>',
			dataType: 'json',
			delay: 250,
			type:'GET'
		},function(ajax_request,data){
			$('.access_module').select2({data:data.data,
				placeholder: "Module",
				allowClear: true
			})		
		})
		break;

		case 'delete':
			swal({
				title: 	'delete role',
				text: 	"Are you sure,want to delete role?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, delete it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/user/delete_role/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(ajax_param,data){
					window.location.reload()
				})
			},(dismiss)=>{});
		break;
	}
})
</script>