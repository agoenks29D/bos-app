<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @method Month In Indonesia
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/
function month_indonesia(int $month=null)
{
    $months = ['januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'];
    return (!empty($month))?$months[$month-1]:$months;
}

function module_role($key=null)
{
    $role = [
        'user' => 'User',
        'role' => 'Role',
        'role_access' => 'Role Access',
        'skpd' => 'SKPD',
        'sekolah' => 'Sekolah',
        'program' => 'Program',
        'kegiatan' => 'Kegiatan',
        'sumber_dana' => 'Sumber Dana',
        'kode_rekening' => 'Kode Rekening',
        'rka' => 'RKA',
        'pembantu_kas' => 'Pembantu Kas',
        'pembantu_bank' => 'Pembantu Bank',
        'bku' => 'BKU',
        'penutupan_kas' => 'Penutupan kas'
    ];
    return (!empty($key))?$role[$key]:$role;
}


/**
 * @method Footer Role
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/
function footer_role($module=null,$data_id=null,$role=null)
{
    $ci =& get_instance();
    $ci->load->model('bos/footer_role_model','footer_role');
    $query = $ci->footer_role->where('module',$module)->where('data_id',$data_id);
    return (!empty($role))?$query->where('role',$role)->first():$query->get();
}

/**
 * @method Check Object or Array If Exist Or Return Default Value On Empty
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/
function return_if_exists($data,$key=null,$onEmpty=null)
{
    switch (gettype($data))
    {
        case 'array':
            if(!empty($key))
            {
                if(isset($data[$key]))
                {
                    return $data[$key];
                }
                else
                {
                    return (!empty($onEmpty))?$onEmpty:null;
                }
            }
        break;

        case 'object':
            if(!empty($key))
            {
                if(isset($data->{$key}))
                {
                    return $data->{$key};
                }
                else
                {
                    return (!empty($onEmpty))?$onEmpty:null;
                }
            }
        break;
        
        default:
            return (!empty($data))?$data:(!empty($onEmpty))?$onEmpty:null;
        break;
    }
}

/**
 * @method Datatable Query
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/
function datatable_query($model,$query)
{
    $search = (isset($query['search']['value']))?$query['search']['value']:FALSE;
    $_model = $model;

    if(isset($query['only_trash']) && filter_var($query['only_trash'],FILTER_VALIDATE_BOOLEAN))
    {
        try 
        {
            $_model = $_model->onlyTrashed();
        }
        catch (Exception $e)
        {

        }
    }
    elseif(isset($query['with_trash']) && filter_var($query['with_trash'],FILTER_VALIDATE_BOOLEAN))
    {
        try 
        {
            $_model = $_model->withTrashed();
        }
        catch (Exception $e)
        {
            
        }
    }

    if(!empty($search))
    {
        foreach (show_columns($model->getTable()) as $column_key => $column)
        {
            if($column_key == 0)
            {
                $_model = $_model->where($column,'LIKE','%'.$search.'%');
            }
            else
            {
                $_model = $_model->orWhere($column,'LIKE','%'.$search.'%');
            }
        }
    }

    if(isset($query['columns']) && is_array($query['columns']))
    {
        foreach ($query['columns'] as $key => $column)
        {
            if(isset($query['columns'][$key]['search']['value']) && !empty($query['columns'][$key]['search']['value']))
            {
                $search_column = $query['columns'][$key]['data'];
                $search_value  = $query['columns'][$key]['search']['value'];
                if(in_array($search_column, show_columns($model->getTable())))
                {
                    $_model = $_model->where($search_column,'LIKE','%'.$search_value.'%');
                }
            }
        }
    }
    
    if(!empty($query['order']))
    {
        if(isset($query['columns']))
        {
            $column = $query['columns'][$query['order'][0]['column']]['data'];
        }
        else
        {
            if(isset($query['order']['column']))
            {
                $column = $query['order']['column'];
            }
            else
            {
                $column = $model->getKeyName();
            }
        }

        if(isset($query['order'][0]['dir']))
        {
            $type = $query['order'][0]['dir'];
        }
        else
        {
            if(isset($query['order']['type']))
            {
                $type = $query['order']['type'];    
            }
            else
            {
                $type = 'asc';
            }
        }

        if(in_array($column, show_columns($model->getTable())))
        {
            $_model = $_model->orderBy($column,$type);
        }
    }
    return $_model;
}

/**
 * @method Codeigniter Routing
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/
function router($param=null)
{
    $ci =& get_instance();
    return $ci->router->{$param};
}

/**
 * @method Site Language
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/
function site_language($key=null,$default=null)
{
    return $default;
}

/**
 * @method Twig Call Function
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/
function _function()
{
    $params = func_get_args();
    $function = $params[0];
    array_shift($params);
    return call_user_func_array($function,$params);
}

/**
 * @method Get Class Methods Without Parent Class
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/
function this_class_methods($class)
{
    $ReflectionClass = new ReflectionClass($class);
    $methods = array();
    foreach ($ReflectionClass->getMethods() as $method)
    {
        if ($method->class == $class)
        {
            $methods[] = $method->name;
        }
    }
    return $methods;
}

/**
 * @method Summary Data From Model
 * @author Agung Dirgantara <iam@agungdirgantara.id>
*/
function summary_data($model=null,$columns=array('id'))
{
    $data = 
    [
        'total_data' => $model->count(),
        'today' => $model::whereDate('created_at', '>=',date('y-m-d'))->get(),
        'last_week' =>  $model::whereDate('created_at','>=',nice_date(unix_to_human(time()-(7 * 24 * 60 * 60)),'y-m-d H:i:s'))->get($columns)
    ];
    return $data;
}
/* End of file app_helper.php */
/* Location: ./application/helpers/app_helper.php */