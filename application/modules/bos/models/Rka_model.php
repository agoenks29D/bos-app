<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category MODEL
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rka_model extends Model
{
	use SoftDeletes;
	public $timestamps	= true;
	
	protected $table = TablePrefix.'rka';
	protected $guarded	= [];
	protected $hidden 	= [];
	protected $fillable = [];
	protected $connection = ENVIRONMENT;

	/* rka -> uraian */
	public function uraian($columns=null)
	{
		return $this->hasMany('Rka_uraian_model','rka_id')->get($columns);
	}

	/* rka rincian */
	public function rincian($columns=null)
	{
		return $this->hasMany('Rka_uraian_rincian_model','rka_id')->get($columns);
	}
}
/* End of file Rka_model.php */
/* Location: ./application/modules/bos/models/Rka_model.php */