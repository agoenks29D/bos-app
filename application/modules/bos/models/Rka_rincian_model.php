<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category MODEL
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rka_rincian_model extends Model
{
	use SoftDeletes;
	public $timestamps	= true;
	
	protected $table = TablePrefix.'rka_rincian';
	protected $guarded	= [];
	protected $hidden 	= [];
	protected $fillable = [];
	protected $connection = ENVIRONMENT;

	/* rincian -> uraian rincian */
	public function uraian_rincian()
	{
		return $this->hasMany('Rka_uraian_rincian_model','rka_rincian_id')->get();
	}

	public function kode_rekening_objek_rincian()
	{
		return $this->belongsTo('Kode_rekening_objek_rincian_model','kode_rekening_objek_rincian_id')->first();
	}
}
/* End of file Rka_rincian_model.php */
/* Location: ./application/modules/bos/models/Rka_rincian_model.php */