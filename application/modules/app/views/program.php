<!-- list program -->
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs pull-right">
				<li class="pull-left header">Program <span class="subtitle_header"></span></li>
				<li><a href="#tab_list_data" class="tab_list_trashed" data-toggle="tab"><i class="fa fa-trash"></i> Trash</a></li>
				<li class="active"><a href="#tab_list_data" class="tab_list_data" data-toggle="tab"><i class="fa fa-list"></i> List</a></li>
				<li><a href="#tab_new_data" title="tambah program" data-toggle="modal" data-target="#modal_add"><i class="fa fa-plus"></i> New</a></li>
			</ul>
			<div class="tab-content no-padding">
				<div class="tab-pane active" id="tab_list_data">
					<div class="box-body">
						<?php echo $table; ?>
					</div>
				</div>
				<div class="box-footer"></div>
			</div>
		</div>
	</div>
</div>

<!-- modal add program  -->
<div class="modal fade" id="modal_add">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="TambahProgram">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tambah Program</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Nama Program</label>
						<input type="text" name="nama" placeholder="Nama Program" class="form-control">
					</div>
					<div class="form-group">
						<label>Kelompok Sasaran</label>
						<input type="text" name="kelompok_sasaran" placeholder="Kelompok Sasaran" class="form-control">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- modal edit program -->
<div class="modal fade" id="modal_edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="EditProgram">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Edit Program</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="EditProgramId">
					<div class="form-group">
						<label>Nama Program</label>
						<input type="text" name="nama" placeholder="Nama Program" class="form-control" id="EditNamaProgram">
					</div>
					<div class="form-group">
						<label>Kelompok Sasaran</label>
						<input type="text" name="kelompok_sasaran" placeholder="Kelompok Sasaran" class="form-control" id="EditKelompokSasaranProgram">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan Perubahan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
// Run On Loaded
$(document).ready(function(){
	// Draw Table
	Program.draw_table({ajax:false})

	// Button Footer
	$('.box-footer').html(btn_footer.check_all+btn_footer.bulk_action_delete);

	// List Data
	$('.tab_list_data').click(function(event) {
		Program.draw_table({ajax:false});
		$('.subtitle_header').text('list');
		$('.box-footer').html(btn_footer.check_all+btn_footer.bulk_action_delete);
	});

	// List Data In Trash
	$('.tab_list_trashed').click(function(event) {
		Program.draw_table({only_trash:true});
		$('.subtitle_header').text('in trash');
		$('.box-footer').html(btn_footer.check_all+' '+btn_footer.bulk_action_restore+' '+btn_footer.bulk_action_force_delete);
	});

	// Set Active Parent Menu
	$.each($('.treeview'),function(index, el){
		if($(el).attr('module') == 'bos')
		{
			$(el).addClass('active')
		}
	});

	// Set Active Sub Menu
	$.each($('ul.treeview-menu'),function(index, el){
		if($(el).attr('parent_module') == 'bos')
		{
			$.each($(el).children('li'),function(index, el){
				if($(el).attr('sub_module') == 'program')
				{
					$(el).addClass('active')
				}
			});
		}
	});
});

// bulk action
$(document).on('click', '.bulk_action', function(event){
	event.preventDefault();
	var checked = [];
	var action 	= $(this).attr('action');
	var title 	= action;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});
    $.each(bulk_option,function(index, el){
    	if(el.checked == true)
    	{
    		checked.push(el.value);
    	}
    });

	swal({
		title: 	title+' program',
		text: 	"Are you sure,want to "+action+" checked program?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#d33',
		cancelButtonColor: '#3085d6',
		confirmButtonText: "Yes, "+action+" it!"
	}).then(function(){
		if(checked !== false)
		{
			if($.isEmptyObject(checked))
			{
				swal("Oops...", "No checked found", "error");
			}
			else
			{
				Program.bulk_action({id:checked,action:action},function(option,data){
					switch(option.data.action)
					{
						case 'restore':
							Program.draw_table({only_trash:true});
						break;

						case 'force delete':
							Program.draw_table({ajax:false,only_trash:true});
						break;

						default :
							Program.draw_table({});
						break;
					}
				})
			}
		}
	},(dismiss)=>{});
});

/* Program Class */
class Program
{
	static bulk_action(option,callback)
	{
		App.ajax_request({url: '<?php echo base_url("bos/bulk_action_program") ?>',type: 'POST',dataType: 'json',data:option},callback)
	}

	static draw_table(option)
	{
		DataTable_Custom.destroy_datatable();
		datatable_server_side(
		{
			url:'<?php echo base_url("bos/program")?>',
			type:'GET',
			data:option
		},
		{
			columns:
			[
				{
					data:'id',render:function (data, type, full, meta)
					{
						return '<input type="checkbox" class="bulk_option flat-green" name="bulk_check[]" value="'+data+'"> ';
					}
				},
				{
					data:'id',render:function (data, type, full, meta)
					{
						return (meta.row+1);
					}
				},
				{data:'nama'},
				{data:'kelompok_sasaran'},
				{
					data:'id',render:function (data, type, full, meta)
					{
						var btn_delete = (!$.isEmptyObject(option) && typeof option.only_trash !== 'undefined')?'force delete':'delete';
						var btn_detail = (!$.isEmptyObject(option) && typeof option.only_trash !== 'undefined')?'detail_trashed':'detail';
						var btn_edit = (typeof option.only_trash == 'undefined')?
						'<button class="btn_option btn btn-xs btn-default" title="edit" data_id="'+data+'" option="edit"><i class="fa fa-edit"></i></button>':'';
						
						var html = 
						btn_edit+
						'<button class="btn_option btn btn-xs btn-danger" title="delete" data_id="'+data+'" option="'+btn_delete+'"><i class="fa fa-trash"></i></button> '+
						'<button class="btn_option btn btn-xs btn-info" title="detail" data_id="'+data+'" option="'+btn_detail+'"><i class="fa fa-search"></i></button>';
						return html;
					}
				}
			],
			columnDefs:
			[
				{
					defaultContent:null,
					searchable: false,
					orderable: false,
					targets: 0
				},
				{
					defaultContent:null,
					searchable: false,
					orderable: false,
					targets: 1
				}
			]
		},
		function(ajax_param,api){
			datatable = api;
			$(this).on('column-visibility.dt', function(e,settings,column,state){
			icheck_init('flat_green').on('ifChecked', function(event){api.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){api.row($(this).parents('tr')).deselect()})})
			icheck_init('flat_green').on('ifChecked', function(event){api.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){api.row($(this).parents('tr')).deselect()})
		})
	}
}

// Button Option
$(document).on( 'click', 'button.btn_option',function(){
	var data_id = $(this).attr('data_id');
	var option = $(this).attr('option');
	var title 	= option;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});
	switch(option)
	{
		// button option edit
		case 'edit':
			App.ajax_request({
				url: '<?php echo base_url("bos/program/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
			},function(ajax_param,data){
				if(data.status == 'success')
				{
					$('#EditNamaProgram').val(data.data.nama);
					$('#EditKelompokSasaranProgram').val(data.data.kelompok_sasaran);
					$('#EditProgramId').val(data.data.id);
					$('#modal_edit').modal('show');
				}
			})
		break;

		// button option detail
		case 'detail':
			App.ajax_request({
				url: '<?php echo base_url("bos/program/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
				data:{only_trash:false},
			},function(ajax_param,data){
				swal({
					title: 'Detail Program '+data.data.nama,
					type: 'info',
					html:
						'<table class="table table-striped table-responsive table-hover">'+
							'<tr>'+
								'<td>Nama Program </td><td>'+data.data.nama+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Kelompok Sasaran </td><td>'+data.data.kelompok_sasaran+'</td>'+
							'</tr>'+
						'</table>',
					showCloseButton: true
				})
			})
		break;

		// button option detail trashed
		case 'detail_trashed':
			App.ajax_request({
				url: '<?php echo base_url("bos/program/");?>'+data_id,
				type: 'GET',
				dataType: 'json',
				data:{only_trash:true},
			},function(ajax_param,data){
				swal({
					title: 'Detail Program '+data.data.nama,
					type: 'info',
					html:
						'<table class="table table-striped table-responsive table-hover">'+
							'<tr>'+
								'<td>Nama Program </td><td>'+data.data.nama+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Kelompok Sasaran </td><td>'+data.data.kelompok_sasaran+'</td>'+
							'</tr>'+
						'</table>',
					showCloseButton: true
				})
			})
		break;

		// button option delete
		case 'delete':
			swal({
				title: 	title+' program',
				text: 	"Are you sure,want to "+option+" program?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, "+option+" it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/delete_program/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(ajax_param,data){
					Program.draw_table({ajax:false});
				})
			},(dismiss)=>{});
		break;

		// button option force delete
		case 'force delete':
			swal({
				title: 	title+' program',
				text: 	"Are you sure,want to "+option+" program?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, "+option+" it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/force_delete_program/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(){
					Program.draw_table({ajax:false,only_trash:true});
				})
			},(dismiss)=>{});

		break;

		default:
		break;
	}
})

// Tambah Program
$("#TambahProgram").on('submit',(function(e){
	e.preventDefault();
	App.ajax_request({
		url: '<?php echo base_url("bos/add_program");?>',
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this)
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			$('#TambahProgram')[0].reset()
			swal({
				position: 'top-end',
				type: 'success',
				title: 'program '+data.data.nama+' berhasil ditambahkan',
				showConfirmButton: false,
				timer: 2000
			}).then(()=>{},(dismiss)=>{})
			Program.draw_table({ajax:false})
		}
	})
}))

// Perbaharui Program
$("#EditProgram").on('submit',(function(e){
	e.preventDefault();
	var data_id = $('#EditProgramId').val();
	App.ajax_request({
		url: '<?php echo base_url("bos/update_program/");?>'+data_id,
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this)
	},function(ajax_param,data){
		if(data.status == 'success')
		{
			$('#EditProgram')[0].reset()
			swal({
				position: 'top-end',
				type: 'success',
				title: 'program '+data.data.nama+' berhasil perbaharui',
				showConfirmButton: false,
				timer: 2000
			}).then(()=>{},(dismiss)=>{})
			Program.draw_table({ajax:false})
		}
	})
	
}))
</script>