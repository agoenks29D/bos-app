<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category MODEL
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Pembantu_kas_data_model extends Model
{
	use SoftDeletes;
	public $timestamps	= true;
	
	protected $table = TablePrefix.'pembantu_kas_data';
	protected $guarded	= [];
	protected $hidden 	= [];
	protected $fillable = [];
	protected $connection = ENVIRONMENT;

	/* get pembantu kas */
	public function pembantu_kas()
	{
		return $this->hasOne('Pembantu_kas_model','pembantu_kas_id','id');
	}
}
/* End of file Pembantu_kas_data_model.php */
/* Location: ./application/modules/bos/models/Pembantu_kas_data_model.php */