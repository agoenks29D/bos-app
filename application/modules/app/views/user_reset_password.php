<?php
$user_id = $this->uri->segment(4);
if(is_me($user_id))
{
?>
<form id="UpdatePassword">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Change Password</h3>
		</div>
		<div class="box-body">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="form-group">
					<label>Password 
						<button class="btn btn-xs btn-primary passwd_generator" type="button" input_form="password|repeat_password">
							<i class="fa fa-key"></i>
							generate
						</button>
						<button class="btn btn-xs btn-primary show_passwd" type="button" input_form="password|repeat_password">show</button>
					</label>
					<input type="password" name="password" class="form-control input-sm">
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="form-group">
					<label>Password Confirm</label>
					<input type="password" name="repeat_password" class="form-control input-sm">
				</div>
			</div>
		</div>
		<div class="box-footer">
			<button type="submit" class="btn btn-success">Simpan <i class="fa fa-save"></i></button>
		</div>
	</div>
</form>
<?php
}
else
{
	show_404();
}
?>

<script type="text/javascript">
$('#UpdatePassword').on('submit',(function(event){
	event.preventDefault();
	$.ajax({
		url: '<?php echo(base_url("bos/user/update_password/".$user_id)) ?>',
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this),
		success:(data) => {
			console.log(data)
			if(data.status == 'success')
			{
				$('#UpdatePassword')[0].reset()
				swal({
					position: 'top-end',
					type: 'success',
					title: 'Password updated',
					showConfirmButton: false,
					timer: 1500
				}).then(function(){},function(dismiss){})
			}
			else
			{
				swal({
					type:'error',
					html:data.data.join('</p><p>')
				})
			}
		},
		error:(error) => {
			console.log(error)
		}
	})
}))
</script>