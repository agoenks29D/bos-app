<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category MODEL
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Bku_data_model extends Model
{
	use SoftDeletes;
	public $timestamps	= true;
	
	protected $table = TablePrefix.'bku_data';
	protected $guarded	= [];
	protected $hidden 	= [];
	protected $fillable = [];
	protected $connection = ENVIRONMENT;
}
/* End of file Bku_data_model.php */
/* Location: ./application/modules/bos/models/Bku_data_model.php */