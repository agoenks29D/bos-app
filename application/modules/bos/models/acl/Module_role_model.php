<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category MODEL
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Module_role_model extends Model
{
	use SoftDeletes;
	public $timestamps	= true;
	
	protected $table = TablePrefix.'module_role';
	protected $guarded	= [];
	protected $hidden 	= [];
	protected $fillable = [];
	protected $connection = ENVIRONMENT;


	public function role_privilege($column=null)
	{
		return $this->hasOne('Module_privilege_model','id','privilege')->first($column);
	}
}
/* End of file Module_role_model.php */
/* Location: ./application/modules/bos/models/Module_role_model.php */