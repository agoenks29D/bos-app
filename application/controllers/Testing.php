<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category CONTROLLER
 * @author author_name <author@domain.com>
 * @version 0.1
*/
class Testing extends CI_Controller
{
	/**
	 * constructor class
	*/
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model([
			'bos/user_model' => 'user',
			'bos/user_role_model' => 'user_role',
			'bos/role_model' => 'role'
		]);

		echo "<pre>";
		print_r ($this->user->find(1)->role()->first()->get_role());
		echo "</pre>";
	}

	public function post()
	{
		echo "<pre>";
		print_r ($this->input->post());
		echo "</pre>";
	}

	public function rka()
	{
		$this->load->model([
			'bos/rka_model' => 'rka',
			'bos/rka_uraian_model' => 'rka_uraian',
			'bos/rka_rincian_model' => 'rka_rincian',
			'bos/rka_uraian_rincian_model' => 'rka_uraian_rincian'
		]);

		// $data = array();
		// foreach ($this->rka->all() as $rka)
		// {
		// 	$get_uraian = $rka->uraian();
		// 	echo "<pre>";
		// 	print_r ($get_uraian);
		// 	echo "</pre>";
		// 	if($get_uraian->isNotEmpty())
		// 	{
		// 		foreach ($get_uraian as $uraian)
		// 		{
		// 			$get_rincian = $uraian->rincian();
		// 			echo "<pre>";
		// 			print_r ($get_rincian);
		// 			echo "</pre>";
		// 		}
		// 	}
		// }

		$rka_id = 1;

		$find_rka = $this->rka->find($rka_id);
		echo "<pre>";
		print_r ($find_rka->uraian());
		echo "</pre>";

		// $this->output->set_content_type('application/json')->set_output(json_encode($data));
	}


	public function user()
	{
		$this->load->model([
			'bos/user_model' => 'user',
			'bos/user_group_model' => 'user_group',
			'bos/user_level_model' => 'user_level',
			'bos/privilege_level_model' => 'privilege_level',
			'bos/module_role_model' => 'module_role',
			'bos/module_privilege_model' => 'module_privilege'
		]);

		$user = $this->user->find(1);
		$get_level = $user->level();
		foreach ($get_level as $level)
		{
			$get_level = $level->privilege_level();
			$get_role = $get_level->role();
			foreach ($get_role as $role)
			{
				// echo "<pre>";
				// print_r ($role->role_privilege());
				// echo "</pre>";
			}
		}

		$user = $this->user->find(1);
		$levels = array();
		$roles = array();
		foreach ($user->level() as $level_key => $level)
		{
			$get_role = $level->privilege_level()->role();
			foreach ($get_role as $role)
			{
				$roles[$level_key][] = $role->role_privilege(['id','module','privilege']);
			}

			$levels[] = array_merge($level->privilege_level(['name','status'])->toArray(),['roles' => $roles[$level_key]]);
		}

		$response = array(
			'user' => $user,
			'levels' => $levels
		);
		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}
}

/* End of file Testing.php */
/* Location: ./application/controllers/Testing.php */