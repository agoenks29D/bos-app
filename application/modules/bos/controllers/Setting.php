<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category API
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
class Setting extends Rest_api
{
	private $app_config = FCPATH.'app.config';
	/**
	 * constructor class
	*/
	public function __construct()
	{
		parent::__construct();
	}

	/* get app config */
	public function get_app_config()
	{
		return $this->app_config;
	}
}
/* End of file Setting.php */
/* Location: ./application/modules/bos/controllers/Setting.php */