<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BOS</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/bootstrap/dist/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/font-awesome/css/font-awesome.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/Ionicons/css/ionicons.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/jvectormap/jquery-jvectormap.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/dist/css/AdminLTE.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/dist/css/skins/_all-skins.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/plugins/sweetalert2/dist/sweetalert2.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/plugins/iCheck/all.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/plugins/DataTables/datatables.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/select2/dist/css/select2.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/plugins/pace/pace.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.4.8/dist/css/AdminLTE.min.css');?>">

	<script src="<?php echo base_url('assets/vendor/plugins/sweetalert2/dist/sweetalert2.all.min.js');?>"></script>
	<script src="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/jquery/dist/jquery.min.js');?>"></script>
	<script src="<?php echo base_url('assets/vendor/plugins/DataTables/datatables.min.js');?>"></script>
	<script src="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
	<script src=""<?php echo base_url('assets/AdminLTE-2.4.8/plugins/pace/pace.min.js');?>"></script>
	<script src="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/select2/dist/js/select2.full.min.js');?>"></script>
	<script src="<?php echo base_url('assets/AdminLTE-2.4.8/plugins/iCheck/icheck.min.js');?>"></script>

	<script src="<?php echo base_url('assets/AdminLTE-2.4.8/plugins/input-mask/jquery.inputmask.js');?>"></script>
	<script src="<?php echo base_url('assets/AdminLTE-2.4.8/plugins/input-mask/jquery.inputmask.date.extensions.js');?>"></script>
	<script src="<?php echo base_url('assets/AdminLTE-2.4.8/plugins/input-mask/jquery.inputmask.extensions.js');?>"></script>

	<script src="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');?>"></script>

	<!-- <script src="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/fastclick/lib/fastclick.js');?>"></script> -->
	<script src="<?php echo base_url('assets/AdminLTE-2.4.8/dist/js/adminlte.min.js');?>"></script>
	<!-- <script src="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js');?>"></script> -->
	<!-- <script src="<?php echo base_url('assets/AdminLTE-2.4.8/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script> -->
	<!-- <script src="<?php echo base_url('assets/AdminLTE-2.4.8/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script> -->
	<!-- <script src="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/jquery-slimscroll/jquery.slimscroll.min.js');?>"></script> -->
	<!-- <script src="<?php echo base_url('assets/AdminLTE-2.4.8/bower_components/chart.js/Chart.js');?>"></script> -->
	<script src="<?php echo base_url('assets/module/backend.js') ?>"></script>
</head>
<style type="text/css">
.btn_option
{
	margin-left: 2px;
	margin-right: 2px;
	margin-top: 2px;
	margin-bottom: 2px;
}
</style>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="<?php echo base_url();?>" class="logo" target="_blank">
				<span class="logo-mini"><b>B</b>OS</span>
				<span class="logo-lg"><b>BOS</b> APP</span>
			</a>
			<nav class="navbar navbar-static-top">
				<a href="#" class="sidebar-toggle fas" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- <li class="dropdown messages-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope-o"></i>
								<span class="label label-success">4</span>
							</a>
							<ul class="dropdown-menu">
								<li class="header">You have 4 messages</li>
								<li>
									<ul class="menu">
										<li>
											<a href="#">
												<div class="pull-left">
													<img src="<?php echo base_url('assets/AdminLTE-2.4.8/dist/img/user2-160x160.jpg');?>" class="img-circle" alt="User Image">
												</div>
												<h4>Support Team<small><i class="fa fa-clock-o"></i> 5 mins</small></h4>
												<p>Why not buy a new awesome theme?</p>
											</a>
										</li>
									</ul>
								</li>
								<li class="footer"><a href="#">See All Messages</a></li>
							</ul>
						</li> -->

						<!-- <li class="dropdown notifications-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i>
								<span class="label label-warning">10</span>
							</a>
							<ul class="dropdown-menu">
								<li class="header">You have 10 notifications</li>
								<li>
									<ul class="menu">
										<li>
											<a href="#"><i class="fa fa-users text-aqua"></i> 5 new members joined today</a>
										</li>
									</ul>
								</li>
								<li class="footer"><a href="#">View all</a></li>
							</ul>
						</li> -->

						<!-- <li class="dropdown tasks-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-flag-o"></i>
								<span class="label label-danger">9</span>
							</a>
							<ul class="dropdown-menu">
								<li class="header">You have 9 tasks</li>
								<li>
									<ul class="menu">
										<li>
											<a href="#">
												<h3>Design some buttons<small class="pull-right">20%</small></h3>
												<div class="progress xs">
													<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
														<span class="sr-only">20% Complete</span>
													</div>
												</div>
											</a>
										</li>
									</ul>
								</li>
								<li class="footer"><a href="#">View all tasks</a></li>
							</ul>
						</li> -->

						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="<?php echo base_url('assets/AdminLTE-2.4.8/dist/img/user2-160x160.jpg');?>" class="user-image profile_image" alt="User Image">
								<span class="hidden-xs user_full_name"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-header">
									<img src="<?php echo base_url('assets/AdminLTE-2.4.8/dist/img/user2-160x160.jpg');?>" class="img-circle profile_image" alt="User Image">
									<p><span class="user_full_name"></span><small class="user_group"></small></p>
								</li>
								<li class="user-footer">
									<div class="pull-left">
										<a href="<?php echo(base_url('app/user/profile/'.$this->session->userdata('user_id'))); ?>" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="<?php echo(base_url('bos/user/sign_out')) ?>" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<aside class="main-sidebar">
			<section class="sidebar">
				<div class="user-panel">
					<div class="pull-left image">
						<img src="<?php echo base_url('assets/AdminLTE-2.4.8/dist/img/user2-160x160.jpg');?>" class="img-circle img-responsive profile_image" alt="User Image">
					</div>
					<div class="pull-left info">
						<p class="user_full_name"></p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<form action="#" method="get" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="q" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
						</span>
					</div>
				</form>
				<ul class="sidebar-menu" data-widget="tree">
					<li class="header">MAIN NAVIGATION</li>
					
					<li id="DashboardLeftMenu"><a href="<?php echo base_url('app') ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>

					<!-- APP MODULE -->
					<li class="treeview menu-close" module="app">
						<a href="#">
							<i class="fa fa-cubes"></i> <span>APP</span>
							<span class="pull-right-container"><i class="fa pull-right fa-angle-left"></i></span>
						</a>
						<ul class="treeview-menu" parent_module="app">
							<li sub_module="user">
								<a href="<?php echo base_url('app/user') ?>">
								<i class="fa fa-circle"></i>User</a>
							</li>
							<li sub_module="role">
								<a href="<?php echo base_url('app/role') ?>">
								<i class="fa fa-circle"></i>Role</a>
							</li>
							<li sub_module="user_role">
								<a href="<?php echo base_url('app/user_role') ?>">
								<i class="fa fa-circle"></i>User Role</a>
							</li>
						</ul>
					</li>

					<!-- BOS MODULE -->
					<li class="treeview menu-close" module="bos">
						<a href="#">
							<i class="fa fa-file-o"></i> <span>Bos</span>
							<span class="pull-right-container"><i class="fa pull-right fa-angle-left"></i></span>
						</a>
						<ul class="treeview-menu" parent_module="bos">
							<li sub_module="dashboard">
								<a href="<?php echo base_url('app/bos') ?>">
								<i class="fa fa-circle"></i> Dashboard</a>
							</li>
							<li sub_module="sekolah">
								<a href="<?php echo base_url('app/bos/sekolah');?>">
								<i class="fa fa-circle"></i> Sekolah</a>
							</li>
							<li sub_module="program">
								<a href="<?php echo base_url('app/bos/program');?>">
								<i class="fa fa-circle"></i> Program</a>
							</li>
							<li sub_module="kegiatan">
								<a href="<?php echo base_url('app/bos/kegiatan');?>">
								<i class="fa fa-circle"></i> Kegiatan</a>
							</li>
							<li sub_module="sumber_dana">
								<a href="<?php echo base_url('app/bos/sumber_dana');?>">
								<i class="fa fa-circle"></i> Sumber Dana</a>
							</li>
							<li sub_module="kode_rekening">
								<a href="<?php echo base_url('app/bos/kode_rekening');?>">
								<i class="fa fa-circle"></i> Kode Rekening</a>
							</li>
						</ul>
					</li>

					<!--  TATA USAHA MODULE  -->
					<li class="treeview menu-close" module="tata_usaha">
						<a href="#">
							<i class="fa fa-file"></i> <span>Tata Usaha</span>
							<span class="pull-right-container"><i class="fa pull-right fa-angle-left"></i></span>
						</a>
						<ul class="treeview-menu" parent_module="tata_usaha">
							<li sub_module="rka">
								<a href="<?php echo base_url('app/bos/rka');?>">
								<i class="fa fa-circle"></i> RKA</a>
							</li>
							<li sub_module="pembantu_kas">
								<a href="<?php echo base_url('app/bos/pembantu_kas');?>">
								<i class="fa fa-circle"></i> Pembantu Kas</a>
							</li>
							<li sub_module="pembantu_bank">
								<a href="<?php echo base_url('app/bos/pembantu_bank');?>">
								<i class="fa fa-circle"></i> Pembantu Bank</a>
							</li>
							<!-- <li sub_module="pembantu_pajak">
								<a href="<?php echo base_url('app/bos/pembantu_pajak');?>">
								<i class="fa fa-circle"></i> Pembantu Pajak</a>
							</li> -->
							<li sub_module="bku">
								<a href="<?php echo base_url('app/bos/bku');?>">
								<i class="fa fa-circle"></i> BKU</a>
							</li>
							<li sub_module="penutupan_kas">
								<a href="<?php echo base_url('app/bos/penutupan_kas');?>">
								<i class="fa fa-circle"></i> Penutupan Kas</a>
							</li>
						</ul>
					</li>

					<!-- SKPD MODULE -->
					<!-- <li class="treeview menu-close" module="skpd">
						<a href="#">
							<i class="fa fa-file-text"></i> <span>SKPD</span>
							<span class="pull-right-container"><i class="fa pull-right fa-angle-left"></i></span>
						</a>
						<ul class="treeview-menu" parent_module="skpd">
							<li sub_module="sp2b_sd">
								<a href="<?php echo base_url('app/bos/sp2b_sd') ?>">
								<i class="fa fa-circle"></i>SP2B SD</a>
							</li>
							<li sub_module="sp2b_smp">
								<a href="<?php echo base_url('app/bos/sp2b_smp') ?>">
								<i class="fa fa-circle"></i>SP2B SMP</a>
							</li>
						</ul>
					</li> -->

					<!--  REKAP MODULE  -->
					<!-- <li class="treeview menu-close" module="laporan">
						<a href="#">
							<i class="fa fa-file"></i> <span>Laporan</span>
							<span class="pull-right-container"><i class="fa pull-right fa-angle-left"></i></span>
						</a>
						<ul class="treeview-menu" parent_module="rekap_per_jenis_belanja">
							<li sub_module="rka">
								<a href="<?php echo base_url('app/bos/rka');?>">
								<i class="fa fa-circle"></i> Rekap Per Jenis Belanja</a>
							</li>
						</ul>
					</li> -->
				</ul>
			</section>
		</aside>
		
		<div class="content-wrapper">
			<section class="content-header">
				<h1><?php echo APP['name']; ?><small>Version <?php echo APP['version'];?></small></h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active">Dashboard</li>
				</ol>
			</section>
			<section class="content"><?php echo $content;?></section>
		</div>
		<footer class="main-footer">
			<div class="pull-right hidden-xs"><b>Version</b> 0.1</div>
			<strong>Copyright &copy; <?php echo date('Y');?> <a href="https://kabanjahe.id">Dinas Pendidikan</a>.</strong> All rightsreserved.
		</footer>
	</div>
	<div class="control-sidebar-bg"></div>
</div>
<script>
$(function(){
	if(typeof(Storage) !== undefined)
	{
		if(typeof localStorage.user !== 'undefined')
		{
			var user = JSON.parse(localStorage.getItem('user'));
			$('.user_full_name').text(user.full_name)
		}

		if(user.profile_photo !== null)
		{
			$('.profile_image').attr('src', '<?php echo(base_url("bos/file/image/"))?>'+user.profile_photo);
		}

		<?php
		if(!empty($this->session->userdata('user_id')))
		{
			?>
			var sekolah_ids = [];
			App.ajax_request({
				url: '<?php echo base_url("bos/user/user_role");?>',
				type: 'GET',
				dataType: 'json',
				data:
				{
					ajax:true,
					user_id:<?php echo $this->session->userdata('user_id');?>
				}
			},function(ajax_param,data){
				if(data.data.length>0)
				{
					$.each(data.data,function(index, el){
						sekolah_ids.push(el.sekolah_id);
					});
				}
				localStorage.setItem('sekolah_ids',JSON.stringify(sekolah_ids))
			})
			<?php

		}
		?>
	}
})

$.each($('ul.treeview-menu'),function(index, el){
	$.each($(el).children('li'),function(index, el){
		// console.log($(el).attr('sub_module'))
	});
});

$(document).ready(function() {
	App.ajax_request({
		url: '<?php echo base_url("bos/user/".$this->session->userdata('user_id'));?>',
		type: 'GET',
		dataType: 'json',
	},function(ajax_param,data){
		if(typeof(Storage) !== undefined)
		{
			localStorage.setItem('user',JSON.stringify(data.data))
		}
	})
});
</script>
</body>
</html>
