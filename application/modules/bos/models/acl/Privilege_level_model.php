<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category MODEL
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Privilege_level_model extends Model
{
	use SoftDeletes;
	public $timestamps	= true;
	
	protected $table = TablePrefix.'privilege_level';
	protected $guarded	= [];
	protected $hidden 	= [];
	protected $fillable = [];
	protected $connection = ENVIRONMENT;


	/* get module role */
	public function role($column=null)
	{
		return $this->hasMany('Module_role_model','privilege_for_id')->where('type','privilege_level')->get($column);
	}
}
/* End of file Privilege_level_model.php */
/* Location: ./application/modules/bos/models/Privilege_level_model.php */