<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category MODEL
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class User_level_model extends Model
{
	use SoftDeletes;
	public $timestamps	= true;
	
	protected $table = TablePrefix.'user_level';
	protected $guarded	= [];
	protected $hidden 	= [];
	protected $fillable = [];
	protected $connection = ENVIRONMENT;

	/* privilege level */
	public function privilege_level($column=null)
	{
		return $this->hasOne('Privilege_level_model','id','privilege_level_id')->first($column);
	}
}
/* End of file User_level_model.php */
/* Location: ./application/modules/bos/models/User_level_model.php */