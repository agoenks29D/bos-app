<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category API
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
class Installation extends Rest_api
{
	/**
	 * constructor class
	*/
	protected $db_file = FCPATH.'.database';
	protected $app_config = FCPATH.'app.config';

	public function __construct()
	{
		parent::__construct();
		$this->load->model([
		]);
	}

	/**
	 * @method installation database
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function database_post()
	{
		// Validation Rules
		$validation_rules = 
		[
			[
				'field' => 'hostname',
				'label' => site_language('hostname','hostname'),
				'rules' => 'trim|required',
				'errors' => 
				[
					'required' 	=> site_language('validation_required','the {field} field is required')
				]
			],
			[
				'field' => 'username',
				'label' => site_language('username','username'),
				'rules' => 'trim|required',
				'errors' => 
				[
					'required' 	=> site_language('validation_required','the {field} field is required')
				]
			],
			[
				'field' => 'dbdriver',
				'label' => site_language('dbdriver','driver'),
				'rules' => 'trim|required|in_list[mysqli,postgre,odbc]',
				'errors' => 
				[
					'required' 	=> site_language('validation_required','the {field} field is required'),
					'in_list' => site_language('validation_in_list','the {field} field must be on of : {param}')
				]
			]
		];

		// Set Validation Data
		$this->form_validation->set_data($this->post());

		// Set Validation Rules
		$this->form_validation->set_rules($validation_rules);

		// Validation Run
		if ($this->form_validation->run() == TRUE)
		{
			// DB Config File 
			$db_config = 
			[
				'dsn'		=> $this->post('dsn'),
				'hostname' 	=> return_if_exists($this->post(),'hostname','localhost'),
				'username' 	=> return_if_exists($this->post(),'username','root'),
				'password' 	=> return_if_exists($this->post(),'password',''),
				'database' 	=> return_if_exists($this->post(),'database',NULL),
				'dbdriver' 	=> return_if_exists($this->post(),'dbdriver','mysqli'),
				'dbprefix' 	=> return_if_exists($this->post(),'dbprefix',NULL),
				'pconnect' 	=> return_if_exists($this->post(),'pconnect',FALSE),
				'db_debug' 	=> return_if_exists($this->post(),'db_debug',FALSE),
				'cache_on' 	=> return_if_exists($this->post(),'cache_on',FALSE),
				'cachedir' 	=> return_if_exists($this->post(),'cachedir',NULL),
				'char_set' 	=> return_if_exists($this->post(),'char_set','utf8mb4'),
				'dbcollat' 	=> return_if_exists($this->post(),'dbcollat','utf8mb4_unicode_ci'),
				'swap_pre' 	=> return_if_exists($this->post(),'swap_pre',NULL),
				'encrypt'  	=> return_if_exists($this->post(),'encrypt',FALSE),
				'compress' 	=> return_if_exists($this->post(),'compress',FALSE),
				'stricton' 	=> return_if_exists($this->post(),'stricton',FALSE),
				'failover' 	=> array(),
				'save_queries' => TRUE
			];

			// Load Database Without Error
			$db = @$this->load->database($db_config,true);

			// Check Database Connection
			if(!empty($db->conn_id))
			{
				$db_data[ENVIRONMENT] = $db_config;
				if(!file_exists($this->db_file))
				{
					// Create Database Config File
					file_put_contents($this->db_file,json_encode($db_data,JSON_PRETTY_PRINT));

					// Load Database Config File
					$db_config = json_decode(file_get_contents($this->db_file));

					// Check Database Name For This Environment
					if(empty($db_config->{ENVIRONMENT}->database))
					{
						// Load Model
						$this->load->model(['installation_model' => 'install']);

						// Set Database Name
						$database = (empty($this->post('database')))?$this->install->create_database():$this->post('database');
						
						// Set Ddatabase Object Name
						$db_config->{ENVIRONMENT}->database = $database;

						// Save Database Object Into Database Config File
						file_put_contents($this->db_file,json_encode($db_config,JSON_PRETTY_PRINT));

						// Set Response
						$response = ['status' => 'success','message' => 'database_has_been_created','data' => ['database_name' => $database]];
					}
				}
				else
				{
					$db_config_file = json_decode(file_get_contents($this->db_file),TRUE);
					if(!isset($db_config_file[ENVIRONMENT]))
					{
						// Set Database Environment Object
						$db_data[ENVIRONMENT] = $db_config;

						// Merge Database Config Data With Database Environment
						$db_data = array_merge($db_config_file,$db_data);

						// Save Database Config
						file_put_contents($this->db_file,json_encode($db_data,JSON_PRETTY_PRINT));

						// Get Database Config
						$db_config = json_decode(file_get_contents($this->db_file));
						
						// Check Database Name For This Environment
						if(empty($db_config->{ENVIRONMENT}->database))
						{
							// Set Database Name
							$database = (empty($this->post('database')))?$this->install->create_database():$this->post('database');
							
							// Set Ddatabase Object Name
							$db_config->{ENVIRONMENT}->database = $database;

							// Save Database Object Into Database Config File
							file_put_contents($this->db_file,json_encode($db_config,JSON_PRETTY_PRINT));

							// Set Response
							$response = ['status' => 'success','message' => 'database_has_been_created','data' => ['database_name' => $database]];
						}
					}
					else
					{
						// Set Response
						$response = ['status' => 'failed','message' => 'database_has_been_initalized'];
					}
				}
			}
			else
			{
				// Set Response
				$response = ['status' => 'failed','message' => 'database_connection_error'];
			}
		}
		else
		{
			// Set Response On Validation Error
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/**
	 * @method installation table
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function table_get()
	{
		// Load Install Tables Model
		$this->load->model([
			'installation_model' => 'install'
		]);

		// Variable For Installed Success
		$installation_done = array();

		// Variable For Installation Fail
		$installation_fail = array();

		// Auto Create Table Return Table Method
		$tables = $this->install->create();

		// Each Table
		foreach ($tables as $key => $table)
		{
			// remove prefix table to call method
			$table = str_replace($this->install->table_prefix,'',$table);

			// Run Create Table Method
			($this->install->{$table}())?array_push($installation_done, $tables[$key]):array_push($installation_fail, $tables[$key]);
		}

		// Set Response
		$response = 
		[
			'status' => (count($tables) == count($installation_done))?'success':'failed',
			'data' => [
				'all' => ['count' => count($tables),'tables' => $tables],
				'done' => ['count' => count($installation_done),'tables' => $installation_done],
				'fail' => ['count' => count($installation_fail),'tables' => $installation_fail]
			]
		];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/**
	 * @method remove installation file
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function remove_installation_file()
	{
		unlink(__FILE__);
	}

	/**
	 * @method installation first user
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function first_user_get()
	{
		$this->load->model([
			'bos/user_model' => 'user'
		]);

		if($this->user->all()->isEmpty())
		{
			$this->user->level = 'admin';
			$this->user->email = 'admin@admin.com';
			$this->user->username = 'admin';
			$this->user->password = sha1('admin');
			$this->user->full_name = 'Administrator';
			$this->user->status = 1;
			$this->user->save();
			$response = ['status' => 'success','data' => $this->user];
		}
		else
		{
			$response = ['status' => 'failed','message' => 'site_has_been_installed'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}
}
/* End of file Installation.php */
/* Location: ./application/controllers/Installation.php */