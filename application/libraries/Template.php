<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category LIBRARY
 * @author author_name <author@domain.com>
 * @version 0.1
*/
class Template
{
	protected $ci;
	protected $base_template;

	public function __construct()
	{
        $this->ci =& get_instance();
        $this->ci->load->library(['table']);
	}

	public function set_base($base=null)
	{
		$this->base_template = $base;
	}

	/* load template */
	public function load($base=null,$page=null,$content_data=array(),$template_data=array())
	{
		$template['content'] = (!empty($page))?$this->ci->load->view($page,$content_data,TRUE):FALSE;
		$this->ci->load->view((!empty($base))?$base:$this->base_template,array_merge($template,$template_data));
	}

	/* datatable */
	public function datatable($column=null,$search_by_column=true)
	{
		$table 	= 
		[
			'table_open' 	=> '<table class="table datatable_server_side table-striped" cellspacing="0" width="100%">',
			'table_close' 	=> ($search_by_column === true)?'<tfoot><tr><th>'.implode('</th><th>', $column).'</th></tr></tfoot></table>':'</table>'
		];
		$this->ci->table->set_template($table);
		$this->ci->table->set_empty("&nbsp;");
		$this->ci->table->set_heading($column);
		return $this->ci->table->generate();
	}
}

/* End of file Template.php */
/* Location: ./application/libraries/Template.php */