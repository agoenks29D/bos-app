<div class="row">
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3>150</h3>
				<p>Users</p>
			</div>
			<div class="icon"><i class="fa fa-users"></i></div>
			<a href="<?php echo(base_url('app/user')) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3>150</h3>
				<p>New Orders</p>
			</div>
			<div class="icon"><i class="ion ion-bag"></i></div>
			<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3>150</h3>
				<p>New Orders</p>
			</div>
			<div class="icon"><i class="ion ion-bag"></i></div>
			<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3>150</h3>
				<p>New Orders</p>
			</div>
			<div class="icon"><i class="ion ion-bag"></i></div>
			<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">CPU Traffic</span>
				<span class="info-box-number">90<small>%</small></span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">CPU Traffic</span>
				<span class="info-box-number">90<small>%</small></span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">CPU Traffic</span>
				<span class="info-box-number">90<small>%</small></span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">CPU Traffic</span>
				<span class="info-box-number">90<small>%</small></span>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-3">
		<div class="info-box bg-yellow">
			<span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Inventory</span>
				<span class="info-box-number">5,200</span>
				<div class="progress"><div class="progress-bar" style="width: 50%"></div></div>
				<span class="progress-description">50% Increase in 30 Days</span>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="info-box bg-yellow">
			<span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Inventory</span>
				<span class="info-box-number">5,200</span>
				<div class="progress"><div class="progress-bar" style="width: 50%"></div></div>
				<span class="progress-description">50% Increase in 30 Days</span>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="info-box bg-yellow">
			<span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Inventory</span>
				<span class="info-box-number">5,200</span>
				<div class="progress"><div class="progress-bar" style="width: 50%"></div></div>
				<span class="progress-description">50% Increase in 30 Days</span>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="info-box bg-yellow">
			<span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Inventory</span>
				<span class="info-box-number">5,200</span>
				<div class="progress"><div class="progress-bar" style="width: 50%"></div></div>
				<span class="progress-description">50% Increase in 30 Days</span>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#DashboardLeftMenu').addClass('active')
});
</script>