<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs pull-right">
				<li class="pull-left header">User<span class="subtitle_header"></span></li>
				<li><a href="#tab_list_data" class="tab_list_trashed" data-toggle="tab"><i class="fa fa-trash"></i> Trash</a></li>
				<li class="active"><a href="#tab_list_data" class="tab_list_data" data-toggle="tab"><i class="fa fa-list"></i> List</a></li>
				<li><a href="#tab_new_data"	 class="tab_new_data" data-toggle="tab"><i class="fa fa-plus"></i> New</a></li>
			</ul>
			<div class="tab-content no-padding">
				<div class="tab-pane" id="tab_new_data">
					<div class="box-body">
						<form id="account_add_form" enctype="multipart/form-data">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>NIP</label>
									<input type="text" name="nip" class="form-control input-sm">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Full Name</label>
									<input type="text" name="full_name" class="form-control input-sm">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Level</label>
									<select name="level" class="form-control">
										<option value="user">User</option>
										<option value="admin">Admin</option>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Gender </label>
									<p>
										<label class="form-check-label" for="male">Male</label>
										<input class="flat-green" type="radio" name="gender" value="male" id="male">
										<label class="form-check-label" for="female">Female</label>
										<input class="flat-green" type="radio" name="gender" value="female" id="female">
									</p>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Email</label>
									<input type="text" name="email" class="form-control input-sm">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Username</label>
									<input type="text" name="username" class="form-control input-sm">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Password 
										<button class="btn btn-xs btn-primary passwd_generator" type="button" input_form="password|repeat_password">
											<i class="fa fa-key"></i>
											generate
										</button>
										<button class="btn btn-xs btn-primary show_passwd" type="button" input_form="password|repeat_password">show</button>
									</label>
									<input type="password" name="password" class="form-control input-sm">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Password Confirm</label>
									<input type="password" name="repeat_password" class="form-control input-sm">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Photo</label>
									<br>
									<input type="file" name="image" class="form-control input-sm">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group">
									<label>Status</label>
									<p>
										<label><input type="radio" name="status" class="flat-red" value="1" checked> Active</label>
										<label><input type="radio" name="status" class="flat-red" value="0"> Non-active</label>
									</p>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
								<div class="form-group">
									<button class="btn btn-primary btn-block"><i class="fa fa-save"></i> Save</button>
								</div>
							</div>
						</form>
					</div>
				</div>

				<div class="tab-pane active" id="tab_list_data">
					<div class="box-body">
						<?php echo $table; ?>
					</div>
				</div>

				<!-- <div class="tab-pane active" id="tab_setting">
					<div class="box-body">
						
					</div>
				</div> -->
				<div class="box-footer"></div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// Set Active Parent Menu
	$.each($('.treeview'),function(index, el){
		if($(el).attr('module') == 'app')
		{
			$(el).addClass('active')
		}
	});

	// Set Active Sub Menu
	$.each($('ul.treeview-menu'),function(index, el){
		if($(el).attr('parent_module') == 'app')
		{
			$.each($(el).children('li'),function(index, el){
				if($(el).attr('sub_module') == 'user')
				{
					$(el).addClass('active')
				}
			});
		}
	});

	User.draw_table({ajax:false});
});

/* User Class */
class User
{
	static bulk_action(option,callback)
	{
		App.ajax_request({url: '<?php echo base_url("bos/bulk_action_user") ?>',type: 'POST',dataType: 'json',data:option},callback)
	}

	static draw_table(option)
	{
		DataTable_Custom.destroy_datatable();
		datatable_server_side(
		{
			url:'<?php echo base_url("bos/user")?>',
			type:'GET',
			data:option
		},
		{
			columns:
			[
				{
					data:'id',render:function (data, type, full, meta)
					{
						return '<input type="checkbox" class="bulk_option flat-green" name="bulk_check[]" value="'+data+'"> ';
					}
				},
				{
					data:'id',render:function (data, type, full, meta)
					{
						return (meta.row+1);
					}
				},
				{data:'username'},
				{data:'email'},
				{data:'full_name'},
				{data:'gender'},
				{
					data:'status',render:function(data, type, full, meta)
					{
						switch(data)
						{
							case 0:
								return '<button class="btn btn-xs btn-warning btn_status_data" status="non-active" data_id="'+full.id+'">non-active</button>';
							break;

							case 1:
								return '<button class="btn btn-xs btn-success btn_status_data" status="active" data_id="'+full.id+'">active</button>';
							break;
						}
					}
				},
				{
					data:'id',render:function (data, type, full, meta)
					{
						var btn_delete = (!$.isEmptyObject(option) && typeof option.only_trash !== 'undefined')?'force delete':'delete';
						var btn_detail = (!$.isEmptyObject(option) && typeof option.only_trash !== 'undefined')?'detail_trashed':'detail';
						var btn_edit = (typeof option.only_trash == 'undefined')?
						'<button class="btn_option btn btn-xs btn-default" title="edit" data_id="'+data+'" option="edit"><i class="fa fa-edit"></i></button>':'';
						
						var html = 
						btn_edit+
						'<button class="btn_option btn btn-xs btn-danger" title="delete" data_id="'+data+'" option="'+btn_delete+'"><i class="fa fa-trash"></i></button> '+
						'<a href="<?php echo(base_url("app/user/profile/")) ?>'+data+'" class="btn_option btn btn-xs btn-info" title="info" data_id="'+data+'" option="info"><i class="fa fa-search"></i></a>';
						return html;
					}
				}
			],
			columnDefs:
			[
				{
					defaultContent:null,
					searchable: false,
					orderable: false,
					targets: 0
				},
				{
					defaultContent:null,
					searchable: false,
					orderable: false,
					targets: 1
				}
			]
		},
		function(ajax_param,api){
			datatable = api;
			$(this).on('column-visibility.dt', function(e,settings,column,state){
			icheck_init('flat_green').on('ifChecked', function(event){api.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){api.row($(this).parents('tr')).deselect()})})
			icheck_init('flat_green').on('ifChecked', function(event){api.row($(this).parents('tr')).select()}).on('ifUnchecked',function(){api.row($(this).parents('tr')).deselect()})
		})
	}
}

$('#account_add_form').on('submit',(function(event){
	event.preventDefault();
	$.ajax({
		url: '<?php echo(base_url("bos/user/create")) ?>',
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this),
		success:(data) => {
			console.log(data)
			if(data.status == 'success')
			{
				$('#account_add_form')[0].reset()
				swal({
					position: 'top-end',
					type: 'success',
					title: 'New user created',
					showConfirmButton: false,
					timer: 1500
				}).then(function(){},function(dismiss){})
			}
			else
			{
				swal({
					type:'error',
					html:data.data.join('</p><p>')
				})
			}
		},
		error:(error) => {
			console.log(error)
		}
	})
}))

$(document).on( 'click', 'button.btn_option',function(){
	var data_id = $(this).attr('data_id');
	var option = $(this).attr('option');
	var title 	= option;
		title 	= title.toLowerCase().replace(/\b[a-z]/g, function(letter){
		return letter.toUpperCase();
	});

	switch (option)
	{
		case 'edit':
			window.location.href = '<?php echo(base_url("app/user/edit/"))?>'+data_id
		break;

		case 'delete':
			swal({
				title: 	title+' user',
				text: 	"Are you sure,want to "+option+" user?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "Yes, "+option+" it!"
			}).then(function(){
				App.ajax_request({
					url: '<?php echo base_url("bos/user/delete/");?>'+data_id,
					type: 'GET',
					dataType: 'json'	
				},function(ajax_param,data){
					User.draw_table({ajax:true});
				})
			},(dismiss)=>{});
		break;
	}
});
</script>