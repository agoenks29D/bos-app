<div class="box">
	<div class="box-header with-border">
	</div>
	<div class="box-body" id="print_page">
		<table class="table table-striped table-bordered table-hover datatable_server_side" cellspacing="0" width="100%">
			<thead>
				<tr>
					<td colspan="8"><center><b>RINCIAN ANGGARAN BELANJA MENURUT PROGRAM DAN PER KEGIATAN SATUAN KERJA PERANGKAT DAERAH</b></center></td>
				</tr>
				<tr>
					<td colspan="2">
						<b>Urusan Pemerintahan</b>
						<br>
						<b>Organisasi</b>
						<br>
						<b>Program</b>
						<br>
						<b>Kegiatan</b>
					</td>
					<td colspan="6">
						<b>: </b><?php echo $rka->urusan_pemerintahan; ?>
						<br>
						<b>: </b><?php echo $rka->organisasi; ?>
						<br>
						<b>: </b><?php echo $rka->program; ?>
						<br>
						<b>: </b><?php echo $rka->kegiatan; ?>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<b>Lokasi Kegiatan</b>
					</td>
					<td colspan="6">
						<b>: </b><?php echo $rka->lokasi_kegiatan; ?>
					</td>
				</tr>
				<tr>
					<td rowspan="2">
						<nobr><b>NO</b></nobr>
					</td>
					<td rowspan="2">
						<nobr><b>KODE REKENING</b></nobr>
					</td>
					<td rowspan="2">
						<nobr><b>URAIAN</b></nobr>
					</td>
					<td colspan="3">
						<nobr><b>RINCIAN PERHITUNGAN</b></nobr>
					</td>
					<td rowspan="2">
						<nobr><b>JUMLAH (Rp)</b></nobr>
					</td>
				</tr>
				<tr>
					<td>
						<nobr><b>Volume</b></nobr>
					</td>
					<td>
						<nobr><b>Satuan</b></nobr>
					</td>
					<td>
						<nobr><b>Harga Satuan</b></nobr>
					</td>
				</tr>
			</thead>
			<tbody>
				<?php
				$get_uraian = $rka->uraian();
				if($get_uraian->isNotEmpty())
				{
					foreach ($get_uraian as $nomor_urut => $uraian)
					{
						// Get Rincian
						$get_rincian = $uraian->rincian();
						?>
						<tr>
							<td><?php echo $nomor_urut+1;?></td>
							<td>
								<?php 
									echo 
									$uraian->kode_rekening()->akun.'.'
									.$uraian->kode_rekening()->kelompok.'.'
									.$uraian->kode_rekening()->jenis;
								?>
							</td>
							<?php 
							if($get_rincian->isNotEmpty())
							{
								echo '<td>';
									echo '<b uraian_id="'.$uraian->id.'" kode_rekening_objek_id="'.$uraian->kode_rekening_objek()->id.'">'.$uraian->kode_rekening()->nama.'</b>';
								echo '</td>';

								echo '<td></td>';
								echo '<td></td>';
								echo '<td></td>';
								echo '<td></td>';
								echo '<tr>';
									echo '<td></td>';
									echo '<td>'.$uraian->kode_rekening()->akun.'.'
									.$uraian->kode_rekening()->kelompok.'.'
									.$uraian->kode_rekening()->jenis.'.'
									.$uraian->kode_rekening_objek()->id.
									'</td>';
									echo '<td>'.$uraian->kode_rekening_objek()->nama.'</td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
								echo '</tr>';
								foreach ($get_rincian as $rincian)
								{
									$get_uraian_rincian = $rincian->uraian_rincian();
									if($get_uraian_rincian->isNotEmpty())
									{
										echo '<tr>';
										echo '<td></td>';
										echo '<td>'.$uraian->kode_rekening()->akun.'.'
										.$uraian->kode_rekening()->kelompok.'.'
										.$uraian->kode_rekening()->jenis.'.'
										.$uraian->kode_rekening_objek()->id.'.'
										.$rincian->kode_rekening_objek_rincian_id.
										'</td>';
										echo '<td>';
										echo $rincian->kode_rekening_objek_rincian()->nama;
										echo '</td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '</tr>';
										foreach ($get_uraian_rincian as $uraian_rincian)
										{
											echo '<tr>';
											echo '<td></td>';
											echo '<td></td>';
											echo '<td>'.$uraian_rincian->rincian.'</td>';
											echo '<td>'.$uraian_rincian->volume.'</td>';
											echo '<td>'.$uraian_rincian->satuan.'</td>';
											echo '<td>'.$uraian_rincian->harga.'</td>';
											echo '<td>'.$uraian_rincian->jumlah.'</td>';
											echo '</tr>';
										}
									}
									else
									{
										echo '<tr>';
										echo '<td></td>';
										echo '<td>'.$uraian->kode_rekening()->akun.'.'
										.$uraian->kode_rekening()->kelompok.'.'
										.$uraian->kode_rekening()->jenis.'.'
										.$uraian->kode_rekening_objek()->id.'.'
										.$rincian->kode_rekening_objek_rincian_id.
										'</td>';
										echo '<td>'.$rincian->kode_rekening_objek_rincian()->nama.'</td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '</tr>';
									}
								}
							}
							else
							{
								echo '<td>';
									echo '<b uraian_id="'.$uraian->id.'" kode_rekening_objek_id="'.$uraian->kode_rekening_objek()->id.'">'.$uraian->kode_rekening()->nama.'</b>';
								echo '</td>';

								echo '<td></td>';
								echo '<td></td>';
								echo '<td></td>';
								echo '<td></td>';
							}
							?>
						</tr>
						<?php
					}
				}
				?>
				<tr>
					<!-- <td colspan="2">Jumlah Total</td> -->
					<td colspan="2"><center><b>Jumlah Total :</b></center></td>
					<td colspan="4"></td>
					<td><b>Rp.<?php echo (isset($rka_jumlah) && !empty($rka_jumlah))?$rka_jumlah:0; ?></b></td>
				</tr>
			</tbody>
		</table>
		<div class="col-lg-4" style="margin-top:6%;float: left;">
			<b>Komite</b>
			<br><br><br><br>
			<?php
			if(!empty($footer_role['komite']))
			{
				$user = $this->user->find($footer_role['komite']['user_id']);
				echo (!empty($user))?$user->full_name.'<br>'.$user->nip:false;
			}
			?>
		</div>
		<div class="col-lg-4" style="margin-top:6%;float: left;margin-left: 28%;">
			<center>
				<b>Bendahara</b>
				<br><br><br><br>
				<?php
				if(!empty($footer_role['bendahara']))
				{
					$user = $this->user->find($footer_role['bendahara']['user_id']);
					echo (!empty($user))?$user->full_name.'<br>'.$user->nip:false;
				}
				?>
			</center>
		</div>
		<div class="col-lg-4" style="margin-top:6%;float: right;">
			<center>
				Kabanjahe, 
			<?php echo 
			nice_date($rka->created_at,'d')
			.' '.
			ucfirst(month_indonesia(nice_date($rka->created_at,'m'))).
			' '.
			nice_date($rka->created_at,'Y');
			?>
			<br>
			<b>Kepala Sekolah</b>
			<br><br><br>
			<?php
			if(!empty($footer_role['komite']))
			{
				$user = $this->user->find($footer_role['komite']['user_id']);
				echo (!empty($user))?$user->full_name.'<br>'.$user->nip:false;
			}
			?>
			</center>
		</div>
	</div>
	<div class="box-footer">
		<a onclick="App.print_page('<?php echo(base_url("app/bos/rka/".$rka->id."/print")) ?>')" class="btn btn-default btn-sm pull-right">
			<i class="fa fa-print"></i> Print
		</a>
	</div>
</div>

<script type="text/javascript">
// Select Kode Rekening
$(document).ready(function(){
	// Set Active Parent Menu
	$.each($('.treeview'),function(index, el){
		if($(el).attr('module') == 'tata_usaha')
		{
			$(el).addClass('active')
		}
	});

	// Set Active Sub Menu
	$.each($('ul.treeview-menu'),function(index, el){
		if($(el).attr('parent_module') == 'tata_usaha')
		{
			$.each($(el).children('li'),function(index, el){
				if($(el).attr('sub_module') == 'rka')
				{
					$(el).addClass('active')
				}
			});
		}
	});
});

</script>