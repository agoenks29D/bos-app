<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category API
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
class Bos extends Rest_api
{
	/**
	 * constructor class
	*/

	protected $group_method;
	protected $sekolah_ids;
	public function __construct()
	{
		parent::__construct();
		$this->load->model([

			// Program
			'program_model' => 'program',
			
			// Kode Rekening
			'kegiatan_model' => 'kegiatan',	
			
			// Sumber Dana
			'sumber_dana_model' => 'sumber_dana',
			
			// Sekolah
			'sekolah_model' => 'sekolah',
			
			// File
			'file_model' => 'file',
			
			// Kode Rekening
			'kode_rekening_model' => 'kode_rekening',
			'kode_rekening_objek_model' => 'kode_rekening_objek',
			'kode_rekening_objek_rincian_model' => 'kode_rekening_objek_rincian',
			
			// RKA
			'rka_model' => 'rka',
			'rka_uraian_model' => 'rka_uraian',
			'rka_rincian_model' => 'rka_rincian',
			'rka_uraian_rincian_model' => 'rka_uraian_rincian',
			
			// BKU
			'bku_model' => 'bku',
			'bku_data_model' => 'bku_data',

			// Pembantu Bank
			'pembantu_bank_model' => 'pembantu_bank',
			'pembantu_bank_data_model' => 'pembantu_bank_data',

			'pembantu_kas_model' => 'pembantu_kas',
			'pembantu_kas_data_model' => 'pembantu_kas_data',

			'penutupan_kas_model' => 'penutupan_kas'
		]);
		
		$this->form_validation->set_data($this->post());
		$this->sekolah_ids = $this->input->get('sekolah_ids');

		if(empty($this->session->userdata('user_id')))
		{
			if(!in_array($this->router->method, ['sign_in']))
			{
				if($this->input->is_ajax_request())
				{
					$this->response(['status' => 'failed','message' => 'login_required'],REST_Controller::HTTP_OK);
				}
				else
				{
					redirect(base_url('app/user/sign_in'),'refresh');
				}
			}
		}
	}

	/* module list */
	public function module_get()
	{
		$data = array();
		foreach (module_role() as $key => $value)
		{
			$data[]	= 
			[
				'id' => $key,
				'text' => $value
			];
		}

		$response = ['status' => 'success','data' => $data];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* index dashboard */
	public function index_get()
	{
		$response = ['status' => 'success','data' =>
			[
				'sekolah' => 
				[
					'count' => $this->sekolah->count()
				],
				'program' => 
				[
					'count' => $this->program->count()
				],
				'kegiatan' => 
				[
					'count' => $this->kegiatan->count()
				],
				'rka' => 
				[
					'count' => $this->rka->count()
				],
				'pembantu_bank' => 
				[
					'count' => $this->pembantu_bank->count()
				],
				'bku' => 
				[
					'count' => $this->bku->count()
				],
				'sumber_dana' => 
				[
					'count' => $this->sumber_dana->count()
				]
			]
		];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* footer role */
	public function footer_role_post()
	{
		$this->load->model('footer_role_model','footer_role');
		$this->form_validation->set_rules('data_id', 'data id', 'trim|required|integer');
		$this->form_validation->set_rules('module', 'module', 'trim|required');
		$this->form_validation->set_rules('user_id', 'user', 'trim|required');
		$this->form_validation->set_rules('role', 'role', 'trim|required');
		if($this->form_validation->run() == TRUE)
		{
			$updateOrCreate = $this->footer_role->updateOrCreate([
				'module' => $this->post('module'),
				'data_id' => $this->post('data_id'),
				'role' => $this->post('role')
			],[
				'user_id' => $this->post('user_id')
			]);
			$response = ['status' => 'success','data' => $updateOrCreate];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		------------------------------------------------------------------------
		SUMBER DANA
		------------------------------------------------------------------------
	*/

	/* sumber dana */
	public function sumber_dana_get($id=null)
	{
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->sumber_dana->onlyTrashed()->find($id):$this->sumber_dana->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->sumber_dana;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				$data = $model->limit($limit)->offset($offset)->get();
			}
			else
			{
				$data = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah sumber dana */
	public function add_sumber_dana_post()
	{
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|numeric');
		if($this->form_validation->run() == TRUE)
		{
			$this->sumber_dana->nama = $this->post('nama');
			$this->sumber_dana->jumlah = $this->post('jumlah');
			$this->sumber_dana->save();
			$response = ['status' => 'success','data' => $this->sumber_dana];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui sumber dana */
	public function update_sumber_dana_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->sumber_dana->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('nama', 'nama', 'trim|required');
				$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|numeric');
				if($this->form_validation->run() == TRUE)
				{
					$find->nama = $this->post('nama');
					$find->jumlah = $this->post('jumlah');
					$find->save();
					$response = ['status' => 'success','data' => $find];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus sumber dana */
	public function delete_sumber_dana_get($id=null)
	{
		$find = $this->sumber_dana->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data sumber dana */
	public function restore_sumber_dana_get($id=null)
	{
		$find = $this->sumber_dana->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permanen sumber dana */
	public function force_delete_sumber_dana_get($id=null)
	{
		$find = $this->sumber_dana->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* bulk action sumber dana */
	public function bulk_action_sumber_dana_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->sumber_dana->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->sumber_dana->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->sumber_dana->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		------------------------------------------------------------------------
		KEGIATAN
		------------------------------------------------------------------------
	*/

	/* daftar kegiatan */
	public function kegiatan_get($id=null)
	{
		$program_id = $this->input->get('program_id');
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->kegiatan->onlyTrashed()->find($id):$this->kegiatan->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->kegiatan;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($program_id))?$model = $model->where('program_id',$program_id):false;
				$data = $model->limit($limit)->offset($offset)->get();
			}
			else
			{
				$data = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah kegiatan */
	public function add_kegiatan_post()
	{
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('program', 'program', 'trim|required');
		if($this->form_validation->run() == TRUE)
		{
			$this->kegiatan->nama = $this->post('nama');
			$this->kegiatan->program_id = $this->post('program');
			$this->kegiatan->save();
			$response = ['status' => 'success','data' => $this->kegiatan];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui kegiatan */
	public function update_kegiatan_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->kegiatan->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('nama', 'nama', 'trim|required');
				$this->form_validation->set_rules('program', 'program', 'trim|required');
				if($this->form_validation->run() == TRUE)
				{
					$find->nama = $this->post('nama');
					$find->program_id = $this->post('program');
					$find->save();
					$response = ['status' => 'success','data' => $find];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus kegiatan */
	public function delete_kegiatan_get($id=null)
	{
		$find = $this->kegiatan->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data kegiatan */
	public function restore_kegiatan_get($id=null)
	{
		$find = $this->kegiatan->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permamen kegiatan */
	public function force_delete_kegiatan_get($id=null)
	{
		$find = $this->kegiatan->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);	
	}

	/* bulk action kegiatan */
	public function bulk_action_kegiatan_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->kegiatan->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->kegiatan->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->kegiatan->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		------------------------------------------------------------------------
		PROGRAM
		------------------------------------------------------------------------
	*/

	/* program */
	public function program_get($id=null)
	{
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->program->onlyTrashed()->find($id):$this->program->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->program;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				$data = $model->limit($limit)->offset($offset)->get();
			}
			else
			{
				$data = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah program */
	public function add_program_post()
	{
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		if($this->form_validation->run() == TRUE)
		{
			$this->program->nama = $this->post('nama');
			$this->program->kelompok_sasaran = $this->post('kelompok_sasaran');
			$this->program->save();
			$response = ['status' => 'success','data' => $this->program];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui program */
	public function update_program_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->program->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('nama', 'nama', 'trim|required');
				if($this->form_validation->run() == TRUE)
				{
					$find->nama = $this->post('nama');
					$find->kelompok_sasaran = $this->post('kelompok_sasaran');
					$find->save();
					$response = ['status' => 'success','data' => $find];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus program */
	public function delete_program_get($id=null)
	{
		$find = $this->program->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data program */
	public function restore_program_get($id=null)
	{
		$find = $this->program->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permamen program */
	public function force_delete_program_get($id=null)
	{
		$find = $this->program->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);	
	}

	/* bulk action prgoram */
	public function bulk_action_program_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->program->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->program->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->program->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		------------------------------------------------------------------------
		KODE REKENING
		------------------------------------------------------------------------
	*/

	/* kode rekening */
	public function kode_rekening_get($id=null)
	{
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->kode_rekening->onlyTrashed()->find($id):$this->kode_rekening->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->kode_rekening;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				$data = $model->limit($limit)->offset($offset)->get();
			}
			else
			{
				$data = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah kode rekening */
	public function add_kode_rekening_post()
	{
		$this->form_validation->set_rules('sumber_dana', 'sumber dana', 'trim|required');
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('akun', 'akun', 'trim|required|integer');
		$this->form_validation->set_rules('kelompok', 'kelompok', 'trim|integer');
		$this->form_validation->set_rules('jenis', 'jenis', 'trim|integer');
		if($this->form_validation->run() == TRUE)
		{
			$this->kode_rekening->sumber_dana_id = $this->post('sumber_dana');
			$this->kode_rekening->nama = $this->post('nama');
			$this->kode_rekening->akun = $this->post('akun');
			$this->kode_rekening->kelompok = $this->post('kelompok');
			$this->kode_rekening->jenis = $this->post('jenis');
			$this->kode_rekening->save();
			$response = ['status' => 'success','data' => $this->kode_rekening];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui kode rekening */
	public function update_kode_rekening_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->kode_rekening->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('sumber_dana', 'sumber_dana', 'trim|required');
				$this->form_validation->set_rules('nama', 'nama', 'trim|required');
				$this->form_validation->set_rules('akun', 'akun', 'trim|required|integer');
				$this->form_validation->set_rules('kelompok', 'kelompok', 'trim|integer');
				$this->form_validation->set_rules('jenis', 'jenis', 'trim|integer');
				if($this->form_validation->run() == TRUE)
				{
					$find->sumber_dana_id = $this->post('sumber_dana');
					$find->nama = $this->post('nama');
					$find->akun = $this->post('akun');
					$find->kelompok = $this->post('kelompok');
					$find->jenis = $this->post('jenis');
					$find->save();
					$response = ['status' => 'success','data' => $find];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus kode rekening */
	public function delete_kode_rekening_get($id=null)
	{
		$find = $this->kode_rekening->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data kode rekening */
	public function restore_kode_rekening_get($id=null)
	{
		$find = $this->kode_rekening->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permamen kode rekening */
	public function force_delete_kode_rekening_get($id=null)
	{
		$find = $this->kode_rekening->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);	
	}

	/* bulk action kode rekening */
	public function bulk_action_kode_rekening_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->kode_rekening->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->kode_rekening->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->kode_rekening->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		------------------------------------------------------------------------
		KODE REKENING OBJEK
		------------------------------------------------------------------------
	*/

	/* kode rekening objek */
	public function kode_rekening_objek_get($id=null)
	{
		$kode_rekening_id = $this->input->get('kode_rekening_id');
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->kode_rekening_objek->onlyTrashed()->find($id):$this->kode_rekening_objek->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->kode_rekening_objek;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($kode_rekening_id))?$model = $model->where('kode_rekening_id',$kode_rekening_id):false;
				$data = $model->limit($limit)->offset($offset)->get();
			}
			else
			{
				$data = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah kode rekening objek */
	public function add_kode_rekening_objek_post()
	{
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('kode_rekening_id', 'kode rekening id', 'trim|required');
		if($this->form_validation->run() == TRUE)
		{
			$this->kode_rekening_objek->nama = $this->post('nama');
			$this->kode_rekening_objek->kode_rekening_id = $this->post('kode_rekening_id');
			$this->kode_rekening_objek->save();
			$response = ['status' => 'success','data' => $this->kode_rekening_objek];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui kode rekening objek */
	public function update_kode_rekening_objek_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->kode_rekening_objek->find($id);
			if(!empty($find))
			{
				$find->nama = $this->input->post('nama');
				$find->kode_rekening_id = $this->post('kode_rekening_id');
				$find->save();
				$response = ['status' => 'success','data' => $find];
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus kode rekening objek */
	public function delete_kode_rekening_objek_get($id=null)
	{
		$find = $this->kode_rekening_objek->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data kode rekening objek */
	public function restore_kode_rekening_objek_get($id=null)
	{
		$find = $this->kode_rekening_objek->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permamen kode rekening objek */
	public function force_delete_kode_rekening_objek_get($id=null)
	{
		$find = $this->kode_rekening_objek->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);	
	}

	/* bulk action kode rekening objek */
	public function bulk_action_kode_rekening_objek_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->kode_rekening_objek->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->kode_rekening_objek->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->kode_rekening_objek->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		------------------------------------------------------------------------
		KODE REKENING OBJEK RINCIAN
		------------------------------------------------------------------------
	*/

	/* kode rekening objek rincian */
	public function kode_rekening_objek_rincian_get($id=null)
	{
		$kode_rekening_objek_id = $this->input->get('kode_rekening_objek_id');
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->kode_rekening_objek_rincian->onlyTrashed()->find($id):$this->kode_rekening_objek_rincian->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->kode_rekening_objek_rincian;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($kode_rekening_objek_id))?$model = $model->where('kode_rekening_objek_id',$kode_rekening_objek_id):false;
				$data = $model->limit($limit)->offset($offset)->get();
			}
			else
			{
				$data = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah kode rekening objek rincian */
	public function add_kode_rekening_objek_rincian_post()
	{
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('kode_rekening_objek_id', 'nama objek', 'trim|required');
		if($this->form_validation->run() == TRUE)
		{
			$this->kode_rekening_objek_rincian->nama = $this->post('nama');
			$this->kode_rekening_objek_rincian->kode_rekening_objek_id = $this->input->post('kode_rekening_objek_id');
			$this->kode_rekening_objek_rincian->save();
			$response = ['status' => 'success','data' => $this->kode_rekening_objek_rincian];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui kode rekening objek rincian */
	public function update_kode_rekening_objek_rincian_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->kode_rekening_objek_rincian->find($id);
			if(!empty($find))
			{
				$find->nama = $this->input->post('nama');
				$find->kode_rekening_objek_id = $this->input->post('kode_rekening_objek_id');
				$find->save();
				$response = ['status' => 'success','data' => $find];
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus kode rekening objek rincian */
	public function delete_kode_rekening_objek_rincian_get($id=null)
	{
		$find = $this->kode_rekening_objek_rincian->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data kode rekening objek rincian */
	public function restore_kode_rekening_objek_rincian_get($id=null)
	{
		$find = $this->kode_rekening_objek_rincian->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permamen kode rekening objek rincian */
	public function force_delete_kode_rekening_objek_rincian_get($id=null)
	{
		$find = $this->kode_rekening_objek->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);	
	}

	/* bulk action kode rekening objek rincian */
	public function bulk_action_kode_rekening_objek_rincian_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->kode_rekening_objek_rincian->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->kode_rekening_objek_rincian->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->kode_rekening_objek_rincian->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		------------------------------------------------------------------------
		SEKOLAH
		------------------------------------------------------------------------
	*/
	/* daftar sekolah */
	public function sekolah_get($unique_id=null)
	{
		if(!empty($unique_id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->sekolah->onlyTrashed()->where('id',$unique_id)->orWhere('npsn',$unique_id)->first():$this->sekolah->where('id',$unique_id)->orWhere('npsn',$unique_id)->first();
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->sekolah;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',true),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($this->sekolah_ids))?$model = $model->whereIn('id',$this->sekolah_ids):FALSE;
				$data = $model->limit($limit)->offset($offset)->get();
			}
			else
			{
				$data = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah sekolah */
	public function add_sekolah_post()
	{
		$this->form_validation->set_rules('npsn', 'npsn',['trim','required',['valid_npsn',function($npsn){
			$find = $this->sekolah->where('npsn',$npsn)->first();
			(!empty($find))?$this->form_validation->set_message('valid_npsn','npsn telah di pakai untuk sekolah : '.$find->nama):FALSE;
			return (empty($find));
		}]]);
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('tipe', 'tipe', 'trim|required|in_list[negeri,swasta]');
		if($this->form_validation->run() == TRUE)
		{
			$upload_config = 
			[
				'upload_path' => Modules::run('bos/file/validate_modules_folder',Modules::run('bos/file/site_data_folder').'/sekolah/'),
				'allowed_types' => 'jpg|jpeg|png|gif',
				'max_size' => 124000,
				'max_width' => 124000,
				'max_height' => 124000,
				'min_width' => 0,
				'min_height' => 0,
				'max_filename' => 0,
				'max_filename_increment' => 1000,
				'encrypt_name' => TRUE,
				'file_name' => NULL,
				'file_ext_tolower' => TRUE,
				'remove_spaces' => TRUE,
				'detect_mime' => TRUE,
				'mod_mime_fix' => TRUE
			];

			$logo = Modules::run('bos/file/upload','logo',$upload_config);
			$upload_logo_status = false;
			if(!empty($logo['logo']))
			{
				$this->sekolah->npsn = $this->post('npsn');
				$this->sekolah->nama = $this->post('nama');
				$this->sekolah->tipe = $this->post('tipe');
				$this->sekolah->alamat = $this->post('alamat');
				$this->sekolah->kecamatan = $this->post('kecamatan');
				$this->sekolah->kabupaten = $this->post('kabupaten');
				$this->sekolah->provinsi = $this->post('provinsi');
				$this->sekolah->jumlah_siswa = $this->post('jumlah_siswa');
				$this->sekolah->latitude = $this->post('latitude');
				$this->sekolah->longitude = $this->post('longitude');
				$this->sekolah->status = $this->post('status');
				$this->sekolah->save();

				if(isset($logo['logo']['file_name']))
				{
					$upload_logo_status = true;
					$this->file->module = 'sekolah';
					$this->file->data_id = $this->sekolah->id;
					$this->file->mime = $logo['logo']['file_type'];
					$this->file->slug = url_title($this->sekolah->nama.' '.' '.$this->sekolah->npsn.' '.time());
					$this->file->path = $logo['logo']['full_path'];
					$this->file->name = $logo['logo']['file_name'];
					$this->file->size = $logo['logo']['file_size'];
					$this->file->static = 1;
					$this->file->status = 1;
					$this->file->save();

					// save logo
					$this->sekolah->logo = $this->file->id;
					$this->sekolah->save();
				}
			}

			$response = ['status' => 'success','data' => $this->sekolah];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui sekolah */
	public function update_sekolah_post($unique_id=null)
	{
		if(!empty($unique_id))
		{
			$find = $this->sekolah->where('id',$unique_id)->orWhere('npsn',$unique_id)->first();
			if(!empty($find))
			{
				$upload_config = 
				[
					'upload_path' => Modules::run('bos/file/validate_modules_folder',Modules::run('bos/file/site_data_folder').'/sekolah/'),
					'allowed_types' => 'jpg|jpeg|png|gif',
					'max_size' => 124000,
					'max_width' => 124000,
					'max_height' => 124000,
					'min_width' => 0,
					'min_height' => 0,
					'max_filename' => 0,
					'max_filename_increment' => 1000,
					'encrypt_name' => TRUE,
					'file_name' => NULL,
					'file_ext_tolower' => TRUE,
					'remove_spaces' => TRUE,
					'detect_mime' => TRUE,
					'mod_mime_fix' => TRUE
				];

				$logo = Modules::run('bos/file/upload','logo',$upload_config);
				$upload_logo_status = false;
				if(!empty($logo['logo']))
				{
					if(isset($logo['logo']['file_name']))
					{
						$upload_logo_status = true;
						$this->file->module = 'sekolah';
						$this->file->data_id = $this->sekolah->id;
						$this->file->mime = $logo['logo']['file_type'];
						$this->file->slug = url_title($this->sekolah->nama.' '.' '.$this->sekolah->npsn.' '.time());
						$this->file->path = $logo['logo']['full_path'];
						$this->file->name = $logo['logo']['file_name'];
						$this->file->size = $logo['logo']['file_size'];
						$this->file->static = 1;
						$this->file->status = 1;
						$this->file->save();
						// save logo
						$find->logo = $this->file->id;
					}
				}
				$find->npsn = $this->post('npsn');
				$find->nama = $this->post('nama');
				$find->tipe = $this->post('tipe');
				$find->alamat = $this->post('alamat');
				$find->kecamatan = $this->post('kecamatan');
				$find->kabupaten = $this->post('kabupaten');
				$find->provinsi = $this->post('provinsi');
				$find->jumlah_siswa = $this->post('jumlah_siswa');
				$find->latitude = $this->post('latitude');
				$find->longitude = $this->post('longitude');
				$find->status = $this->post('status');
				$find->save();
				$response = ['status' => 'success','data' => $find];
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus sekolah */
	public function delete_sekolah_get($unique_id=null)
	{
		$find = $this->sekolah->where('id',$unique_id)->orWhere('npsn',$unique_id)->first();
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data sekolah */
	public function restore_sekolah_get($unique_id=null)
	{
		$find = $this->sekolah->withTrashed()->where('id',$unique_id)->orWhere('npsn',$unique_id)->first();
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);	
	}

	/* hapus permanen sekolah */
	public function force_delete_sekolah_get($unique_id=null)
	{
		$find = $this->sekolah->withTrashed()->where('id',$unique_id)->orWhere('npsn',$unique_id)->first();
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* bulk action sekolah */
	public function bulk_action_sekolah_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->sekolah->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->sekolah->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->sekolah->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'enable':
				$execute = $this->sekolah->whereIn('id',$this->post('id'))->update(['status' => 1]);
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'disable':
				$execute = $this->sekolah->whereIn('id',$this->post('id'))->update(['status' => 0]);
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}



	/*
		------------------------------------------------------------------------
		RKA
		------------------------------------------------------------------------
	*/

	/* rka */
	public function rka_get($id=null)
	{
		$sekolah_id = $this->input->get('sekolah_id');
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->rka->onlyTrashed()->find($id):$this->rka->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->rka;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($sekolah_id))?$model = $model->where('sekolah_id',$sekolah_id):FALSE;
				(!empty($this->sekolah_ids) && is_array($this->sekolah_ids))?$model = $model->whereIn('sekolah_id',$this->sekolah_ids):FALSE;
				$get = $model->limit($limit)->offset($offset)->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$data[] = array_merge($value->toArray(),['sekolah' => $this->sekolah->find($value->sekolah_id)->toArray()]);
					}
				}
			}
			else
			{
				$get = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$data[] = array_merge($value->toArray(),['sekolah' => $this->sekolah->find($value->sekolah_id)->toArray()]);
					}
				}
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah rka */
	public function add_rka_post()
	{
		$this->form_validation->set_rules('sekolah_id', 'sekolah', 'trim|required');
		$this->form_validation->set_rules('urusan_pemerintahan', 'urusan pemerintah', 'trim|required');
		$this->form_validation->set_rules('organisasi', 'organisasi', 'trim|required');
		$this->form_validation->set_rules('program', 'program', 'trim|required');
		$this->form_validation->set_rules('kegiatan', 'kegiatan', 'trim|required');
		$this->form_validation->set_rules('sasaran_kegiatan', 'sasaran kegiatan', 'trim|required');
		if($this->form_validation->run() == TRUE)
		{
			$this->rka->sekolah_id = $this->post('sekolah_id');
			$this->rka->urusan_pemerintahan = $this->post('urusan_pemerintahan');
			$this->rka->organisasi = $this->post('organisasi');
			$this->rka->program_id = $this->post('program');
			$this->rka->program = $this->program->find($this->post('program'))->nama;
			$this->rka->kegiatan_id = $this->post('kegiatan');
			$this->rka->kegiatan = $this->kegiatan->find($this->post('kegiatan'))->nama;
			$this->rka->lokasi_kegiatan = $this->sekolah->find($this->post('sekolah_id'))->nama;
			$this->rka->sasaran_kegiatan = $this->post('sasaran_kegiatan');
			$this->rka->jumlah_dana = $this->post('jumlah_dana');
			$this->rka->save();
			$response = ['status' => 'success','data' => $this->rka];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui rka */
	public function update_rka_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->rka->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('sekolah_id', 'sekolah', 'trim|required');
				$this->form_validation->set_rules('urusan_pemerintahan', 'urusan pemerintah', 'trim|required');
				$this->form_validation->set_rules('organisasi', 'organisasi', 'trim|required');
				$this->form_validation->set_rules('program', 'program', 'trim|required');
				$this->form_validation->set_rules('kegiatan', 'kegiatan', 'trim|required');
				$this->form_validation->set_rules('sasaran_kegiatan', 'sasaran kegiatan', 'trim|required');
				if($this->form_validation->run() == TRUE)
				{
					$find->sekolah_id = $this->post('sekolah_id');
					$find->urusan_pemerintahan = $this->post('urusan_pemerintahan');
					$find->organisasi = $this->post('organisasi');
					$find->program_id = $this->post('program');
					$find->program = $this->program->find($this->post('program'))->nama;
					$find->kegiatan_id = $this->post('kegiatan');
					$find->kegiatan = $this->kegiatan->find($this->post('kegiatan'))->nama;
					$find->lokasi_kegiatan = $this->sekolah->find($this->post('sekolah_id'))->nama;
					$find->sasaran_kegiatan = $this->post('sasaran_kegiatan');
					$find->jumlah_dana = $this->post('jumlah_dana');
					$find->save();
					$find->save();
					$response = ['status' => 'success','data' => $find];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus rka */
	public function delete_rka_get($id=null)
	{
		$find = $this->rka->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data rka */
	public function restore_rka_get($id=null)
	{
		$find = $this->rka->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permanen rka */
	public function force_delete_rka_get($id=null)
	{
		$find = $this->rka->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* bulk action rka  */
	public function bulk_action_rka_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->rka->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->rka->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->rka->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}


	/* set rka status */
	public function set_status_rka_get($id=null,$option='publish')
	{
		$find = $this->rka->find($id);
		$response = (!empty($find))?['status' => ($find->update(['status' => ($option == 'publish')?1:0]))?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		===============================================
		RKA URAIAN
		===============================================
	*/

	/* rka uraian */
	public function rka_uraian_get($id=null)
	{
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->rka_uraian->onlyTrashed()->find($id):$this->rka_uraian->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->rka_uraian;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				$data = $model->limit($limit)->offset($offset)->get();
			}
			else
			{
				$data = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* add rka uraian */
	public function add_rka_uraian_post()
	{
		$this->form_validation->set_rules('rka_id', 'rka id', 'trim|required|integer');
		$this->form_validation->set_rules('kode_rekening', 'kode rekening', 'trim|required|integer');
		$this->form_validation->set_rules('kode_rekening_objek', 'kode rekening objek', 'trim|integer');
		if($this->form_validation->run() == TRUE)
		{
			$this->rka_uraian->rka_id = $this->post('rka_id');
			$this->rka_uraian->kode_rekening_id = $this->post('kode_rekening');
			$this->rka_uraian->kode_rekening_objek_id = $this->post('kode_rekening_objek');
			$this->rka_uraian->save();
			$response = ['status' => 'success','data' => $this->rka_uraian];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* update rka uraian */
	public function update_rka_uraian_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->rka_uraian->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('rka_id', 'rka id', 'trim|required|integer');
				$this->form_validation->set_rules('kode_rekening_id', 'kode rekening', 'trim|required|integer');
				$this->form_validation->set_rules('kode_rekening_objek_id', 'kode rekening objek', 'trim|integer');
				if($this->form_validation->run() == TRUE)
				{
					$find->rka_id = $this->post('rka_id');
					$find->kode_rekening_id = $this->post('kode_rekening_id');
					$find->kode_rekening_objek_id = $this->post('kode_rekening_objek_id');
					$find->save();
					$response = ['status' => 'success','data' => $find];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus rka uraian */
	public function delete_rka_uraian_get($id=null)
	{
		$find = $this->rka_uraian->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data rka uraian */
	public function restore_rka_uraian_get($id=null)
	{
		$find = $this->rka_uraian->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permanen rka uraian */
	public function force_delete_rka_uraian_get($id=null)
	{
		$find = $this->rka_uraian->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* bulk action rka uraian */
	public function bulk_action_rka_uraian_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->rka_uraian->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->rka_uraian->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->rka_uraian->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		===============================================
		RKA RINCIAN
		===============================================
	*/

	/* rka rincian */
	public function rka_rincian_get($rka_uraian_id=null)
	{
		$model = $this->rka_rincian;
		$data = array();
		$record_total = $model->count();
		$query = $this->input->get();
		if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
		{
			$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
			$offset = (isset($query['start']))? $query['start'] : 0;
			$model = datatable_query($model,$query);
			$data = $model->limit($limit)->offset($offset)->get();
		}
		else
		{
			$data = (isset($query['in_trash']))?$model->onlyTrashed()->get():$model->get();
		}
		
		// Set Response
		$response = [
			'draw' => (isset($query['draw']))?$query['draw']:false,
			'record_total' => $record_total,
			'record_filtered' => $record_total,
			'data' => $data
		];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* add rka rincian */
	public function add_rka_rincian_post()
	{
		$this->form_validation->set_rules('rka_uraian_id', 'uraian', 'trim|required|integer');
		$this->form_validation->set_rules('kode_rekening_objek_rincian_id', 'rincian', 'trim|required|integer');
		if($this->form_validation->run() == TRUE)
		{
			$this->rka_rincian->rka_uraian_id = $this->post('rka_uraian_id');
			$this->rka_rincian->kode_rekening_objek_rincian_id = $this->post('kode_rekening_objek_rincian_id');
			$this->rka_rincian->save();
			$response = ['status' => 'success','data' => $this->rka_rincian];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* update rka rincian */
	public function update_rka_rincian_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->rka_rincian->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('rka_id', 'rka id', 'trim|required|integer');
				$this->form_validation->set_rules('kode_rekening_id', 'kode rekening', 'trim|required|integer');
				$this->form_validation->set_rules('kode_rekening_objek_id', 'kode rekening objek', 'trim|integer');
				if($this->form_validation->run() == TRUE)
				{
					$find->rka_id = $this->post('rka_id');
					$find->kode_rekening_id = $this->post('kode_rekening_id');
					$find->kode_rekening_objek_id = $this->post('kode_rekening_objek_id');
					$find->save();
					$response = ['status' => 'success','data' => $find];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus rka rincian */
	public function delete_rka_rincian_get($id=null)
	{
		$find = $this->rka_rincian->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data rka rincian */
	public function restore_rka_rincian_get($id=null)
	{
		$find = $this->rka_rincian->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permanen rka rincian */
	public function force_delete_rka_rincian_get($id=null)
	{
		$find = $this->rka_rincian->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* bulk action rka rincian */
	public function bulk_action_rka_rincian_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->rka_rincian->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->rka_rincian->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->rka_rincian->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		===============================================
		RKA URAIAN RINCIAN
		===============================================
	*/

	/* rka uraian rincian */
	public function rka_uraian_rincian_get($id=null)
	{
		$rka_id = $this->input->get('rka_id');
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->rka_uraian_rincian->onlyTrashed()->find($id):$this->rka_uraian_rincian->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->rka_uraian_rincian;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($rka_id))?$model = $model->where('rka_id',$rka_id):FALSE;
				$data = $model->limit($limit)->offset($offset)->get();
			}
			else
			{
				$data = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* add rka uraian rincian */
	public function add_rka_uraian_rincian_post()
	{
		$this->form_validation->set_rules('rka_id', 'rka id', 'trim|required|integer');
		$this->form_validation->set_rules('rka_rincian_id', 'rka rincian', 'trim|required|integer');
		$this->form_validation->set_rules('rincian', 'rincian', 'trim|required');
		$this->form_validation->set_rules('volume', 'volume', 'trim|required|numeric');
		$this->form_validation->set_rules('satuan', 'satuan', 'trim|required');
		$this->form_validation->set_rules('harga', 'harga', 'trim|required|numeric');
		if($this->form_validation->run() == TRUE)
		{
			$this->rka_uraian_rincian->rka_id = $this->post('rka_id');
			$this->rka_uraian_rincian->rka_rincian_id = $this->post('rka_rincian_id');
			$this->rka_uraian_rincian->rincian = $this->post('rincian');
			$this->rka_uraian_rincian->volume = $this->post('volume');
			$this->rka_uraian_rincian->satuan = $this->post('satuan');
			$this->rka_uraian_rincian->harga = $this->post('harga');
			$this->rka_uraian_rincian->jumlah = $this->post('volume')*$this->post('harga');
			$this->rka_uraian_rincian->save();
			$response = ['status' => 'success','data' => $this->rka_uraian_rincian];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* update rka uraian rincian */
	public function update_rka_uraian_rincian_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->rka_uraian_rincian->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('rincian', 'rincian', 'trim|required');
				$this->form_validation->set_rules('volume', 'volume', 'trim|required');
				$this->form_validation->set_rules('satuan', 'satuan', 'trim|required');
				$this->form_validation->set_rules('harga', 'harga', 'trim|required');
				if($this->form_validation->run() == TRUE)
				{
					$find->rincian = $this->post('rincian');
					$find->volume = $this->post('volume');
					$find->satuan = $this->post('satuan');
					$find->harga = $this->post('harga');
					$find->jumlah = $this->post('volume')*$this->post('harga');
					$find->save();
					$response = ['status' => 'success','data' => $find];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus rka uraian rincian */
	public function delete_rka_uraian_rincian_get($id=null)
	{
		$find = $this->rka_uraian_rincian->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data rka uraian rincian */
	public function restore_rka_uraian_rincian_get($id=null)
	{
		$find = $this->rka_uraian_rincian->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permanen rka uraian rincian */
	public function force_delete_rka_uraian_rincian_get($id=null)
	{
		$find = $this->rka_uraian_rincian->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* bulk action rka uraian rincian */
	public function bulk_action_rka_uraian_rincian_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->rka_uraian_rincian->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->rka_uraian_rincian->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->rka_uraian_rincian->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		------------------------------------------------------------------------
		PEMBANTU KAS
		------------------------------------------------------------------------
	*/

	/* list pembantu kas */
	public function pembantu_kas_get($id=null)
	{
		$sekolah_id = $this->input->get('sekolah_id');
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->pembantu_kas->onlyTrashed()->find($id):$this->pembantu_kas->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->pembantu_kas;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($sekolah_id))?$model = $model->where('sekolah_id',$sekolah_id):FALSE;
				(!empty($this->sekolah_ids) && is_array($this->sekolah_ids))?$model = $model->whereIn('sekolah_id',$this->sekolah_ids):FALSE;
				$get = $model->limit($limit)->offset($offset)->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$data[] = array_merge($value->toArray(),['sekolah' => $this->sekolah->find($value->sekolah_id)->toArray()]);
					}
				}
			}
			else
			{
				$get = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$data[] = array_merge($value->toArray(),['sekolah' => $this->sekolah->find($value->sekolah_id)->toArray()]);
					}
				}
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah pembantu kas */
	public function add_pembantu_kas_post()
	{
		$this->form_validation->set_rules('sekolah_id', 'sekolah', 'trim|required');
		$this->form_validation->set_rules('bulan', 'bulan', 'trim|required|integer');
		$this->form_validation->set_rules('tahun', 'tahun', 'trim|required|integer|max_length[4]');
		if($this->form_validation->run() == TRUE)
		{
			$this->pembantu_kas->sekolah_id = $this->post('sekolah_id');
			$this->pembantu_kas->bulan = $this->post('bulan');
			$this->pembantu_kas->tahun = $this->post('tahun');
			$this->pembantu_kas->save();
			$response = ['status' => 'success','data' => $this->pembantu_kas];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui pembantu kas */
	public function update_pembantu_kas_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->pembantu_kas->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('sekolah_id', 'sekolah', 'trim|required');
				$this->form_validation->set_rules('bulan', 'bulan', 'trim|required|integer');
				$this->form_validation->set_rules('tahun', 'tahun', 'trim|required|integer|max_length[4]');
				if($this->form_validation->run() == TRUE)
				{
					$find->sekolah_id = $this->post('sekolah_id');
					$find->bulan = $this->post('bulan');
					$find->tahun = $this->post('tahun');
					$find->save();
					$response = ['status' => 'success','data' => $find];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus pembantu kas */
	public function delete_pembantu_kas_get($id=null)
	{
		$find = $this->pembantu_kas->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data pembantu kas */
	public function restore_pembantu_kas_get($id=null)
	{
		$find = $this->pembantu_kas->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permanen pembantu kas */
	public function force_delete_pembantu_kas_get($id=null)
	{
		$find = $this->pembantu_kas->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* bulk action pembantu kas */
	public function bulk_action_pembantu_kas_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->pembantu_kas->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->pembantu_kas->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->pembantu_kas->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* set status pembantu kas */
	public function set_status_pembantu_kas_get($id=null,$option='publish')
	{
		$find = $this->pembantu_kas->find($id);
		$response = (!empty($find))?['status' => ($find->update(['status' => ($option == 'publish')?1:0]))?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		===============================================
		PEMBANTU KAS DATA
		===============================================
	*/

	/* list pembantu kas data */
	public function pembantu_kas_data_get($id=null)
	{
		$pembantu_kas_id = $this->input->get('pembantu_kas_id');
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->bku_data->onlyTrashed()->find($id):$this->bku_data->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->bku_data;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($pembantu_kas_id))?$model = $model->where('pembantu_kas_id',$pembantu_kas_id):FALSE;
				$data = $model->limit($limit)->offset($offset)->get();
			}
			else
			{
				$data = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah pembantu kas data */
	public function add_pembantu_kas_data_post()
	{
		$this->form_validation->set_rules('pembantu_kas_id', 'pembantu kas', 'trim|required');
		$this->form_validation->set_rules('uraian', 'uraian', 'trim|required');
		$this->form_validation->set_rules('jenis', 'jenis', 'trim|required|in_list[pemasukan,pengeluaran]');
		$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|numeric');
		if($this->form_validation->run() == TRUE)
		{
			$this->pembantu_kas_data->pembantu_kas_id = $this->post('pembantu_kas_id');
			$this->pembantu_kas_data->kode_rekening = $this->post('kode_rekening');
			$this->pembantu_kas_data->nomor_bukti = $this->post('nomor_bukti');
			$this->pembantu_kas_data->uraian = $this->post('uraian');
			$this->pembantu_kas_data->{$this->post('jenis')} = $this->post('jumlah');
			$this->pembantu_kas_data->save();
			$response = ['status' => 'success','data' => $this->pembantu_kas_data];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui pembantu kas data */
	public function update_pembantu_kas_data_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->pembantu_kas->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('uraian', 'uraian', 'trim|required');
				$this->form_validation->set_rules('jenis', 'jenis', 'trim|required|in_list[pemasukan,pengeluaran]');
				$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|numeric');
				if($this->form_validation->run() == TRUE)
				{
					$find->kode_rekening = $this->post('kode_rekening');
					$find->nomor_bukti = $this->post('nomor_bukti');
					$find->uraian = $this->post('uraian');
					$find->jenis = $this->post('jenis');
					$find->jumlah = $this->post('jumlah');
					$find->save();
					$response = ['status' => 'success','data' => $find];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus pembantu kas data */
	public function delete_pembantu_kas_data_get($id=null)
	{
		$find = $this->pembantu_kas_data->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data pembantu kas data */
	public function restore_pembantu_kas_data_get($id=null)
	{
		$find = $this->pembantu_kas_data->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permanen pembantu kas data */
	public function force_delete_pembantu_kas_data_get($id=null)
	{
		$find = $this->pembantu_kas_data->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* bulk action pembantu kas data */
	public function bulk_action_pembantu_kas_data_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->pembantu_kas_data->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->pembantu_kas_data->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->pembantu_kas_data->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

		/*
		------------------------------------------------------------------------
		PEMBANTU BANK
		------------------------------------------------------------------------
	*/

	/* list pembantu bank */
	public function pembantu_bank_get($id=null)
	{
		$sekolah_id = $this->input->get('sekolah_id');
		$month = $this->input->get('month');
		$year = $this->input->get('year');
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->pembantu_bank->onlyTrashed()->find($id):$this->pembantu_bank->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->pembantu_bank;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($sekolah_id))?$model = $model->where('sekolah_id',$sekolah_id):FALSE;
				(!empty($this->sekolah_ids) && is_array($this->sekolah_ids))?$model = $model->whereIn('sekolah_id',$this->sekolah_ids):FALSE;
				(!empty($month))?$model = $model->where('bulan',$month):FALSE;
				(!empty($year))?$model = $model->where('tahun',$year):FALSE;
				$get = $model->limit($limit)->offset($offset)->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$data[] = array_merge($value->toArray(),['sekolah' => $this->sekolah->find($value->sekolah_id)->toArray()]);
					}
				}
			}
			else
			{
				$get = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$data[] = array_merge($value->toArray(),['sekolah' => $this->sekolah->find($value->sekolah_id)->toArray()]);
					}
				}
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah pembantu bank */
	public function add_pembantu_bank_post()
	{
		$this->form_validation->set_rules('sekolah_id', 'sekolah', 'trim|required');
		$this->form_validation->set_rules('bulan', 'bulan', 'trim|required|integer');
		$this->form_validation->set_rules('tahun', 'tahun', 'trim|required|integer|max_length[4]');
		if($this->form_validation->run() == TRUE)
		{
			$this->pembantu_bank->sekolah_id = $this->post('sekolah_id');
			$this->pembantu_bank->bulan = $this->post('bulan');
			$this->pembantu_bank->tahun = $this->post('tahun');
			$this->pembantu_bank->save();
			$response = ['status' => 'success','data' => $this->pembantu_bank];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui pembantu bank */
	public function update_pembantu_bank_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->pembantu_bank->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('sekolah_id', 'sekolah', 'trim|required');
				$this->form_validation->set_rules('bulan', 'bulan', 'trim|required|integer');
				$this->form_validation->set_rules('tahun', 'tahun', 'trim|required|integer|max_length[4]');
				if($this->form_validation->run() == TRUE)
				{
					$find->sekolah_id = $this->post('sekolah_id');
					$find->bulan = $this->post('bulan');
					$find->tahun = $this->post('tahun');
					$find->save();
					$response = ['status' => 'success','data' => $find,'post' => $this->post()];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus pembantu bank */
	public function delete_pembantu_bank_get($id=null)
	{
		$find = $this->pembantu_bank->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data pembantu bank */
	public function restore_pembantu_bank_get($id=null)
	{
		$find = $this->pembantu_bank->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permanen pembantu bank */
	public function force_delete_pembantu_bank_get($id=null)
	{
		$find = $this->pembantu_bank->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* bulk action pembantu bank */
	public function bulk_action_pembantu_bank_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->pembantu_bank->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->pembantu_bank->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->pembantu_bank->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		===============================================
		PEMBANTU BANK DATA
		===============================================
	*/

	/* list pembantu bank data */
	public function pembantu_bank_data_get($id=null)
	{
		$pembantu_bank_id = $this->input->get('pembantu_bank_id');
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->bku_data->onlyTrashed()->find($id):$this->bku_data->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->bku_data;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($pembantu_bank_id))?$model = $model->where('pembantu_bank_id',$pembantu_bank_id):FALSE;
				$data = $model->limit($limit)->offset($offset)->get();
			}
			else
			{
				$data = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah pembantu bank data */
	public function add_pembantu_bank_data_post()
	{
		$this->form_validation->set_rules('pembantu_bank_id', 'pembantu bank', 'trim|required');
		$this->form_validation->set_rules('uraian', 'uraian', 'trim|required');
		$this->form_validation->set_rules('jenis', 'jenis', 'trim|required|in_list[pemasukan,pengeluaran]');
		$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|numeric');
		if($this->form_validation->run() == TRUE)
		{
			$this->pembantu_bank_data->pembantu_bank_id = $this->post('pembantu_bank_id');
			$this->pembantu_bank_data->kode_rekening = $this->post('kode_rekening');
			$this->pembantu_bank_data->nomor_bukti = $this->post('nomor_bukti');
			$this->pembantu_bank_data->uraian = $this->post('uraian');
			$this->pembantu_bank_data->{$this->post('jenis')} = $this->post('jumlah');
			$this->pembantu_bank_data->save();
			$response = ['status' => 'success','data' => $this->pembantu_bank_data];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui pembantu bank data */
	public function update_pembantu_bank_data_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->pembantu_bank->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('uraian', 'uraian', 'trim|required');
				$this->form_validation->set_rules('jenis', 'jenis', 'trim|required|in_list[pemasukan,pengeluaran]');
				$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|numeric');
				if($this->form_validation->run() == TRUE)
				{
					$find->kode_rekening = $this->post('kode_rekening');
					$find->nomor_bukti = $this->post('nomor_bukti');
					$find->uraian = $this->post('uraian');
					$find->jenis = $this->post('jenis');
					$find->jumlah = $this->post('jumlah');
					$find->save();
					$response = ['status' => 'success','data' => $find];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus pembantu bank data */
	public function delete_pembantu_bank_data_get($id=null)
	{
		$find = $this->pembantu_bank_data->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data pembantu bank data */
	public function restore_pembantu_bank_data_get($id=null)
	{
		$find = $this->pembantu_bank_data->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permanen pembantu bank data */
	public function force_delete_pembantu_bank_data_get($id=null)
	{
		$find = $this->pembantu_bank_data->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* bulk action pembantu bank data */
	public function bulk_action_pembantu_bank_data_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->pembantu_bank_data->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->pembantu_bank_data->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->pembantu_bank_data->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* set status pembantu bank */
	public function set_status_pembantu_bank_get($id=null,$option='publish')
	{
		$find = $this->pembantu_bank->find($id);
		$response = (!empty($find))?['status' => ($find->update(['status' => ($option == 'publish')?1:0]))?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}
	
	/*
		------------------------------------------------------------------------
		BKU
		------------------------------------------------------------------------
	*/

	/* list bku */
	public function bku_get($id=null)
	{
		$sekolah_id = $this->input->get('sekolah_id');
		$date = (!empty($this->input->get('date')))?nice_date($this->input->get('date'),'y-m-d'):null;
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->bku->onlyTrashed()->find($id):$this->bku->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->bku;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($this->sekolah_ids) && is_array($this->sekolah_ids))?$model = $model->whereIn('sekolah_id',$this->sekolah_ids):FALSE;
				(!empty($sekolah_id))?$model = $model->where('sekolah_id',$sekolah_id):FALSE;
				(!empty($date))?$model = $model->whereDate('created_at',$date):FALSE;
				$get = $model->limit($limit)->offset($offset)->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$data[] = array_merge($value->toArray(),['sekolah' => $this->sekolah->find($value->sekolah_id)->toArray()]);
					}
				}
			}
			else
			{
				$get = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$data[] = array_merge($value->toArray(),['sekolah' => $this->sekolah->find($value->sekolah_id)->toArray()]);
					}
				}
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah bku */
	public function add_bku_post()
	{
		$this->form_validation->set_rules('sekolah_id', 'sekolah', 'trim|required');
		$this->form_validation->set_rules('bulan', 'bulan', 'trim|required|integer');
		$this->form_validation->set_rules('tahun', 'tahun', 'trim|required|integer|max_length[4]');
		if($this->form_validation->run() == TRUE)
		{
			$this->bku->sekolah_id = $this->post('sekolah_id');
			$this->bku->bulan = $this->post('bulan');
			$this->bku->tahun = $this->post('tahun');
			$this->bku->save();
			$response = ['status' => 'success','data' => $this->bku];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui bku */
	public function update_bku_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->bku->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('sekolah_id', 'sekolah', 'trim|required');
				$this->form_validation->set_rules('bulan', 'bulan', 'trim|required|integer');
				$this->form_validation->set_rules('tahun', 'tahun', 'trim|required|integer|max_length[4]');
				if($this->form_validation->run() == TRUE)
				{
					$find->sekolah_id = $this->post('sekolah_id');
					$find->bulan = $this->post('bulan');
					$find->tahun = $this->post('tahun');
					$find->save();
					$response = ['status' => 'success','data' => $find,'pos' => $this->post()];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus bku */
	public function delete_bku_get($id=null)
	{
		$find = $this->bku->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data bku */
	public function restore_bku_get($id=null)
	{
		$find = $this->bku->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permanen bku */
	public function force_delete_bku_get($id=null)
	{
		$find = $this->bku->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* bulk action bku */
	public function bulk_action_bku_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->bku->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->bku->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->bku->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* set bku status */
	public function set_status_bku_get($id=null,$option='publish')
	{
		$find = $this->bku->find($id);
		$response = (!empty($find))?['status' => ($find->update(['status' => ($option == 'publish')?1:0]))?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		===============================================
		BKU DATA
		===============================================
	*/

	/* list bku data */
	public function bku_data_get($id=null)
	{
		$bku_id = $this->input->get('bku_id');
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->bku_data->onlyTrashed()->find($id):$this->bku_data->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->bku_data;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($bku_id))?$model = $model->where('bku_id',$bku_id):FALSE;
				$data = $model->limit($limit)->offset($offset)->get();
			}
			else
			{
				$data = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah bku data */
	public function add_bku_data_post()
	{
		$this->form_validation->set_rules('bku_id', 'bku', 'trim|required');
		$this->form_validation->set_rules('uraian', 'uraian', 'trim|required');
		$this->form_validation->set_rules('jenis', 'jenis', 'trim|required|in_list[pemasukan,pengeluaran]');
		$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|numeric');
		if($this->form_validation->run() == TRUE)
		{
			$this->bku_data->bku_id = $this->post('bku_id');
			$this->bku_data->kode_rekening = $this->post('kode_rekening');
			$this->bku_data->nomor_bukti = $this->post('nomor_bukti');
			$this->bku_data->uraian = $this->post('uraian');
			$this->bku_data->{$this->post('jenis')} = $this->post('jumlah');
			$this->bku_data->save();
			$response = ['status' => 'success','data' => $this->bku_data];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui bku data */
	public function update_bku_data_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->bku->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('uraian', 'uraian', 'trim|required');
				$this->form_validation->set_rules('jenis', 'jenis', 'trim|required|in_list[pemasukan,pengeluaran]');
				$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|numeric');
				if($this->form_validation->run() == TRUE)
				{
					$find->kode_rekening = $this->post('kode_rekening');
					$find->nomor_bukti = $this->post('nomor_bukti');
					$find->uraian = $this->post('uraian');
					$find->jenis = $this->post('jenis');
					$find->jumlah = $this->post('jumlah');
					$find->save();
					$response = ['status' => 'success','data' => $find];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus bku data */
	public function delete_bku_data_get($id=null)
	{
		$find = $this->bku_data->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data bku data */
	public function restore_bku_data_get($id=null)
	{
		$find = $this->bku_data->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permanen bku data */
	public function force_delete_bku_data_get($id=null)
	{
		$find = $this->bku_data->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* bulk action bku data */
	public function bulk_action_bku_data_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->bku_data->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->bku_data->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->bku_data->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*
		------------------------------------------------------------------------
		PENUTUPAN KAS
		------------------------------------------------------------------------
	*/

	/* pembantu kas */
	public function penutupan_kas_get($id=null)
	{
		$sekolah_id = $this->input->get('sekolah_id');
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->penutupan_kas->onlyTrashed()->find($id):$this->penutupan_kas->find($id);
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->penutupan_kas;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',false),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($sekolah_id))?$model = $model->where('sekolah_id',$sekolah_id):FALSE;
				(!empty($this->sekolah_ids) && is_array($this->sekolah_ids))?$model = $model->whereIn('sekolah_id',$this->sekolah_ids):FALSE;
				$get = $model->limit($limit)->offset($offset)->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$data[] = array_merge($value->toArray(),['sekolah' => $this->sekolah->find($value->sekolah_id)->toArray()]);
					}
				}
			}
			else
			{
				$get = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$data[] = array_merge($value->toArray(),['sekolah' => $this->sekolah->find($value->sekolah_id)->toArray()]);
					}
				}
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* tambah pembantu kas */
	public function add_penutupan_kas_post()
	{
		$this->form_validation->set_rules('sekolah_id', 'sekolah', 'trim|required');
		$this->form_validation->set_rules('tanggal', 'tanggal penutupan', 'trim|required');
		$this->form_validation->set_rules('nama_penutup_kas', 'nama penutup kas', 'trim|required');
		if($this->form_validation->run() == TRUE)
		{
			$this->penutupan_kas->sekolah_id = $this->post('sekolah_id');
			$this->penutupan_kas->tanggal = nice_date($this->post('tanggal'),'d-m-y');
			$this->penutupan_kas->pemasukan = $this->post('pemasukan');
			$this->penutupan_kas->pengeluaran = $this->post('pengeluaran');
			$this->penutupan_kas->nama_penutup_kas = $this->post('nama_penutup_kas');
			$this->penutupan_kas->user_id_penutup_kas = (!empty($this->session->userdata('user_id')))?$this->session->userdata('user_id'):0;
			$this->penutupan_kas->saldo_buku = $this->post('saldo_buku');
			$this->penutupan_kas->saldo_kas = $this->post('saldo_kas');
			$this->penutupan_kas->saldo_bank = $this->post('saldo_bank');
			$this->penutupan_kas->saldo_kas_rincian = $this->post('saldo_kas_rincian');
			$this->penutupan_kas->deskripsi_perbedaan_kas_bank = $this->post('deskripsi_perbedaan_kas_bank');
			$this->penutupan_kas->status = 1;
			$this->penutupan_kas->save();
			$response = ['status' => 'success','data' => $this->penutupan_kas];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* perbaharui pembantu kas */
	public function update_penutupan_kas_post($id=null)
	{
		if(!empty($id))
		{
			$find = $this->penutupan_kas->find($id);
			if(!empty($find))
			{
				$this->form_validation->set_rules('sekolah_id', 'sekolah', 'trim|required');
				$this->form_validation->set_rules('urusan_pemerintahan', 'urusan pemerintah', 'trim|required');
				$this->form_validation->set_rules('organisasi', 'organisasi', 'trim|required');
				$this->form_validation->set_rules('program', 'program', 'trim|required');
				$this->form_validation->set_rules('kegiatan', 'kegiatan', 'trim|required');
				$this->form_validation->set_rules('sasaran_kegiatan', 'sasaran kegiatan', 'trim|required');
				if($this->form_validation->run() == TRUE)
				{
					$find->sekolah_id = $this->post('sekolah_id');
					$find->urusan_pemerintahan = $this->post('urusan_pemerintahan');
					$find->organisasi = $this->post('organisasi');
					$find->program_id = $this->post('program');
					$find->program = $this->program->find($this->post('program'))->nama;
					$find->kegiatan_id = $this->post('kegiatan');
					$find->kegiatan = $this->kegiatan->find($this->post('kegiatan'))->nama;
					$find->lokasi_kegiatan = $this->sekolah->find($this->post('sekolah_id'))->nama;
					$find->sasaran_kegiatan = $this->post('sasaran_kegiatan');
					$find->jumlah_dana = $this->post('jumlah_dana');
					$find->save();
					$find->save();
					$response = ['status' => 'success','data' => $find];
				}
				else
				{
					$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
					array_shift($validation_errors);
					$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
				}
			}
			else
			{
				$response = ['status' => 'failed','message' => 'data_not_found'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'invalid_request'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus pembantu kas */
	public function delete_penutupan_kas_get($id=null)
	{
		$find = $this->penutupan_kas->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data pembantu kas */
	public function restore_penutupan_kas_get($id=null)
	{
		$find = $this->penutupan_kas->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus permanen pembantu kas */
	public function force_delete_penutupan_kas_get($id=null)
	{
		$find = $this->penutupan_kas->withTrashed()->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* bulk action penutupan_kas  */
	public function bulk_action_penutupan_kas_post()
	{
		switch ($this->post('action'))
		{
			case 'delete':
				$execute = $this->penutupan_kas->destroy($this->post('id'));
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'restore':
				$execute = $this->penutupan_kas->withTrashed()->whereIn('id',$this->post('id'))->restore();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;

			case 'force delete':
				$execute = $this->penutupan_kas->withTrashed()->whereIn('id',$this->post('id'))->forceDelete();
				$response = ($execute)?['status' => 'success']:['status' => 'failed','message' => 'bad_request'];
			break;
			
			default:
				$response = ['status' => 'failed','message' => 'invalid_request'];
			break;
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* latest penutupan kas */
	public function latest_penutupan_kas_get($sekolah_id=null)
	{
		$get_data = $this->penutupan_kas->where('sekolah_id',$sekolah_id)->where('sekolah_id',$sekolah_id)->orderBy('id','desc')->first();
		$response = ['status' => 'success','data' => $get_data];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* set penutupan kas status */
	public function set_status_penutupan_kas_get($id=null,$option='publish')
	{
		$find = $this->penutupan_kas->find($id);
		$response = (!empty($find))?['status' => ($find->update(['status' => ($option == 'publish')?1:0]))?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}
}
/* End of file Bos.php */
/* Location: ./application/modules/bos/controllers/Bos.php */