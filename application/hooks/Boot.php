<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Boot
{
	protected $app_config = FCPATH.'app.config';
	protected $database_config = FCPATH.'.database';

	/**
	 * constructor class
	*/
	public function __construct()
	{

	}

	/**
	 * All Application Config File
	 * @method check all config file redirect to installation if not exist
	 * @return exit
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function AllAppConfigFile()
	{
		if(!file_exists($this->app_config) && !file_exists($this->database_config))
		{
			if(!preg_match('/(installation)/', CURRENT_URL))
			{
				header('location:'.BASE_URL.'installation?code=start');
				exit;
			}
		}
	}

	/**
	 * Site Data Fodler
	 * @method auto create site data folder at application root
	 * @return null
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function SiteDataFolder()
	{
		if(file_exists($this->app_config))
		{
			$decode_content = json_decode(file_get_contents($this->app_config));
		}

		$object = (isset($decode_content))?$decode_content:(object) array();

		// Set Site Folder If Not Exsist
		if(!isset($object->site_folder))
		{
			$object->site_folder = 'site_data_'.time();
			(!is_dir(FCPATH.$object->site_folder))?mkdir(FCPATH.$object->site_folder,0777,TRUE):FALSE;
			$object->site_folder = realpath($object->site_folder);
			file_put_contents($this->app_config, json_encode($object,JSON_PRETTY_PRINT));
		}
	}

	/**
	 * Site Path
	 * @method create site path for webpack site_path
	 * @return null
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function SitePath()
	{
		if(file_exists($this->app_config))
		{
			$decode_content = json_decode(file_get_contents($this->app_config));
		}

		$object = (isset($decode_content))?$decode_content:(object) array();

		// Set Site Folder If Not Exsist
		if(!isset($object->site_path))
		{
			$object->site_path = (isset(parse_url(BASE_URL)['path']))?parse_url(BASE_URL)['path']:'/';
			file_put_contents($this->app_config, json_encode($object,JSON_PRETTY_PRINT));
		}
	}

	/**
	 * Site URL
	 * @method create site url for webpack site_url
	 * @return null
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function SiteURL()
	{
		if(file_exists($this->app_config))
		{
			$decode_content = json_decode(file_get_contents($this->app_config));
		}

		$object = (isset($decode_content))?$decode_content:(object) array();

		// Set Site Folder If Not Exsist
		if(!isset($object->site_url))
		{
			$object->site_url = BASE_URL;
			file_put_contents($this->app_config, json_encode($object,JSON_PRETTY_PRINT));
		}
	}

	/**
	 * Prefix Table
	 * @method prefix table on installation
	 * @return null
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function TablePrefix()
	{
		if(file_exists($this->app_config))
		{
			$decode_content = json_decode(file_get_contents($this->app_config));
		}

		$object = (isset($decode_content))?$decode_content:(object) array();

		// Set Site Folder If Not Exsist
		if(!isset($object->table_prefix))
		{
			$object->table_prefix = APP['name'].'_';
			file_put_contents($this->app_config, json_encode($object,JSON_PRETTY_PRINT));
		}
	}

	/**
	 * Database Config File
	 * @method check .database config file & redirect to installation if not exsists
	 * @return false
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function DatabaseConfigFile()
	{
		if(!file_exists($this->database_config))
		{
			// Redirect Install Database
			$current_url = 
			($_SERVER['SERVER_PORT'] == 443)?'https':'http'."://"
			.(!empty($_SERVER['SERVER_NAME'])?$_SERVER['SERVER_NAME']:FALSE)
			.str_replace('//', '/', $_SERVER['REQUEST_URI']);

			if(!preg_match('/(installation)/', $current_url))
			{
				header('location:'.BASE_URL.'installation/database?code=initalize_database');
				exit();
			}
		}
	}

	/**
	 * @method check databsae connection from .database config default set by codeigniter environment & redirect to installation if not error
	 * @return string
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function DatabaseConnection()
	{
		if(file_exists($this->database_config))
		{
			// Get Config File
			$db_config = json_decode(file_get_contents($this->database_config),TRUE);

			// Configure Database
			$capsule = new Illuminate\Database\Capsule\Manager;
			foreach ($db_config as $db_group => $config)
			{
			    $capsule->addConnection([
			        'driver'    => ($config['dbdriver'] == 'mysqli')?'mysql':$config['dbdriver'],
			        'host'      => $config['hostname'],
			        'database'  => $config['database'],
			        'username'  => $config['username'],
			        'password'  => $config['password'],
			        'charset'   => $config['char_set'],
			        'collation' => $config['dbcollat'],
			        'prefix'    => $config['dbprefix']
			    ],$db_group);    
			}
			$capsule->setAsGlobal();
			$capsule->bootEloquent();
			try
			{
				$capsule::connection(ENVIRONMENT)->getPdo();
			}
			catch (Exception $e)
			{
				if(!is_cli())
				{
					$current_url = 
					($_SERVER['SERVER_PORT'] == 443)?'https':'http'."://"
					.(!empty($_SERVER['SERVER_NAME'])?$_SERVER['SERVER_NAME']:FALSE)
					.str_replace('//', '/', $_SERVER['REQUEST_URI']);

					if(!preg_match('/(installation)/', $current_url))
					{
						header('location:'.BASE_URL.'installation/database?code=database_connection_failed');
						exit();
					}
				}
				else
				{
					echo 'database configuration failed';
				}
			}
		}
	}
}

/* End of file Boot.php */
/* Location: ./application/hooks/Boot.php */
