<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category MODEL
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
class Installation_model extends CI_Model
{
	/**
	 * constructor class
	*/
	public $table_prefix;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->dbforge();
		$this->table_prefix = json_decode(file_get_contents(FCPATH.'app.config'))->table_prefix;
	}

	/**
	 * @method create all table by class methods
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function create()
	{
		$methods = array();
		foreach (this_class_methods(self::class) as $value)
		{
			if(!in_array($value, ['__construct','create','create_database']))
			array_push($methods, $this->table_prefix.$value);
		}
		return $methods;
	}

	/**
	 * @method create database
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function create_database($db_name=null)
	{
		$db_name = (!empty($db_name))?$db_name:APP['name'].'_'.ENVIRONMENT.'_'.time();
		return ($this->dbforge->create_database($db_name))?$db_name:false;
	}

	/**
	 * @method table for user
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function user()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'level'					=> ['type' => 'ENUM','constraint' => ['admin','user'],'default' => 'user'],
			'nip'					=> ['type' => 'INT','constraint' => 20,'default' => NULL],
			'email'					=> ['type' => 'VARCHAR','constraint' => 320,'default' => NULL],
			'username'				=> ['type' => 'VARCHAR','constraint' => 16,'default' => NULL],
			'password'				=> ['type' => 'VARCHAR','constraint' => 40,'default' => NULL],
			'full_name'				=> ['type' => 'VARCHAR','constraint' => 80,'default' => NULL],
			'birthday'				=> ['type' => 'DATE','default' => NULL],
			'gender'				=> ['type' => 'ENUM','constraint' => ['male','female'],'default' => NULL],
			'address'				=> ['type' => 'VARCHAR','constraint' => 240,'default' => NULL],
			'latitude'				=> ['type' => 'DOUBLE','default' => NULL],
			'longitude'				=> ['type' => 'DOUBLE','default' => NULL],
			'profile_photo'			=> ['type' => 'VARCHAR','constraint' => 620,'default' => NULL],
			'status'				=> ['type' => 'TINYINT','constraint' => 1,'default' => 0],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for role
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function role()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'name'					=> ['type' => 'VARCHAR','constraint' => 255],
			'access_module'			=> ['type' => 'TEXT','default' => NULL],
			'per_sekolah'			=> ['type' => 'TINYINT','constraint' => 1,'default' => NULL],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for footer role
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function footer_role()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'module'				=> ['type' => 'VARCHAR','constraint' => 255],
			'data_id'				=> ['type' => 'BIGINT','unsigned' => TRUE],
			'role'					=> ['type' => 'VARCHAR','constraint' => 255],
			'user_id'				=> ['type' => 'BIGINT','unsigned' => TRUE],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for password reset
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function password_reset()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'user_id'				=> ['type' => 'BIGINT','unsigned' => TRUE],
			'unique_id'				=> ['type' => 'VARCHAR','constraint' => 20],
			'expire'				=> ['type' => 'DATETIME'],
			'code'					=> ['type' => 'VARCHAR','constraint' => 10],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for user role
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function user_role()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'user_id'				=> ['type' => 'BIGINT','unsigned' => TRUE],
			'role_id'				=> ['type' => 'BIGINT','unsigned' => TRUE],
			'sekolah_id'			=> ['type' => 'BIGINT','unsigned' => TRUE],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}


	/**
	 * @method table for setting
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function setting()
	{
		$fields =
		[
			// Default Fields
			'id'				=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'group'				=> ['type' => 'VARCHAR','constraint' => 40,'default' => NULL],
			'name'				=> ['type' => 'VARCHAR','constraint' => 80],
			'display_name'		=> ['type' => 'VARCHAR','constraint' => 60],
			'value'				=> ['type' => 'TEXT','default' => NULL],
			'additional_data'	=> ['type' => 'TEXT','default' => NULL],
			'description'		=> ['type' => 'TEXT','default' => NULL],
			//	User Responsibility
			'created_by'		=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'		=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'		=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'		=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'		=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'		=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for file
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function file()
	{
		$fields =
		[
			// Default Fields
			'id'				=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'module'			=> ['type' => 'VARCHAR','constraint' => 40,'default' => NULL],
			'data_id'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'mime'				=> ['type' => 'VARCHAR','constraint' => 40,'default' => NULL],
			'slug'				=> ['type' => 'VARCHAR','constraint' => 255,'default' => NULL],
			'path'				=> ['type' => 'VARCHAR','constraint' => 660,'default' => NULL],
			'name'				=> ['type' => 'VARCHAR','constraint' => 240,'default' => NULL],
			'size'				=> ['type' => 'DOUBLE','default' => NULL],
			'static'			=> ['type' => 'BOOLEAN','default' => TRUE],
			'password'			=> ['type' => 'VARCHAR','constraint' => 40,'default' => NULL],
			'additional_data'	=> ['type' => 'TEXT','default' => NULL],
			'status'			=> ['type' => 'TINYINT','constraint' => 1,'default' => 1],
			//	User Responsibility
			'created_by'		=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'		=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'		=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'		=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'		=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'		=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for sekolah
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function sekolah()
	{
		$fields =
		[
			// Default Fields
			'id'				=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'npsn'				=> ['type' => 'BIGINT','unsigned' => TRUE],
			'nama'				=> ['type' => 'VARCHAR','constraint' => 255],
			'tipe'				=> ['type' => 'ENUM','constraint' => ['negeri','swasta']],
			'logo'				=> ['type' => 'VARCHAR','constraint' => 255,'default' => NULL],
			'latitude'			=> ['type' => 'DOUBLE','default' => NULL],
			'longitude'			=> ['type' => 'DOUBLE','default' => NULL],
			'jumlah_siswa'		=> ['type' => 'BIGINT','unsigned' => TRUE],
			'alamat'			=> ['type' => 'TEXT','default' => NULL],
			'kecamatan'			=> ['type' => 'VARCHAR','constraint' => 255,'default' => NULL],
			'kabupaten'			=> ['type' => 'VARCHAR','constraint' => 255,'default' => NULL],
			'provinsi'			=> ['type' => 'VARCHAR','constraint' => 255,'default' => NULL],
			'status'			=> ['type' => 'TINYINT','constraint' => 1,'default' => 1],
			//	User Responsibility
			'created_by'		=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'		=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'		=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'		=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'		=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'		=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for rka
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function rka()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'sekolah_id'			=> ['type' => 'BIGINT','unsigned' => TRUE],
			'urusan_pemerintahan'	=> ['type' => 'VARCHAR','constraint' => 255],
			'organisasi'			=> ['type' => 'VARCHAR','constraint' => 255],
			'program'				=> ['type' => 'VARCHAR','constraint' => 255],
			'kegiatan'				=> ['type' => 'VARCHAR','constraint' => 255],
			'program_id'			=> ['type' => 'BIGINT','unsigned' => TRUE],
			'kegiatan_id'			=> ['type' => 'BIGINT','unsigned' => TRUE],
			'lokasi_kegiatan'		=> ['type' => 'VARCHAR','constraint' => 255],
			'sasaran_kegiatan'		=> ['type' => 'VARCHAR','constraint' => 255],
			'jumlah_dana'			=> ['type' => 'DOUBLE'],
			'sumber_dana'			=> ['type' => 'VARCHAR','constraint' => 255],
			'allow_edit'			=> ['type' => 'TINYINT','constraint' => 1,'default' => 0],
			'status'				=> ['type' => 'TINYINT','constraint' => 1,'default' => 0],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for bku
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function bku()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'sekolah_id'			=> ['type' => 'BIGINT','unsigned' => TRUE],
			'bulan'					=> ['type' => 'INT','constraint' => 2],
			'tahun'					=> ['type' => 'INT','constraint' => 4],
			'saldo'					=> ['type' => 'DOUBLE','default' => NULL],
			'allow_edit'			=> ['type' => 'TINYINT','constraint' => 1,'default' => 0],
			'status'				=> ['type' => 'TINYINT','constraint' => 1,'default' => 0],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for bku data
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function bku_data()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'bku_id'				=> ['type' => 'BIGINT','unsigned' => TRUE],
			'kode_rekening'			=> ['type' => 'VARCHAR','constraint' => 255,'default' => NULL],
			'nomor_bukti'			=> ['type' => 'VARCHAR','constraint' => 255,'default' => NULL],
			'uraian'				=> ['type' => 'TEXT'],
			'pemasukan'				=> ['type' => 'DOUBLE','default' => NULL],
			'pengeluaran'			=> ['type' => 'DOUBLE','default' => NULL],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for pembantu bank
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function pembantu_bank()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'sekolah_id'			=> ['type' => 'BIGINT','unsigned' => TRUE],
			'bulan'					=> ['type' => 'INT','constraint' => 2],
			'tahun'					=> ['type' => 'INT','constraint' => 4],
			'saldo'					=> ['type' => 'DOUBLE','default' => NULL],
			'allow_edit'			=> ['type' => 'TINYINT','constraint' => 1,'default' => 0],
			'status'				=> ['type' => 'TINYINT','constraint' => 1,'default' => 0],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for pembantu bank data
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function pembantu_bank_data()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'pembantu_bank_id'		=> ['type' => 'BIGINT','unsigned' => TRUE],
			'kode_rekening'			=> ['type' => 'VARCHAR','constraint' => 255,'default' => NULL],
			'nomor_bukti'			=> ['type' => 'VARCHAR','constraint' => 255,'default' => NULL],
			'uraian'				=> ['type' => 'TEXT'],
			'pemasukan'				=> ['type' => 'DOUBLE','default' => NULL],
			'pengeluaran'			=> ['type' => 'DOUBLE','default' => NULL],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for pembantu kas
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function pembantu_kas()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'sekolah_id'			=> ['type' => 'BIGINT','unsigned' => TRUE],
			'bulan'					=> ['type' => 'INT','constraint' => 2],
			'tahun'					=> ['type' => 'INT','constraint' => 4],
			'saldo'					=> ['type' => 'DOUBLE','default' => NULL],
			'allow_edit'			=> ['type' => 'TINYINT','constraint' => 1,'default' => 0],
			'status'				=> ['type' => 'TINYINT','constraint' => 1,'default' => 0],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for pembantu kas data
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function pembantu_kas_data()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'pembantu_kas_id'		=> ['type' => 'BIGINT','unsigned' => TRUE],
			'kode_rekening'			=> ['type' => 'VARCHAR','constraint' => 255,'default' => NULL],
			'nomor_bukti'			=> ['type' => 'VARCHAR','constraint' => 255,'default' => NULL],
			'uraian'				=> ['type' => 'TEXT'],
			'pemasukan'				=> ['type' => 'DOUBLE','default' => NULL],
			'pengeluaran'			=> ['type' => 'DOUBLE','default' => NULL],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for penutupan kas
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function penutupan_kas()
	{
		$fields =
		[
			// Default Fields
			'id'							=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'sekolah_id'					=> ['type' => 'BIGINT','unsigned' => TRUE],
			'tanggal'						=> ['type' => 'DATE','default' => NULL],
			'pemasukan'						=> ['type' => 'DOUBLE','default' => NULL],
			'pengeluaran'					=> ['type' => 'DOUBLE','default' => NULL],
			'nama_penutup_kas'				=> ['type' => 'VARCHAR','constraint' => 255],
			'user_id_penutup_kas'			=> ['type' => 'BIGINT','unsigned' => TRUE],
			'saldo_buku'					=> ['type' => 'DOUBLE','default' => NULL],
			'saldo_kas'						=> ['type' => 'DOUBLE','default' => NULL],
			'saldo_bank'					=> ['type' => 'DOUBLE','default' => NULL],
			'saldo_kas_rincian'				=> ['type' => 'TEXT','default' => NULL],
			'perbedaan_kas_bank'			=> ['type' => 'DOUBLE','default' => NULL],
			'deskripsi_perbedaan_kas_bank'	=> ['type' => 'TEXT','default' => NULL],
			'status'						=> ['type' => 'TINYINT','constraint' => 1,'default' => 0],
			//	User Responsibility
			'created_by'					=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'					=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'					=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'					=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'					=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'					=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for rka uraian
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function rka_uraian()
	{
		$fields =
		[
			// Default Fields
			'id'						=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'rka_id'					=> ['type' => 'BIGINT','unsigned' => TRUE],
			'kode_rekening_id'			=> ['type' => 'BIGINT','unsigned' => TRUE],
			'kode_rekening_objek_id'	=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for rka rincian
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function rka_rincian()
	{
		$fields =
		[
			// Default Fields
			'id'								=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'rka_uraian_id'						=> ['type' => 'BIGINT','unsigned' => TRUE],
			'kode_rekening_objek_rincian_id'	=> ['type' => 'BIGINT','unsigned' => TRUE],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for rka uraian rincian
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function rka_uraian_rincian()
	{
		$fields =
		[
			// Default Fields
			'id'				=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'rka_id'			=> ['type' => 'BIGINT','unsigned' => TRUE],
			'rka_rincian_id'	=> ['type' => 'BIGINT','unsigned' => TRUE],
			'rincian'			=> ['type' => 'VARCHAR','constraint' => 255],
			'volume'			=> ['type' => 'VARCHAR','constraint' => 255],
			'satuan'			=> ['type' => 'VARCHAR','constraint' => 255],
			'harga'				=> ['type' => 'VARCHAR','constraint' => 255],
			'jumlah'			=> ['type' => 'VARCHAR','constraint' => 255],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for sumber dana
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function sumber_dana()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'nama'					=> ['type' => 'VARCHAR','constraint' => 255],
			'jumlah'				=> ['type' => 'DOUBLE'],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for program
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function program()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'nama'					=> ['type' => 'VARCHAR','constraint' => 255],
			'kelompok_sasaran'		=> ['type' => 'VARCHAR','constraint' => 255],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for kegiatan
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function kegiatan()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'program_id'			=> ['type' => 'BIGINT','unsigned' => TRUE],
			'nama'					=> ['type' => 'VARCHAR','constraint' => 255],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}
	
	/**
	 * @method table for kode rekening
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function kode_rekening()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'sumber_dana_id'		=> ['type' => 'BIGINT','unsigned' => TRUE],
			'nama'					=> ['type' => 'VARCHAR','constraint' => 255],
			'akun'					=> ['type' => 'INT','constraint' => 6],
			'kelompok'				=> ['type' => 'INT','constraint' => 6,'default' => NULL],
			'jenis'					=> ['type' => 'INT','constraint' => 6,'default' => NULL],
			'saldo'					=> ['type' => 'DOUBLE','default' => NULL],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for kode rekening objek
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function kode_rekening_objek()
	{
		$fields =
		[
			// Default Fields
			'id'					=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'kode_rekening_id'		=> ['type' => 'BIGINT','unsigned' => TRUE],
			'nama'					=> ['type' => 'VARCHAR','constraint' => 255],
			//	User Responsibility
			'created_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'			=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'			=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'			=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'			=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}

	/**
	 * @method table for kode rekening objek rincian
	 * @author Agung Dirgantara <iam@agungdirgantara.id>
	*/
	public function kode_rekening_objek_rincian()
	{
		$fields =
		[
			// Default Fields
			'id'						=> ['type' => 'BIGINT','unsigned' => TRUE,'auto_increment' => TRUE],
			'kode_rekening_objek_id' 	=> ['type' => 'BIGINT','unsigned' => TRUE],
			'nama'						=> ['type' => 'VARCHAR','constraint' => 255],
			//	User Responsibility
			'created_by'				=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'updated_by'				=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			'deleted_by'				=> ['type' => 'BIGINT','unsigned' => TRUE,'default' => NULL],
			// Date of Occurrence
			'created_at'				=> ['type' => 'DATETIME','default' => NULL],
			'updated_at'				=> ['type' => 'DATETIME','default' => NULL],
			'deleted_at'				=> ['type' => 'DATETIME','default' => NULL]
		];
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		return $this->dbforge->create_table($this->table_prefix.__FUNCTION__,TRUE);
	}
}

/* End of file Installation_model.php */
/* Location: ./application/models/Installation_model.php */