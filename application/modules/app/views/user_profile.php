 <div class="box">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $user->full_name; ?></h3>
	</div>
	<div class="box-body">
		<table class="table table-striped">
			<tr>
				<td>NIP</td>
				<td><?php echo $user->nip; ?></td>
			</tr>
			<tr>
				<td>Level</td>
				<td><?php echo $user->level; ?></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><?php echo $user->email; ?></td>
			</tr>
			<tr>
				<td>Username</td>
				<td><?php echo $user->username; ?></td>
			</tr>
		</table>
	</div>
	<div class="box-footer">
	<?php 
	$user_id = $this->uri->segment(4);
	if(is_me($user_id))
	{
		echo '<a href="'.base_url('app/user/reset_password/').$user_id.'" class="btn btn-primary"><i class="fa fa-key"></i> Change Password</a>';
	}
	?>
	</div>
</div>