<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category MODEL
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Pembantu_kas_model extends Model
{
	use SoftDeletes;
	public $timestamps	= true;
	
	protected $table = TablePrefix.'pembantu_kas';
	protected $guarded	= [];
	protected $hidden 	= [];
	protected $fillable = [];
	protected $connection = ENVIRONMENT;

	/* get sekolah */
	public function sekolah($column=null)
	{
		return $this->hasOne('Sekolah_model','id','sekolah_id')->first($column);
	}

	/* get uraian */
	public function uraian($column=null)
	{
		return $this->hasMany('Pembantu_kas_data_model','pembantu_kas_id')->get($column);
	}
}
/* End of file Pembantu_kas_model.php */
/* Location: ./application/modules/bos/models/Pembantu_kas_model.php */