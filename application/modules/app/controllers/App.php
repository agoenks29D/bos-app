<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category MODULAR
 * @author author_name <author@domain.com>
 * @version 0.1
*/
class App extends MX_Controller
{
	/**
	 * constructor class
	*/
	public function __construct()
	{
		parent::__construct();
		$this->template->set_base('base_template');
		if(empty($this->session->userdata('user_id')))
		{
			if($this->router->method !== 'sign_in')
			{
				redirect(base_url('app/sign_in'));
			}
		}
		$this->load->model([
			'bos/user_model' => 'user'
		]);
	}

	/* default */
	public function index()
	{
		$this->template->load(null,'app_dashboard');
	}

	/* user module */
	public function user($param=null,$param1=null)
	{
		switch ($param)
		{
			case 'profile':
				$this->user_profile($param1);
			break;

			case 'edit':
				$this->user_edit($param1);
			break;

			case 'reset_password':
				$this->user_reset_password($param1);
			break;
			
			default:
				$data['table'] = $this->template->datatable(['#','No','Username','Email','Full Name','Gender','Status','Option'],true);
				$this->template->load(null,'user',$data);
			break;
		}
	}

	/* role */
	public function role()
	{
		$this->load->model([
			'bos/role_model' => 'role',
			'bos/user_role_model' => 'user_role',
			'bos/role_model' => 'role'
		]);
		$data['role'] = $this->role->all();
		$this->template->load(null,'role',$data);
	}

	/* user role */
	public function user_role($user_id=null)
	{
		$data['table'] = $this->template->datatable(['#','No','User','Role','Sekolah','Option'],true);
		$this->template->load(null,'user_role',$data);		
	}

	/* user profile */
	public function user_profile($unique_id=null)
	{
		$data['user'] = $this->user->find($unique_id);
		$this->template->load(null,'user_profile',$data);
	}

	/* user reset password */
	public function user_reset_password($unique_id=null)
	{
		$data['user'] = $this->user->find($unique_id);
		$this->template->load(null,'user_reset_password',$data);
	}

	/* user edit */
	public function user_edit($unique_id=null)
	{
		$data['user'] = $this->user->find($unique_id);
		$this->template->load(null,'user_edit',$data);
	}

	/* user level */
	public function user_level()
	{
		$data['table'] = $this->template->datatable(['#','No','Username','Email','Full Name','Gender','Status','Option'],true);
		$this->template->load(null,'user',$data);
	}

	/* access control */
	public function access_control()
	{
		$this->template->load(null,'access_control');
	}

	/* user sign in */
	public function sign_in()
	{
		$this->load->view('sign_in');
	}

	/* reset password */
	public function reset_password()
	{
		
	}
}

/* End of file App.php */
/* Location: ./application/modules/app/controllers/App.php */