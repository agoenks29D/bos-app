<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category API
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/
class User extends Rest_api
{
	/**
	 * constructor class
	*/
	public function __construct()
	{
		parent::__construct();
		$this->load->model([
			'user_model' => 'user',
			'role_model' => 'role',
			'user_role_model' => 'user_role',
			'file_model' => 'file',
			'sekolah_model' => 'sekolah',
			'password_reset_model' => 'password_reset'
		]);
		
		// Check Session
		if(empty($this->session->userdata('user_id')))
		{
			if(!in_array($this->router->method, ['sign_in']))
			{
				if($this->input->is_ajax_request())
				{
					$this->response(['status' => 'failed','message' => 'login_required'],REST_Controller::HTTP_OK);
				}
				else
				{
					redirect(base_url('app/user/sign_in'),'refresh');
				}
			}
		}
		$this->form_validation->set_data($this->post());
	}

	/* users */
	public function index_get($unique_id=null)
	{
		if(!empty($unique_id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?$this->user->onlyTrashed()->where('id',$unique_id)->orWhere('username',$unique_id)->first():$this->user->where('id',$unique_id)->orWhere('username',$unique_id)->first();
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->user;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',true),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				$get = $model->limit($limit)->offset($offset)->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$roles = array();
						foreach ($value->role() as $role)
						{
							$roles[] = $role->get_role();
						}

						$data[] = array_merge($value->toArray(),
							[
								'roles' => $roles
							]
						);
					}
				}
			}
			else
			{
				$get = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$roles = array();
						foreach ($value->role() as $role)
						{
							$roles[] = $role->get_role();
						}
						
						$data[] = array_merge($value->toArray(),
							[
								'roles' => $roles
							]
						);
					}
				}
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* sign in user rest */
	public function sign_in_post()
	{
		$identity = $this->post('identity');
		$password = $this->post('password');
		$identify = $this->user->where(function($identity_account) use ($identity){
		$identity_account
			->where('username',$identity)
			->orWhere('email',$identity);
		})->first();

		if(!empty($identify))
		{
			if($identify->password == sha1($password))
			{
				$this->session->set_userdata('user_id',$identify->id);
				$response = ['status' => 'success','data' => $identify];
			}
			else
			{
				$response = ['status' => 'failed','message' => 'authentication_failed'];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'data_not_found'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* create user */
	public function create_post()
	{
		// validate nip
		$this->form_validation->set_rules('nip', 'nip', ['trim',['validate_nip',function($nip){
			$check = $this->user->where('nip',$nip)->first();
			$this->form_validation->set_message('validate_nip','nip has been used');
			return (empty($check));
		}]]);
		$this->form_validation->set_rules('full_name', 'full name', 'trim|required');
		$this->form_validation->set_rules('level', 'level', 'trim|required|in_list[admin,user]');
		// Email
		$this->form_validation->set_rules('email', 'email', ['trim','required','valid_email',['validate_user_email',function($email){
			$this->form_validation->set_message('validate_user_email','email has been used');
			return empty($this->user->where('email',$email)->first());
		}]]);
		// Username
		$this->form_validation->set_rules('username', 'username', ['trim',['validate_username',function($username){
			$this->form_validation->set_message('validate_username','username has been used');
			return empty($this->user->where('username',$username)->first());
		}]]);
		$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('repeat_password', 'repeat password', 'trim|required|matches[password]');
		$this->form_validation->set_rules('gender', 'gender', 'trim|in_list[male,female]');
		$this->form_validation->set_rules('status', 'status', 'trim|required|integer');
		if($this->form_validation->run() == TRUE)
		{

			$upload_config = 
			[
				'upload_path' => Modules::run('bos/file/validate_modules_folder',Modules::run('bos/file/site_data_folder').'/user/'),
				'allowed_types' => 'jpg|jpeg|png|gif',
				'max_size' => 124000,
				'max_width' => 124000,
				'max_height' => 124000,
				'min_width' => 0,
				'min_height' => 0,
				'max_filename' => 0,
				'max_filename_increment' => 1000,
				'encrypt_name' => TRUE,
				'file_name' => NULL,
				'file_ext_tolower' => TRUE,
				'remove_spaces' => TRUE,
				'detect_mime' => TRUE,
				'mod_mime_fix' => TRUE
			];

			$image = Modules::run('bos/file/upload','image',$upload_config);
			$upload_image_status = false;
			if(!empty($image['image']))
			{
				$this->user->level = $this->post('level');
				$this->user->nip = $this->post('nip');
				$this->user->full_name = $this->post('full_name');
				$this->user->email = $this->post('email');
				$this->user->username = $this->post('username');
				$this->user->password = sha1($this->post('password'));
				$this->user->gender = $this->post('gender');
				$this->user->address = $this->post('address');
				$this->user->status = $this->post('status');
				$this->user->save();

				if(isset($image['image']['file_name']))
				{
					$upload_image_status = true;
					$this->file->module = 'user';
					$this->file->data_id = $this->user->id;
					$this->file->mime = $image['image']['file_type'];
					$this->file->slug = url_title($this->user->full_name.' '.time());
					$this->file->path = $image['image']['full_path'];
					$this->file->name = $image['image']['file_name'];
					$this->file->size = $image['image']['file_size'];
					$this->file->static = 1;
					$this->file->status = 1;
					$this->file->save();
					// save image
					$this->user->profile_photo = $this->file->id;
					$this->user->save();
				}
			}
			$response = ['status' => 'success','data' => $this->user];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* update user */
	public function update_post($unique_id=null)
	{
		$find = $this->user->where('id',$unique_id)->orWhere('username',$unique_id)->first();
		if(!empty($find))
		{
			$this->form_validation->set_rules('nip', 'nip', ['trim',['validate_nip',function($nip) use ($find){
				$check = $this->user->where('nip',$nip)->first();
				$this->form_validation->set_message('validate_nip','nip has been used');
				return (empty($check))?TRUE:($check->id == $find->id);
			}]]);
			if($this->form_validation->run() == TRUE)
			{
				$upload_config = 
				[
					'upload_path' => Modules::run('bos/file/validate_modules_folder',Modules::run('bos/file/site_data_folder').'/user/'),
					'allowed_types' => 'jpg|jpeg|png|gif',
					'max_size' => 124000,
					'max_width' => 124000,
					'max_height' => 124000,
					'min_width' => 0,
					'min_height' => 0,
					'max_filename' => 0,
					'max_filename_increment' => 1000,
					'encrypt_name' => TRUE,
					'file_name' => NULL,
					'file_ext_tolower' => TRUE,
					'remove_spaces' => TRUE,
					'detect_mime' => TRUE,
					'mod_mime_fix' => TRUE
				];

				$image = Modules::run('bos/file/upload','image',$upload_config);
				$upload_image_status = false;
				if(!empty($image['image']))
				{
					$find->level = return_if_exists($this->post(),'level',$find->level);
					$find->nip = $this->post('nip');
					$find->full_name = $this->post('full_name');
					$find->email = $this->post('email');
					$find->username = $this->post('username');
					$find->gender = $this->post('gender');
					$find->address = $this->post('address');
					$find->status = return_if_exists($this->post(),'status',$find->status);
					$find->save();

					if(isset($image['image']['file_name']))
					{
						$upload_image_status = true;
						$this->file->module = 'user';
						$this->file->data_id = $find->id;
						$this->file->mime = $image['image']['file_type'];
						$this->file->slug = url_title($find->full_name.' '.time());
						$this->file->path = $image['image']['full_path'];
						$this->file->name = $image['image']['file_name'];
						$this->file->size = $image['image']['file_size'];
						$this->file->static = 1;
						$this->file->status = 1;
						$this->file->save();
						// save image
						$find->profile_photo = $this->file->id;
						$find->save();
					}
				}
				$response = ['status' => 'success','data' => $find];
			}
			else
			{
				$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
				array_shift($validation_errors);
				$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'data_not_found'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* update password */
	public function update_password_post($unique_id=null)
	{
		$find = $this->user->where('id',$unique_id)->orWhere('username',$unique_id)->first();
		if(!empty($find))
		{
			$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('repeat_password', 'repeat password', 'trim|required|matches[password]');
			$find->password = sha1($this->post('password'));
			$find->save();
			$response = ['status' => 'success'];
		}
		else
		{
			$response = ['status' => 'failed','message' => 'data_not_found'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* delete user */
	public function delete_get($unique_id=null)
	{
		$find = $this->user->where('id',$unique_id)->orWhere('username',$unique_id)->first();
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* restpre user */
	public function restore_get($unique_id=null)
	{
		$find = $this->user->where('id',$unique_id)->orWhere('username',$unique_id)->first();
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);	
	}

	/* force delete user */
	public function force_delete_get($unique_id=null)
	{
		$find = $this->user->where('id',$unique_id)->orWhere('username',$unique_id)->first();
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);		
	}

	/* role */
	public function role_get($id=null)
	{
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?
			$this->role->onlyTrashed()->where('id',$id)->first():
			$this->role->where('id',$id)->first();
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->role;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',true),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				$data = $model->limit($limit)->offset($offset)->get();
			}
			else
			{
				$data = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* add role */
	public function add_role_post()
	{
		$this->form_validation->set_rules('name', 'name', 'trim|required');
		$this->form_validation->set_rules('per_sekolah', 'per sekolah', 'trim|required|in_list[yes,no]');
		if($this->form_validation->run() == TRUE)
		{
			$this->role->name = $this->post('name');
			$this->role->access_module = json_encode($this->post('access_module'));
			$this->role->per_sekolah = filter_var($this->post('per_sekolah'),FILTER_VALIDATE_BOOLEAN);
			$this->role->save();
			$response = ['status' => 'success','data' => $this->role];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* update role */
	public function update_role_post($id=null)
	{
		$find = $this->role->find($id);
		if(!empty($find))
		{
			$this->form_validation->set_rules('name', 'name', 'trim|required');
			$this->form_validation->set_rules('access_module', 'access module', 'trim|required');
			$this->form_validation->set_rules('per_sekolah', 'per sekolah', 'trim|required|in_list[yes,no]');
			if($this->form_validation->run() == TRUE)
			{
				$this->role->name = $this->post('name');
				$this->role->access_module = $this->post('access_module');
				$this->role->per_sekolah = $this->post('per_sekolah');
				$this->role->save();
				$response = ['status' => 'success','data' => $this->role];
			}
			else
			{
				$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
				array_shift($validation_errors);
				$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
			}
		}
		else
		{
			$response = ['status' => 'failed','message' => 'data_not_found'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* hapus role */
	public function delete_role_get($id=null)
	{
		$find = $this->role->find($id);
		$response = (!empty($find))?['status' => ($find->delete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* kembalikan data role */
	public function restore_role_get($id=null)
	{
		$find = $this->role->find($id);
		$response = (!empty($find))?['status' => ($find->restore())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);	
	}

	/* hapus permanen role */
	public function force_delete_role_get($id=null)
	{
		$find = $this->role->find($id);
		$response = (!empty($find))?['status' => ($find->forceDelete())?'success':'failed']:['status' => 'failed','message' => 'data_not_found'];
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/*  user role */
	public function user_role_get($id=null)
	{
		$user_id = $this->input->get('user_id');
		$sekolah_id = $this->input->get('sekolah_id');
		$role_id = $this->input->get('role_id');
		if(!empty($id))
		{
			$find = (filter_var($this->input->get('only_trash'),FILTER_VALIDATE_BOOLEAN))?
			$this->user_role->onlyTrashed()->where('id',$id)->first():
			$this->user_role->where('id',$id)->first();
			$response = (!empty($find))?['status' => 'success','data' => $find]:['status' => 'failed','message' => 'data_not_found'];
		}
		else
		{
			$model = $this->user_role;
			$data = array();
			$record_total = $model->count();
			$query = $this->input->get();
			if(filter_var(return_if_exists($query,'ajax',true),FILTER_VALIDATE_BOOLEAN))
			{
				$limit = (isset($query['length']) && $query['length'] != -1)?$query['length'] : $model->count();
				$offset = (isset($query['start']))? $query['start'] : 0;
				$model = datatable_query($model,$query);
				(!empty($user_id))?$model = $model->where('user_id',$user_id):FALSE;
				(!empty($sekolah_id))?$model = $model->where('sekolah_id',$sekolah_id):FALSE;
				(!empty($role_id))?$model = $model->where('role_id',$role_id):FALSE;
				$get = $model->limit($limit)->offset($offset)->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$data[] = array_merge($value->toArray(),
							[
								'user' => $this->user->find($value->user_id),
								'role' => $this->role->find($value->role_id),
								'sekolah' => $this->sekolah->find($value->sekolah_id)
							]
						);
					}
				}
			}
			else
			{
				$get = (isset($query['only_trash']))?$model->onlyTrashed()->get():$model->get();
				if(!empty($get))
				{
					foreach ($get as $value)
					{
						$data[] = array_merge($value->toArray(),
							[
								'user' => $this->user->find($value->user_id),
								'role' => $this->role->find($value->role_id),
								'sekolah' => $this->sekolah->find($value->sekolah_id)
							]
						);
					}
				}
			}
			
			// Set Response
			$response = [
				'draw' => (isset($query['draw']))?$query['draw']:false,
				'record_total' => $record_total,
				'record_filtered' => $record_total,
				'data' => $data
			];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* add user role */
	public function add_user_role_post()
	{
		$this->form_validation->set_rules('user_id', 'user', 'trim|required|integer');
		$this->form_validation->set_rules('role_id', 'role', 'trim|required|integer');
		if($this->form_validation->run() == TRUE)
		{
			$this->user_role->updateOrCreate([
				'user_id' => $this->post('user_id'),
				'role_id' => $this->post('role_id')
			],[
				'sekolah_id' => $this->post('sekolah_id')
			]);
			$response = ['status' => 'success','data' => $this->user_role];
		}
		else
		{
			$validation_errors = explode('<p>',str_replace('</p>','',validation_errors()));
			array_shift($validation_errors);
			$response = ['status' => 'failed','message' => 'validation_error','data' => $validation_errors];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* reset password */
	public function reset_password_post()
	{
		$unique_id = $this->post('identity');
		$find = $this->user->where('email',$unique_id)->orWhere('username',$unique_id)->first();
		if(!empty($find))
		{
			$this->password_reset->user_id = $find->id;
			$this->password_reset->unique_id = random_string('alnum',12);
			$this->password_reset->expire = date('d-m-y');
			$this->password_reset->code = random_string('int',6);
			$this->password_reset->save();
			$response = ['status' => 'success','data' => $find];
		}
		else
		{
			$response = ['status' => 'failed'];
		}
		$this->response($response,REST_Controller::HTTP_OK);
	}

	/* sign out user */
	public function sign_out_get()
	{
		session_destroy();
		if($this->input->is_ajax_request())
		{
			$this->response(['status' => 'success'],REST_Controller::HTTP_OK);
		}
		else
		{
			redirect(base_url('app/sign_in'),'refresh');
		}
	}
}
/* End of file User.php */
/* Location: ./application/modules/bos/controllers/User.php */