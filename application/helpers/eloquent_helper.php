<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package CODEIGNITER
 * @category HELPER
 * @author Agung Dirgantara <iam@agungdirgantara.id>
 * @version 0.1
*/

use Illuminate\Database\Capsule\Manager as Capsule;
$db_config_file = FCPATH.'.database';
if(file_exists($db_config_file))
{
    // Get DB Config
    $db_config = json_decode(file_get_contents($db_config_file),TRUE);

    // Configure Database
    $capsule = new Capsule;
    foreach ($db_config as $db_group => $config)
    {
        $capsule->addConnection([
            'driver'    => ($config['dbdriver'] == 'mysqli')?'mysql':$config['dbdriver'],
            'host'      => $config['hostname'],
            'database'  => $config['database'],
            'username'  => $config['username'],
            'password'  => $config['password'],
            'charset'   => $config['char_set'],
            'collation' => $config['dbcollat'],
            'prefix'    => $config['dbprefix']
        ],$db_group);
    }
    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    /**
     * @method Check Database Connection
     * @author Agung Dirgantara <iam@agungdirgantara.id>
    */
    function check_connection($connection=ENVIRONMENT)
    {
        return Capsule::connection($connection)->getPdo();
    }

    /**
     * @method Get Schema Builder
     * @author Agung Dirgantara <iam@agungdirgantara.id>
    */
    function schema($connection=ENVIRONMENT)
    {
        return Capsule::connection($connection)->getSchemaBuilder();
    }

    /**
     * @method Get Table Prefix
     * @author Agung Dirgantara <iam@agungdirgantara.id>
    */
    function table_prefix($table=null)
    {
        $table_prefix =  json_decode(file_get_contents(FCPATH.'app.config'))->table_prefix;
        return (!empty($table))?$table_prefix.$table:$table_prefix;
    }

    /**
     * @method Show Create Table
     * @author Agung Dirgantara <iam@agungdirgantara.id>
    */
    function show_create_table($table_name=null)
    {
        return Capsule::select('SHOW CREATE TABLE '.$table_name);
    }

    /**
     * @method Show Database Tables
     * @author Agung Dirgantara <iam@agungdirgantara.id>
    */
    function show_tables($connection=ENVIRONMENT)
    {
        foreach (Capsule::connection($connection)->select('SHOW TABLES') as $value)
        {
            $tables[] = $value->{'Tables_in_'.Capsule::connection($connection)->getDatabaseName()};
        }
        return $tables;
    }

    /**
     * @method Show Database Column
     * @author Agung Dirgantara <iam@agungdirgantara.id>
    */
    function show_columns($table=null,$connection=ENVIRONMENT)
    {
        return Capsule::connection($connection)->getSchemaBuilder()->getColumnListing((is_object($table))?$table->table:$table);
    }
}
/* End of file eloquent_helper.php */
/* Location: ./application/helpers/eloquent_helper.php */