<?php
if(is_me($this->uri->segment(4)) OR is_admin())
{
?>
	<form enctype="multipart/form-data" id="UpdateUser">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Edit Profile</h3>
			</div>
			<div class="box-body">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="form-group">
						<label>NIP</label>
						<input type="text" name="nip" class="form-control input-sm" value="<?php echo $user->nip;?>" placeholder="NIP">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="form-group">
						<label>Full Name</label>
						<input type="text" name="full_name" class="form-control input-sm" value="<?php echo $user->full_name;?>" placeholder="Username">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="form-group">
						<label>Email</label>
						<input type="text" name="email" class="form-control input-sm" value="<?php echo $user->email;?>" placeholder="Email">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="form-group">
						<label>Username</label>
						<input type="text" name="username" class="form-control input-sm" value="<?php echo $user->username;?>" placeholder="Username">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="form-group">
						<label>Gender </label>
						<p>
							<label class="form-check-label" for="male">Male</label>
							<input class="flat-green" type="radio" name="gender" value="male" id="male" <?php echo ($user->gender ==  'male')?'checked':false; ?>>
							<label class="form-check-label" for="female">Female</label>
							<input class="flat-green" type="radio" name="gender" value="female" id="female" <?php echo ($user->gender ==  'female')?'checked':false; ?>>
						</p>
					</div>
				</div>
				<?php 
				if(is_admin())
				{

				?>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="form-group">
						<label>Level</label>
						<select name="level" class="form-control">
							<option value="user" <?php echo ($user->level == 'user')?'selected':false; ?>>User</option>
							<option value="admin" <?php echo ($user->level == 'admin')?'selected':false; ?>>Admin</option>
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="form-group">
						<label>Status</label>
						<p>
							<label><input type="radio" name="status" class="flat-red" value="1" <?php echo ($user->status == 1)?'checked':false; ?>> Active</label>
							<label><input type="radio" name="status" class="flat-red" value="0" <?php echo ($user->status == 0)?'checked':false; ?>> Non-active</label>
						</p>
					</div>
				</div>
				<?php
				}
				?>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="form-group">
						<label>Photo</label>
						<br>
						<input type="file" name="image" class="form-control input-sm">
					</div>
				</div>
			</div>
			<div class="box-footer">
				<button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
			</div>
		</div>
	</form>
<?php
}
else
{
	show_404();
}
?>

<script type="text/javascript">

$('#UpdateUser').on('submit',(function(event){
	event.preventDefault();
	$.ajax({
		url: '<?php echo base_url("bos/user/update/".$user->id);?>',
		type: 'POST',
		dataType: 'json',
		contentType:false,
		cache: false,
		processData:false,
		data:new FormData(this),
		success:(data) => {
			if(data.status == 'success')
			{
				$('#UpdateUser')[0].reset()
				swal({
					position: 'top-end',
					type: 'success',
					title: 'Account updated success',
					showConfirmButton: false,
					timer: 1500,
					onClose:window.location.reload()
				}).then(function(){
				},function(dismiss){})
			}
			else
			{
				swal({
					type:'error',
					html:data.data.join('</p><p>')
				})
			}
		},
		error:(error) => {
			console.log(error)
		}
	})
}))
</script>